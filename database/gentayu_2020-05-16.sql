# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.19)
# Database: gentayu
# Generation Time: 2020-05-16 08:17:19 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_admin_group_menu_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_group_menu_access`;

CREATE TABLE `tbl_admin_group_menu_access` (
  `aga_id` int unsigned NOT NULL AUTO_INCREMENT,
  `aga_aug_id` int DEFAULT NULL,
  `aga_asm_id` int DEFAULT NULL,
  `aga_access` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`aga_id`),
  KEY `query_index` (`aga_aug_id`,`aga_asm_id`,`aga_access`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_admin_group_menu_access` WRITE;
/*!40000 ALTER TABLE `tbl_admin_group_menu_access` DISABLE KEYS */;

INSERT INTO `tbl_admin_group_menu_access` (`aga_id`, `aga_aug_id`, `aga_asm_id`, `aga_access`)
VALUES
	(1,1,1,1),
	(6,1,2,1),
	(9,1,3,1),
	(10,1,4,1),
	(11,1,5,1),
	(12,1,6,1),
	(21,1,7,1),
	(22,1,8,1),
	(23,1,9,1),
	(30,1,10,1),
	(31,1,11,1),
	(36,1,13,1),
	(2,2,1,1),
	(7,2,2,1),
	(13,2,3,1),
	(14,2,4,1),
	(15,2,5,1),
	(16,2,6,1),
	(24,2,7,1),
	(25,2,8,1),
	(26,2,9,1),
	(32,2,10,1),
	(33,2,11,1),
	(37,2,13,1),
	(3,3,1,1),
	(8,3,2,1),
	(17,3,3,1),
	(18,3,4,1),
	(19,3,5,1),
	(20,3,6,1),
	(27,3,7,1),
	(28,3,8,1),
	(29,3,9,1),
	(34,3,10,1),
	(35,3,11,1),
	(4,4,1,1),
	(5,5,1,1);

/*!40000 ALTER TABLE `tbl_admin_group_menu_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_admin_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_session`;

CREATE TABLE `tbl_admin_session` (
  `ses_id` int NOT NULL AUTO_INCREMENT,
  `ses_usr_id` int DEFAULT NULL,
  `ses_anr_id` int DEFAULT NULL,
  `ses_key` varchar(255) DEFAULT NULL,
  `ses_activity` int DEFAULT NULL,
  `ses_valid` int DEFAULT NULL,
  `ses_last_ip` varchar(100) DEFAULT NULL,
  `ses_lang_id` int DEFAULT '1',
  `ses_create_datetime` int DEFAULT NULL,
  `ses_app_type` int DEFAULT '0' COMMENT '0 = web, 1 = mobile',
  `ses_dvc_id` int DEFAULT '0' COMMENT '0= not set, 1=Ios, 2=Android',
  `ses_device_id` text,
  `ses_time_zone_name` varchar(100) DEFAULT NULL,
  `ses_time_zone_gmt` varchar(100) DEFAULT NULL,
  `ses_time_zone_offset` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ses_id`),
  KEY `ses_usr_id` (`ses_usr_id`,`ses_key`,`ses_activity`,`ses_valid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_admin_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_user`;

CREATE TABLE `tbl_admin_user` (
  `adu_id` int unsigned NOT NULL AUTO_INCREMENT,
  `adu_com_id` int NOT NULL DEFAULT '0',
  `adu_usr_id` int DEFAULT NULL,
  `adu_aug_id` int NOT NULL DEFAULT '0',
  `adu_dpt_id` int NOT NULL DEFAULT '0',
  `adu_lvl_id` int NOT NULL DEFAULT '1',
  `adu_zon_id` int DEFAULT '1',
  `adu_username` varchar(255) NOT NULL DEFAULT '',
  `adu_first_name` varchar(255) DEFAULT NULL,
  `adu_last_name` varchar(255) DEFAULT NULL,
  `adu_screen_name` varchar(255) DEFAULT NULL,
  `adu_auth_key` varchar(255) NOT NULL DEFAULT '',
  `adu_image` varchar(255) DEFAULT NULL,
  `adu_password_hash` varchar(255) NOT NULL DEFAULT '',
  `adu_password_reset_token` varchar(255) DEFAULT NULL,
  `adu_password_md5` varchar(255) DEFAULT NULL,
  `adu_email` varchar(255) NOT NULL DEFAULT '',
  `adu_datetime` int DEFAULT '0',
  `adu_create_time` int NOT NULL DEFAULT '0',
  `adu_update_time` int NOT NULL DEFAULT '0',
  `adu_delete_time` int DEFAULT '0',
  `adu_create_adu_id` int DEFAULT '0',
  `adu_update_adu_id` int DEFAULT '0',
  `adu_delete_adu_id` int DEFAULT '0',
  `adu_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active, 2=Deleted',
  PRIMARY KEY (`adu_id`),
  KEY `query_index` (`adu_com_id`,`adu_usr_id`,`adu_aug_id`,`adu_dpt_id`,`adu_lvl_id`,`adu_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_admin_user` WRITE;
/*!40000 ALTER TABLE `tbl_admin_user` DISABLE KEYS */;

INSERT INTO `tbl_admin_user` (`adu_id`, `adu_com_id`, `adu_usr_id`, `adu_aug_id`, `adu_dpt_id`, `adu_lvl_id`, `adu_zon_id`, `adu_username`, `adu_first_name`, `adu_last_name`, `adu_screen_name`, `adu_auth_key`, `adu_image`, `adu_password_hash`, `adu_password_reset_token`, `adu_password_md5`, `adu_email`, `adu_datetime`, `adu_create_time`, `adu_update_time`, `adu_delete_time`, `adu_create_adu_id`, `adu_update_adu_id`, `adu_delete_adu_id`, `adu_status`)
VALUES
	(1,1,NULL,1,1,1,1,'sandyQx@gmail.com','Sandi','Ardyansyah','Sandi Ardyansyah','JEKEK83EIT6u5_fy-U9aSGyJFoPnMQQF','usr_1427513098.jpg','$2y$13$Fb18QCLHfLSD5Sf2rRCOgOnyMKfu.cJOCACqhNkzBSoavvXzDuAUi','ZpQw_TiCOzxresmZOCPw3nfdg2v55eWt_1433266234','e10adc3949ba59abbe56e057f20f883e','sandyQx@gmail.com',NULL,1419178562,1419215092,NULL,NULL,NULL,NULL,1),
	(8,1,NULL,3,2,1,1,'mamang@gmail.com','Mamang','Suramang','Mamang Suramang','wIGSUD5rffglODfPTC-8KdTbH2chS-pj','usr_1435834536.jpg','$2y$13$SK7KwuneD4J6vI5Kf55HYehWQnkiD/4f4CwSdqxULyf.YspNrh5nO','aT95TFy4F-BioTTJbr1ODSi4uKph6-oi_1496928030','e10adc3949ba59abbe56e057f20f883e','mamang@gmail.com',0,1428560068,0,0,0,0,0,1),
	(9,1,NULL,2,2,1,1,'user@gmail.com','user','biasa','user biasa','FM4FuW6rBD2fq_aCTeeZqzDsmtdGjyjE','','$2y$13$lOWFUbTKxivS3kUJNyli2.dvz4kzNUkfoHNoP44Bn2KOZ5QNAir1O','DvSboojlc2VYKpsEbELbw79djqR5TG5Q_1479543922',NULL,'user@gmail.com',0,1428566472,0,0,0,0,0,1),
	(10,1,NULL,5,7,3,1,'people@gmail.com','orang','basa','orang basa','PUhqQ-9C3D1kJlZJjmXcG0_fCQzaYDlj','','','qUFoybiIvm3Pwzy8j2OuEE9qey3SW3kh_1428566533',NULL,'people@gmail.com',0,1428566533,0,1481692336,0,0,1,1);

/*!40000 ALTER TABLE `tbl_admin_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_admin_user_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_user_group`;

CREATE TABLE `tbl_admin_user_group` (
  `aug_id` int unsigned NOT NULL AUTO_INCREMENT,
  `aug_name` varchar(255) DEFAULT NULL,
  `aug_code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT '',
  `aug_desck` text,
  `aug_datetime` int DEFAULT NULL,
  `aug_datetime_insert` int DEFAULT NULL,
  `aug_datetime_update` int DEFAULT NULL,
  `aug_datetime_delete` int DEFAULT NULL,
  `aug_adu_id_insert` int DEFAULT NULL,
  `aug_adu_id_update` int DEFAULT NULL,
  `aug_adu_id_delete` int DEFAULT NULL,
  `aug_status` int DEFAULT '1' COMMENT '0=Inactive, 1=Active, 2=Deleted',
  PRIMARY KEY (`aug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_admin_user_group` WRITE;
/*!40000 ALTER TABLE `tbl_admin_user_group` DISABLE KEYS */;

INSERT INTO `tbl_admin_user_group` (`aug_id`, `aug_name`, `aug_code`, `aug_desck`, `aug_datetime`, `aug_datetime_insert`, `aug_datetime_update`, `aug_datetime_delete`, `aug_adu_id_insert`, `aug_adu_id_update`, `aug_adu_id_delete`, `aug_status`)
VALUES
	(1,'Super Hero','SHR','This is to manage all dashboard',1588800056,1427559909,1588800056,NULL,1,1,NULL,1),
	(2,'Administrator','ADM','login group for all vendor partner',1588800051,1427560045,1588800051,NULL,1,1,NULL,1),
	(3,'Owner','OWN','',1479102396,NULL,1479102396,NULL,NULL,1,NULL,1),
	(4,'Developer','DEV','',1588505803,NULL,1588505803,NULL,NULL,1,NULL,1),
	(5,'Operator-Admin','OPR','',1479102426,NULL,1479102426,NULL,NULL,1,NULL,1),
	(6,'Partner','PTR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(7,'Vendor','VDR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);

/*!40000 ALTER TABLE `tbl_admin_user_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_admin_user_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_user_menu`;

CREATE TABLE `tbl_admin_user_menu` (
  `asm_id` int unsigned NOT NULL AUTO_INCREMENT,
  `asm_parent_id` int DEFAULT '0',
  `asm_name` varchar(255) DEFAULT NULL,
  `asm_icon` varchar(255) DEFAULT NULL,
  `asm_url` varchar(255) DEFAULT NULL,
  `asm_have_child` int DEFAULT '0',
  `asm_sort` int DEFAULT '0',
  `asm_status` int DEFAULT '1',
  PRIMARY KEY (`asm_id`),
  KEY `query_index` (`asm_parent_id`,`asm_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_admin_user_menu` WRITE;
/*!40000 ALTER TABLE `tbl_admin_user_menu` DISABLE KEYS */;

INSERT INTO `tbl_admin_user_menu` (`asm_id`, `asm_parent_id`, `asm_name`, `asm_icon`, `asm_url`, `asm_have_child`, `asm_sort`, `asm_status`)
VALUES
	(1,0,'Dashboar','fa-dashboard','dashboard/index',0,1,1),
	(2,0,'RBAC','fa-gear','#',1,20,1),
	(3,2,'Module List','fa-group','rbac/default/index',0,1,1),
	(4,2,'Access Group','fa-users','rbac/group/index',0,2,1),
	(5,2,'Users','fa-bar-chart-o','rbac/users/index',0,3,1),
	(6,2,'Access Permision','fa-sitemap','rbac/access-permision/index',0,4,1),
	(7,0,'Project','fa-money','#',1,3,1),
	(8,0,'Vulnerability','fa-bug','design',1,4,1),
	(9,0,'DevOps','fa-sitemap','patent',0,5,1),
	(10,7,'All Project','fa-gavel','product/project',0,1,1),
	(11,7,'Repository','fa-bar-chart-o','product/repository',0,2,1),
	(13,8,'Tools','','vulnerability',0,1,1);

/*!40000 ALTER TABLE `tbl_admin_user_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_department
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_department`;

CREATE TABLE `tbl_department` (
  `dpt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `dpt_com_id` int DEFAULT NULL,
  `dpt_cot_id` int DEFAULT NULL,
  `dpt_name` varchar(255) DEFAULT NULL,
  `dpt_code` varchar(255) DEFAULT NULL,
  `dpt_datetime` int DEFAULT NULL,
  `dpt_datetime_insert` int DEFAULT NULL,
  `dpt_datetime_update` int DEFAULT NULL,
  `dpt_datetime_delete` int DEFAULT NULL,
  `dpt_adu_id_insert` int DEFAULT NULL,
  `dpt_adu_id_update` int DEFAULT NULL,
  `dpt_adu_id_delete` int DEFAULT NULL,
  `dpt_status` int DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`dpt_id`),
  KEY `query_index` (`dpt_com_id`,`dpt_cot_id`,`dpt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_department` WRITE;
/*!40000 ALTER TABLE `tbl_department` DISABLE KEYS */;

INSERT INTO `tbl_department` (`dpt_id`, `dpt_com_id`, `dpt_cot_id`, `dpt_name`, `dpt_code`, `dpt_datetime`, `dpt_datetime_insert`, `dpt_datetime_update`, `dpt_datetime_delete`, `dpt_adu_id_insert`, `dpt_adu_id_update`, `dpt_adu_id_delete`, `dpt_status`)
VALUES
	(1,1,1,'Security','SEC',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(2,1,1,'DevOps','DOS',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(3,1,1,'Development','DEV',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(4,1,1,'Digital','DIG',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(5,1,1,'Management','MGT',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,2),
	(6,1,1,'Core','CRE',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(7,1,1,'Product','PDT',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(8,1,1,'Operation','OPR',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(9,1,1,'Monitoring','MTR',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,2);

/*!40000 ALTER TABLE `tbl_department` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_developed
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_developed`;

CREATE TABLE `tbl_developed` (
  `dvp_id` int unsigned NOT NULL AUTO_INCREMENT,
  `dvp_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `dvp_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `dvp_desc` int DEFAULT NULL,
  `dvp_datetime` int NOT NULL DEFAULT '0',
  `dvp_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `dvp_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`dvp_id`),
  KEY `query_index` (`dvp_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_developed` WRITE;
/*!40000 ALTER TABLE `tbl_developed` DISABLE KEYS */;

INSERT INTO `tbl_developed` (`dvp_id`, `dvp_name`, `dvp_code`, `dvp_desc`, `dvp_datetime`, `dvp_status`, `dvp_update_adu_id`)
VALUES
	(1,'Internally Developed',NULL,NULL,0,1,NULL),
	(2,'Outsourced',NULL,NULL,0,1,NULL),
	(3,'Third Party',NULL,NULL,0,1,NULL),
	(4,'Purchased',NULL,NULL,0,1,NULL),
	(5,'Contractor Developed',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_developed` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_interface
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_interface`;

CREATE TABLE `tbl_interface` (
  `itf_id` int unsigned NOT NULL AUTO_INCREMENT,
  `itf_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `itf_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `itf_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `itf_datetime` int NOT NULL DEFAULT '0',
  `itf_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `itf_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`itf_id`),
  KEY `query_index` (`itf_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_level`;

CREATE TABLE `tbl_level` (
  `lvl_id` int unsigned NOT NULL AUTO_INCREMENT,
  `lvl_name` varchar(255) NOT NULL DEFAULT '',
  `lvl_code` varchar(100) DEFAULT NULL,
  `lvl_datetime` int NOT NULL DEFAULT '0',
  `lvl_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `lvl_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`lvl_id`),
  KEY `query_index` (`lvl_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_level` WRITE;
/*!40000 ALTER TABLE `tbl_level` DISABLE KEYS */;

INSERT INTO `tbl_level` (`lvl_id`, `lvl_name`, `lvl_code`, `lvl_datetime`, `lvl_status`, `lvl_update_adu_id`)
VALUES
	(1,'Administrator','ADM',1419178562,1,NULL),
	(2,'Management','MGM',1419178562,1,NULL),
	(3,'Developer','DEV',1419178562,1,NULL),
	(4,'Operator','OPR',1419178562,1,NULL),
	(5,'Guest','GUT',1445342840,1,8);

/*!40000 ALTER TABLE `tbl_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_lifecycle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_lifecycle`;

CREATE TABLE `tbl_lifecycle` (
  `lfc_id` int unsigned NOT NULL AUTO_INCREMENT,
  `lfc_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `lfc_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `lfc_desc` text,
  `lfc_datetime` int NOT NULL DEFAULT '0',
  `lfc_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `lfc_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`lfc_id`),
  KEY `query_index` (`lfc_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_lifecycle` WRITE;
/*!40000 ALTER TABLE `tbl_lifecycle` DISABLE KEYS */;

INSERT INTO `tbl_lifecycle` (`lfc_id`, `lfc_name`, `lfc_code`, `lfc_desc`, `lfc_datetime`, `lfc_status`, `lfc_update_adu_id`)
VALUES
	(1,'Production',NULL,NULL,0,1,NULL),
	(2,'Contruction',NULL,NULL,0,1,NULL),
	(3,'Retritment',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_lifecycle` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_platform
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_platform`;

CREATE TABLE `tbl_platform` (
  `plf_id` int unsigned NOT NULL AUTO_INCREMENT,
  `plf_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `plf_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `plf_desc` text,
  `plf_datetime` int NOT NULL DEFAULT '0',
  `plf_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `plf_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`plf_id`),
  KEY `query_index` (`plf_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_platform` WRITE;
/*!40000 ALTER TABLE `tbl_platform` DISABLE KEYS */;

INSERT INTO `tbl_platform` (`plf_id`, `plf_name`, `plf_code`, `plf_desc`, `plf_datetime`, `plf_status`, `plf_update_adu_id`)
VALUES
	(1,'API',NULL,NULL,0,1,NULL),
	(2,'Web Apps',NULL,NULL,0,1,NULL),
	(3,'Mobile',NULL,NULL,0,1,NULL),
	(4,'Desktop',NULL,NULL,0,1,NULL),
	(5,'Internet Of Thinks',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_platform` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_project`;

CREATE TABLE `tbl_project` (
  `prj_id` int unsigned NOT NULL AUTO_INCREMENT,
  `prj_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `prj_code` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `prj_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `prj_pm_adu_id` int DEFAULT NULL COMMENT 'Product manager ',
  `prj_tc_adu_id` int DEFAULT NULL COMMENT 'Technical contact',
  `prj_tm_adu_id` int DEFAULT NULL COMMENT 'Team manager ',
  `prj_pjt_id` int DEFAULT NULL COMMENT 'Project Type',
  `prj_svt_id` int DEFAULT NULL COMMENT 'Businness Severity',
  `prj_plf_id` int DEFAULT NULL COMMENT 'Platform',
  `prj_lfc_id` int DEFAULT NULL COMMENT 'Lifecycle',
  `prj_dvp_id` int DEFAULT NULL,
  `prj_datetime` int NOT NULL DEFAULT '0',
  `prj_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `prj_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`prj_id`),
  KEY `query_index` (`prj_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_project` WRITE;
/*!40000 ALTER TABLE `tbl_project` DISABLE KEYS */;

INSERT INTO `tbl_project` (`prj_id`, `prj_name`, `prj_code`, `prj_desc`, `prj_pm_adu_id`, `prj_tc_adu_id`, `prj_tm_adu_id`, `prj_pjt_id`, `prj_svt_id`, `prj_plf_id`, `prj_lfc_id`, `prj_dvp_id`, `prj_datetime`, `prj_status`, `prj_update_adu_id`)
VALUES
	(1,'Altopay','ATY','sadada',1,8,9,1,1,1,1,1,1589374582,1,1);

/*!40000 ALTER TABLE `tbl_project` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_project_regulation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_project_regulation`;

CREATE TABLE `tbl_project_regulation` (
  `pjr_id` int unsigned NOT NULL AUTO_INCREMENT,
  `pjr_prj_id` int NOT NULL,
  `pjr_rgt_id` int DEFAULT NULL,
  `pjr_datetime` int NOT NULL DEFAULT '0',
  `pjr_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`pjr_id`),
  KEY `query_index` (`pjr_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_project_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_project_type`;

CREATE TABLE `tbl_project_type` (
  `pjt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `pjt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `pjt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pjt_desc` text,
  `pjt_datetime` int NOT NULL DEFAULT '0',
  `pjt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `pjt_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`pjt_id`),
  KEY `query_index` (`pjt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_project_type` WRITE;
/*!40000 ALTER TABLE `tbl_project_type` DISABLE KEYS */;

INSERT INTO `tbl_project_type` (`pjt_id`, `pjt_name`, `pjt_code`, `pjt_desc`, `pjt_datetime`, `pjt_status`, `pjt_update_adu_id`)
VALUES
	(1,'Reasearch And Development',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_project_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_regulation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_regulation`;

CREATE TABLE `tbl_regulation` (
  `rgt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `rgt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `rgt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `rgt_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `rgt_datetime` int NOT NULL DEFAULT '0',
  `rgt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `rgt_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`rgt_id`),
  KEY `query_index` (`rgt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_regulation` WRITE;
/*!40000 ALTER TABLE `tbl_regulation` DISABLE KEYS */;

INSERT INTO `tbl_regulation` (`rgt_id`, `rgt_name`, `rgt_code`, `rgt_desc`, `rgt_datetime`, `rgt_status`, `rgt_update_adu_id`)
VALUES
	(1,'BI',NULL,NULL,0,1,NULL),
	(2,'PCI-DSS',NULL,NULL,0,1,NULL),
	(3,'ISO 27001',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_regulation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_repository
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_repository`;

CREATE TABLE `tbl_repository` (
  `rpy_id` int unsigned NOT NULL AUTO_INCREMENT,
  `rpy_prj_id` int DEFAULT NULL,
  `rpy_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `rpy_code` varchar(5) DEFAULT NULL,
  `rpy_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `rpy_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `rpy_datetime` int NOT NULL DEFAULT '0',
  `rpy_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `rpy_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`rpy_id`),
  KEY `query_index` (`rpy_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_repository` WRITE;
/*!40000 ALTER TABLE `tbl_repository` DISABLE KEYS */;

INSERT INTO `tbl_repository` (`rpy_id`, `rpy_prj_id`, `rpy_name`, `rpy_code`, `rpy_desc`, `rpy_url`, `rpy_datetime`, `rpy_status`, `rpy_update_adu_id`)
VALUES
	(1,1,'Disbursment','DBV','hjhjkhk','https://local.bitbucket.com/dis.git',1588830071,1,1);

/*!40000 ALTER TABLE `tbl_repository` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_sectest_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sectest_type`;

CREATE TABLE `tbl_sectest_type` (
  `stt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `stt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `stt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `stt_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `stt_collor_code` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `stt_sla` int DEFAULT NULL,
  `stt_datetime` int NOT NULL DEFAULT '0',
  `stt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`stt_id`),
  KEY `query_index` (`stt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_sectest_type` WRITE;
/*!40000 ALTER TABLE `tbl_sectest_type` DISABLE KEYS */;

INSERT INTO `tbl_sectest_type` (`stt_id`, `stt_name`, `stt_code`, `stt_desc`, `stt_collor_code`, `stt_sla`, `stt_datetime`, `stt_status`)
VALUES
	(1,'Static Code Analisys','SCA',NULL,NULL,NULL,0,1),
	(2,'Static Application Security Testing','SAST',NULL,NULL,NULL,0,1),
	(3,'Static Application Security Testing','DAST',NULL,NULL,NULL,0,1),
	(4,'Runtime Application Protection Security Testing','RAPST',NULL,NULL,NULL,0,1),
	(5,'Penetration Testing','PT',NULL,NULL,NULL,0,1),
	(6,'Vulnerebility Assessment','VA',NULL,NULL,NULL,0,1);

/*!40000 ALTER TABLE `tbl_sectest_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_session`;

CREATE TABLE `tbl_session` (
  `ses_id` int NOT NULL AUTO_INCREMENT,
  `ses_sest_id` int DEFAULT '1' COMMENT 'tbl_session_type',
  `ses_usr_id` int DEFAULT NULL,
  `ses_anr_id` int DEFAULT NULL,
  `ses_key` varchar(255) DEFAULT NULL,
  `ses_activity` int DEFAULT NULL,
  `ses_valid` int DEFAULT NULL,
  `ses_last_ip` varchar(100) DEFAULT NULL,
  `ses_lang_id` int DEFAULT '1',
  `ses_create_datetime` int DEFAULT NULL,
  `ses_app_type` int DEFAULT '0' COMMENT '0 = web, 1 = mobile',
  `ses_dvc_id` int DEFAULT '0' COMMENT '0= not set, 1=Ios, 2=Android',
  `ses_device_id` varchar(255) DEFAULT '',
  `ses_time_zone_name` varchar(100) DEFAULT NULL,
  `ses_time_zone_gmt` varchar(100) DEFAULT NULL,
  `ses_time_zone_offset` varchar(100) DEFAULT NULL,
  `ses_device_name` text,
  `ses_socket_key` varchar(255) DEFAULT NULL,
  `ses_datetime_socket_connect` int DEFAULT NULL,
  `ses_datetime_socket_disconect` int DEFAULT NULL,
  `ses_socket_status` int DEFAULT '3' COMMENT '1=connect, 2= online 3= ofline, 4= disconect',
  PRIMARY KEY (`ses_id`),
  UNIQUE KEY `unique_index` (`ses_id`,`ses_sest_id`,`ses_usr_id`,`ses_anr_id`,`ses_activity`,`ses_dvc_id`,`ses_socket_key`,`ses_socket_status`),
  KEY `ses_usr_id` (`ses_usr_id`,`ses_key`,`ses_activity`,`ses_valid`),
  KEY `index_table` (`ses_socket_key`),
  KEY `index_ses_key` (`ses_key`),
  KEY `index_device_register_id` (`ses_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_session` WRITE;
/*!40000 ALTER TABLE `tbl_session` DISABLE KEYS */;

INSERT INTO `tbl_session` (`ses_id`, `ses_sest_id`, `ses_usr_id`, `ses_anr_id`, `ses_key`, `ses_activity`, `ses_valid`, `ses_last_ip`, `ses_lang_id`, `ses_create_datetime`, `ses_app_type`, `ses_dvc_id`, `ses_device_id`, `ses_time_zone_name`, `ses_time_zone_gmt`, `ses_time_zone_offset`, `ses_device_name`, `ses_socket_key`, `ses_datetime_socket_connect`, `ses_datetime_socket_disconect`, `ses_socket_status`)
VALUES
	(1,2,1,NULL,'$2y$10$Vm3fvr9VkrEYVj5aYKzfseiSqeSQCDBKgnMhopc5ESUWGjjWsirSW',1496981800,1528517800,'127.0.0.1',1,1496981800,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(3,2,1,NULL,'$2y$10$v5aFQMiK1NDbnwgWKSxH6OAwHZ03vaEDDY71MYvjR1PK9hjEZhYIi',1496981965,1528517965,'127.0.0.1',1,1496981965,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(4,2,1,NULL,'$2y$10$sBoTGUs/P/hoycC6t6vaN.y2LbIyWbTciI47GODTwn6vOsG7yRAZu',1588352973,1619888973,'127.0.0.1',1,1588352973,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(5,2,1,NULL,'$2y$10$n6qZ1pQj3ctoaUysbCQJced0brcYkV0bXEjEROxR5A3/brA6p2MC6',1588410649,1619946649,'127.0.0.1',1,1588410649,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(6,2,1,NULL,'$2y$10$IKrK9UCMc1lsNld5DlEt3.Xz3kSGFjaLkVK2qTgKyNkkuXeHC62mi',1588410786,1619946786,'127.0.0.1',1,1588410786,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(7,2,1,NULL,'$2y$10$0AAQrnMSdBuqZ7HL8lfZQOzuodZ6b0hZ8OYr5q56Iex.Yx69ac0Om',1588438027,1619974027,'127.0.0.1',1,1588438027,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(8,2,1,NULL,'$2y$10$gGDt6MB17BMY0WlKQ0OebeB8TtZKX9PVPfT.KsCeDcinWYchzXOcO',1589050886,1620586886,'127.0.0.1',1,1589050886,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(9,2,1,NULL,'$2y$10$xlRWRDO94ZG1SYtOldQPwu3TD5yRqKZ8UszoCzuogBi4zFHyKUj0K',1589052642,1620588642,'127.0.0.1',1,1589052642,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(10,2,1,NULL,'$2y$10$iFA5ygWx/nzIfjQ4YS.wzOpRSxdfX2EIFEPypcfkLyIZdAI/0r/N6',1589052900,1620588900,'127.0.0.1',1,1589052900,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(11,2,1,NULL,'$2y$10$y.T2UygjGeD5Dw2zg9Rj/uzvSF9d2VZQcykbzKdoba6U10pEfOLH6',1589052973,1620588973,'127.0.0.1',1,1589052973,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3);

/*!40000 ALTER TABLE `tbl_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_session_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_session_type`;

CREATE TABLE `tbl_session_type` (
  `sest_id` int unsigned NOT NULL AUTO_INCREMENT,
  `sest_name` varchar(255) DEFAULT NULL,
  `sest_desck` text,
  `sest_datetime` int DEFAULT NULL,
  `sest_datetime_insert` int DEFAULT NULL,
  `sest_datetime_update` int DEFAULT NULL,
  `sest_datetime_delete` int DEFAULT NULL,
  `sest_adu_id_insert` int DEFAULT NULL,
  `sest_adu_id_update` int DEFAULT NULL,
  `sest_adu_id_delete` int DEFAULT NULL,
  `sest_status` int DEFAULT '1' COMMENT '0=Inactive, 1=Active, 2=Deleted',
  PRIMARY KEY (`sest_id`),
  KEY `query_index` (`sest_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_session_type` WRITE;
/*!40000 ALTER TABLE `tbl_session_type` DISABLE KEYS */;

INSERT INTO `tbl_session_type` (`sest_id`, `sest_name`, `sest_desck`, `sest_datetime`, `sest_datetime_insert`, `sest_datetime_update`, `sest_datetime_delete`, `sest_adu_id_insert`, `sest_adu_id_update`, `sest_adu_id_delete`, `sest_status`)
VALUES
	(1,'Api',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(2,'Admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);

/*!40000 ALTER TABLE `tbl_session_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_severity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_severity`;

CREATE TABLE `tbl_severity` (
  `svt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `svt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `svt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `sct_desc` text,
  `svt_collor_code` varchar(8) DEFAULT NULL,
  `svt_sla` int DEFAULT NULL,
  `svt_datetime` int NOT NULL DEFAULT '0',
  `svt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `svt_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`svt_id`),
  KEY `query_index` (`svt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_severity` WRITE;
/*!40000 ALTER TABLE `tbl_severity` DISABLE KEYS */;

INSERT INTO `tbl_severity` (`svt_id`, `svt_name`, `svt_code`, `sct_desc`, `svt_collor_code`, `svt_sla`, `svt_datetime`, `svt_status`, `svt_update_adu_id`)
VALUES
	(1,'Critical','',NULL,'C70039',7,0,1,NULL),
	(2,'Hight','',NULL,'ff3366',30,0,1,NULL),
	(3,'Medium','','','FF5732',90,0,1,NULL),
	(4,'Low','',NULL,'FFC300',120,0,1,NULL),
	(5,'Info','',NULL,'8cdf02',400,0,0,NULL);

/*!40000 ALTER TABLE `tbl_severity` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
