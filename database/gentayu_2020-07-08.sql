# ************************************************************
# Sequel Pro SQL dump
# Version 5446
#
# https://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 8.0.20)
# Database: gentayu
# Generation Time: 2020-07-08 00:31:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
SET NAMES utf8mb4;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table tbl_admin_group_menu_access
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_group_menu_access`;

CREATE TABLE `tbl_admin_group_menu_access` (
  `aga_id` int unsigned NOT NULL AUTO_INCREMENT,
  `aga_aug_id` int DEFAULT NULL,
  `aga_asm_id` int DEFAULT NULL,
  `aga_access` int NOT NULL DEFAULT '1',
  PRIMARY KEY (`aga_id`),
  KEY `query_index` (`aga_aug_id`,`aga_asm_id`,`aga_access`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_admin_group_menu_access` WRITE;
/*!40000 ALTER TABLE `tbl_admin_group_menu_access` DISABLE KEYS */;

INSERT INTO `tbl_admin_group_menu_access` (`aga_id`, `aga_aug_id`, `aga_asm_id`, `aga_access`)
VALUES
	(1,1,1,1),
	(6,1,2,1),
	(9,1,3,1),
	(10,1,4,1),
	(11,1,5,1),
	(12,1,6,1),
	(21,1,7,1),
	(22,1,8,1),
	(23,1,9,1),
	(30,1,10,1),
	(31,1,11,1),
	(36,1,13,1),
	(39,1,14,1),
	(40,1,15,1),
	(38,1,16,1),
	(2,2,1,1),
	(7,2,2,1),
	(13,2,3,1),
	(14,2,4,1),
	(15,2,5,1),
	(16,2,6,1),
	(24,2,7,1),
	(25,2,8,1),
	(26,2,9,1),
	(32,2,10,1),
	(33,2,11,1),
	(37,2,13,1),
	(41,2,14,1),
	(42,2,15,1),
	(43,2,16,1),
	(3,3,1,1),
	(8,3,2,1),
	(17,3,3,1),
	(18,3,4,1),
	(19,3,5,1),
	(20,3,6,1),
	(27,3,7,1),
	(28,3,8,1),
	(29,3,9,1),
	(34,3,10,1),
	(35,3,11,1),
	(44,3,14,1),
	(45,3,15,1),
	(46,3,16,1),
	(4,4,1,1),
	(5,5,1,1);

/*!40000 ALTER TABLE `tbl_admin_group_menu_access` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_admin_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_session`;

CREATE TABLE `tbl_admin_session` (
  `ses_id` int NOT NULL AUTO_INCREMENT,
  `ses_usr_id` int DEFAULT NULL,
  `ses_anr_id` int DEFAULT NULL,
  `ses_key` varchar(255) DEFAULT NULL,
  `ses_activity` int DEFAULT NULL,
  `ses_valid` int DEFAULT NULL,
  `ses_last_ip` varchar(100) DEFAULT NULL,
  `ses_lang_id` int DEFAULT '1',
  `ses_create_datetime` int DEFAULT NULL,
  `ses_app_type` int DEFAULT '0' COMMENT '0 = web, 1 = mobile',
  `ses_dvc_id` int DEFAULT '0' COMMENT '0= not set, 1=Ios, 2=Android',
  `ses_device_id` text,
  `ses_time_zone_name` varchar(100) DEFAULT NULL,
  `ses_time_zone_gmt` varchar(100) DEFAULT NULL,
  `ses_time_zone_offset` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`ses_id`),
  KEY `ses_usr_id` (`ses_usr_id`,`ses_key`,`ses_activity`,`ses_valid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_admin_user
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_user`;

CREATE TABLE `tbl_admin_user` (
  `adu_id` int unsigned NOT NULL AUTO_INCREMENT,
  `adu_com_id` int NOT NULL DEFAULT '0',
  `adu_usr_id` int DEFAULT NULL,
  `adu_aug_id` int NOT NULL DEFAULT '0',
  `adu_dpt_id` int NOT NULL DEFAULT '0',
  `adu_lvl_id` int NOT NULL DEFAULT '1',
  `adu_zon_id` int DEFAULT '1',
  `adu_username` varchar(255) NOT NULL DEFAULT '',
  `adu_first_name` varchar(255) DEFAULT NULL,
  `adu_last_name` varchar(255) DEFAULT NULL,
  `adu_screen_name` varchar(255) DEFAULT NULL,
  `adu_auth_key` varchar(255) NOT NULL DEFAULT '',
  `adu_image` varchar(255) DEFAULT NULL,
  `adu_password_hash` varchar(255) NOT NULL DEFAULT '',
  `adu_password_reset_token` varchar(255) DEFAULT NULL,
  `adu_password_md5` varchar(255) DEFAULT NULL,
  `adu_email` varchar(255) NOT NULL DEFAULT '',
  `adu_datetime` int DEFAULT '0',
  `adu_create_time` int NOT NULL DEFAULT '0',
  `adu_update_time` int NOT NULL DEFAULT '0',
  `adu_delete_time` int DEFAULT '0',
  `adu_create_adu_id` int DEFAULT '0',
  `adu_update_adu_id` int DEFAULT '0',
  `adu_delete_adu_id` int DEFAULT '0',
  `adu_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active, 2=Deleted',
  PRIMARY KEY (`adu_id`),
  KEY `query_index` (`adu_com_id`,`adu_usr_id`,`adu_aug_id`,`adu_dpt_id`,`adu_lvl_id`,`adu_status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `tbl_admin_user` WRITE;
/*!40000 ALTER TABLE `tbl_admin_user` DISABLE KEYS */;

INSERT INTO `tbl_admin_user` (`adu_id`, `adu_com_id`, `adu_usr_id`, `adu_aug_id`, `adu_dpt_id`, `adu_lvl_id`, `adu_zon_id`, `adu_username`, `adu_first_name`, `adu_last_name`, `adu_screen_name`, `adu_auth_key`, `adu_image`, `adu_password_hash`, `adu_password_reset_token`, `adu_password_md5`, `adu_email`, `adu_datetime`, `adu_create_time`, `adu_update_time`, `adu_delete_time`, `adu_create_adu_id`, `adu_update_adu_id`, `adu_delete_adu_id`, `adu_status`)
VALUES
	(1,1,NULL,1,1,1,1,'sandyQx@gmail.com','Sandi','Ardyansyah','Sandi Ardyansyah','JEKEK83EIT6u5_fy-U9aSGyJFoPnMQQF','usr_1427513098.jpg','$2y$13$Fb18QCLHfLSD5Sf2rRCOgOnyMKfu.cJOCACqhNkzBSoavvXzDuAUi','ZpQw_TiCOzxresmZOCPw3nfdg2v55eWt_1433266234','e10adc3949ba59abbe56e057f20f883e','sandyQx@gmail.com',NULL,1419178562,1419215092,NULL,NULL,NULL,NULL,1),
	(8,1,NULL,3,2,1,1,'mamang@gmail.com','Mamang','Suramang','Mamang Suramang','wIGSUD5rffglODfPTC-8KdTbH2chS-pj','usr_1435834536.jpg','$2y$13$SK7KwuneD4J6vI5Kf55HYehWQnkiD/4f4CwSdqxULyf.YspNrh5nO','aT95TFy4F-BioTTJbr1ODSi4uKph6-oi_1496928030','e10adc3949ba59abbe56e057f20f883e','mamang@gmail.com',0,1428560068,0,0,0,0,0,1),
	(9,1,NULL,2,2,1,1,'user@gmail.com','user','biasa','user biasa','FM4FuW6rBD2fq_aCTeeZqzDsmtdGjyjE','','$2y$13$lOWFUbTKxivS3kUJNyli2.dvz4kzNUkfoHNoP44Bn2KOZ5QNAir1O','DvSboojlc2VYKpsEbELbw79djqR5TG5Q_1479543922',NULL,'user@gmail.com',0,1428566472,0,0,0,0,0,1),
	(10,1,NULL,5,7,3,1,'people@gmail.com','orang','basa','orang basa','PUhqQ-9C3D1kJlZJjmXcG0_fCQzaYDlj','','','qUFoybiIvm3Pwzy8j2OuEE9qey3SW3kh_1428566533',NULL,'people@gmail.com',0,1428566533,0,1481692336,0,0,1,1);

/*!40000 ALTER TABLE `tbl_admin_user` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_admin_user_group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_user_group`;

CREATE TABLE `tbl_admin_user_group` (
  `aug_id` int unsigned NOT NULL AUTO_INCREMENT,
  `aug_name` varchar(255) DEFAULT NULL,
  `aug_code` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT '',
  `aug_desck` text,
  `aug_datetime` int DEFAULT NULL,
  `aug_datetime_insert` int DEFAULT NULL,
  `aug_datetime_update` int DEFAULT NULL,
  `aug_datetime_delete` int DEFAULT NULL,
  `aug_adu_id_insert` int DEFAULT NULL,
  `aug_adu_id_update` int DEFAULT NULL,
  `aug_adu_id_delete` int DEFAULT NULL,
  `aug_status` int DEFAULT '1' COMMENT '0=Inactive, 1=Active, 2=Deleted',
  PRIMARY KEY (`aug_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_admin_user_group` WRITE;
/*!40000 ALTER TABLE `tbl_admin_user_group` DISABLE KEYS */;

INSERT INTO `tbl_admin_user_group` (`aug_id`, `aug_name`, `aug_code`, `aug_desck`, `aug_datetime`, `aug_datetime_insert`, `aug_datetime_update`, `aug_datetime_delete`, `aug_adu_id_insert`, `aug_adu_id_update`, `aug_adu_id_delete`, `aug_status`)
VALUES
	(1,'Super Hero','SHR','This is to manage all dashboard',1588800056,1427559909,1588800056,NULL,1,1,NULL,1),
	(2,'Administrator','ADM','login group for all vendor partner',1588800051,1427560045,1588800051,NULL,1,1,NULL,1),
	(3,'Owner','OWN','',1479102396,NULL,1479102396,NULL,NULL,1,NULL,1),
	(4,'Developer','DEV','',1588505803,NULL,1588505803,NULL,NULL,1,NULL,1),
	(5,'Operator-Admin','OPR','',1479102426,NULL,1479102426,NULL,NULL,1,NULL,1),
	(6,'Partner','PTR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(7,'Vendor','VDR',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);

/*!40000 ALTER TABLE `tbl_admin_user_group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_admin_user_menu
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_admin_user_menu`;

CREATE TABLE `tbl_admin_user_menu` (
  `asm_id` int unsigned NOT NULL AUTO_INCREMENT,
  `asm_parent_id` int DEFAULT '0',
  `asm_name` varchar(255) DEFAULT NULL,
  `asm_icon` varchar(255) DEFAULT NULL,
  `asm_url` varchar(255) DEFAULT NULL,
  `asm_have_child` int DEFAULT '0',
  `asm_sort` int DEFAULT '0',
  `asm_status` int DEFAULT '1',
  PRIMARY KEY (`asm_id`),
  KEY `query_index` (`asm_parent_id`,`asm_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_admin_user_menu` WRITE;
/*!40000 ALTER TABLE `tbl_admin_user_menu` DISABLE KEYS */;

INSERT INTO `tbl_admin_user_menu` (`asm_id`, `asm_parent_id`, `asm_name`, `asm_icon`, `asm_url`, `asm_have_child`, `asm_sort`, `asm_status`)
VALUES
	(1,0,'Dashboard','fa-dashboard','dashboard/index',0,1,1),
	(2,0,'RBAC','fa-gear','#',1,20,1),
	(3,2,'Module List','fa-group','rbac/default/index',0,1,1),
	(4,2,'Access Group','fa-users','rbac/group/index',0,2,1),
	(5,2,'Users','fa-bar-chart-o','rbac/users/index',0,3,1),
	(6,2,'Access Permision','fa-sitemap','rbac/access-permision/index',0,4,1),
	(7,0,'Project','fa-money','#',1,2,1),
	(8,0,'Vulnerability','fa-bug','design',1,4,1),
	(9,0,'DevOps','fa-sitemap','patent',0,5,1),
	(10,7,'All Project','fa-gavel','projects/project',0,1,1),
	(11,7,'Repository','fa-bar-chart-o','projects/repository',0,2,1),
	(13,8,'Assessment','','vulnerability/assessment',0,1,1),
	(14,0,'Assessment','fa-cogs','#',1,3,1),
	(15,14,'All Assessment','','assessment',0,1,1),
	(16,14,'Assessment Test','','assessment/test',0,2,1);

/*!40000 ALTER TABLE `tbl_admin_user_menu` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_department
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_department`;

CREATE TABLE `tbl_department` (
  `dpt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `dpt_com_id` int DEFAULT NULL,
  `dpt_cot_id` int DEFAULT NULL,
  `dpt_name` varchar(255) DEFAULT NULL,
  `dpt_code` varchar(255) DEFAULT NULL,
  `dpt_datetime` int DEFAULT NULL,
  `dpt_datetime_insert` int DEFAULT NULL,
  `dpt_datetime_update` int DEFAULT NULL,
  `dpt_datetime_delete` int DEFAULT NULL,
  `dpt_adu_id_insert` int DEFAULT NULL,
  `dpt_adu_id_update` int DEFAULT NULL,
  `dpt_adu_id_delete` int DEFAULT NULL,
  `dpt_status` int DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`dpt_id`),
  KEY `query_index` (`dpt_com_id`,`dpt_cot_id`,`dpt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_department` WRITE;
/*!40000 ALTER TABLE `tbl_department` DISABLE KEYS */;

INSERT INTO `tbl_department` (`dpt_id`, `dpt_com_id`, `dpt_cot_id`, `dpt_name`, `dpt_code`, `dpt_datetime`, `dpt_datetime_insert`, `dpt_datetime_update`, `dpt_datetime_delete`, `dpt_adu_id_insert`, `dpt_adu_id_update`, `dpt_adu_id_delete`, `dpt_status`)
VALUES
	(1,1,1,'Security','SEC',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(2,1,1,'DevOps','DOS',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(3,1,1,'Development','DEV',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(4,1,1,'Digital','DIG',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(5,1,1,'Management','MGT',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,2),
	(6,1,1,'Core','CRE',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(7,1,1,'Product','PDT',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(8,1,1,'Operation','OPR',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(9,1,1,'Monitoring','MTR',1419178562,NULL,NULL,NULL,NULL,NULL,NULL,2);

/*!40000 ALTER TABLE `tbl_department` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_developed
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_developed`;

CREATE TABLE `tbl_developed` (
  `dvp_id` int unsigned NOT NULL AUTO_INCREMENT,
  `dvp_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `dvp_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `dvp_desc` int DEFAULT NULL,
  `dvp_datetime` int NOT NULL DEFAULT '0',
  `dvp_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `dvp_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`dvp_id`),
  KEY `query_index` (`dvp_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_developed` WRITE;
/*!40000 ALTER TABLE `tbl_developed` DISABLE KEYS */;

INSERT INTO `tbl_developed` (`dvp_id`, `dvp_name`, `dvp_code`, `dvp_desc`, `dvp_datetime`, `dvp_status`, `dvp_update_adu_id`)
VALUES
	(1,'Internally Developed',NULL,NULL,0,1,NULL),
	(2,'Outsourced',NULL,NULL,0,1,NULL),
	(3,'Third Party',NULL,NULL,0,1,NULL),
	(4,'Purchased',NULL,NULL,0,1,NULL),
	(5,'Contractor Developed',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_developed` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_finding
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_finding`;

CREATE TABLE `tbl_finding` (
  `fdg_id` int NOT NULL AUTO_INCREMENT,
  `fdg_prj_id` int DEFAULT NULL COMMENT 'tbl_project',
  `fdg_pas_id` int DEFAULT NULL COMMENT 'tbl_project_assessment',
  `fdg_pat_id` int DEFAULT NULL COMMENT 'tbl_project_assessment_test',
  `fdg_plf_id` int DEFAULT NULL COMMENT 'tbl_platform',
  `fdg_stt_id` int NOT NULL COMMENT 'tbl_sectest_type',
  `fdg_sts_id` int DEFAULT NULL COMMENT 'tbl_sectest_tools',
  `fdg_fdt_id` int DEFAULT NULL COMMENT 'tbl_finding_type',
  `fdg_svt_id` int NOT NULL COMMENT 'tbl_severity',
  `fdg_title` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fdg_date` date NOT NULL,
  `fdg_cwe` int DEFAULT NULL,
  `fdg_cve` varchar(28) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fdg_url` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_desc` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fdg_mitigation` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fdg_impact` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `fdg_steps_to_reproduce` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_severity_justification` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_refs` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_duplicate_fdg_id` int NOT NULL,
  `fdg_is_out_of_scope` int NOT NULL,
  `fdg_mitigated` datetime(6) DEFAULT NULL,
  `fdg_last_reviewed` datetime(6) DEFAULT NULL,
  `fdg_line_number` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `fdg_sourcefilepath` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_sourcefile` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_param` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_payload` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_hash_code` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `fdg_line` int DEFAULT NULL,
  `fdg_file_path` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci,
  `created` datetime(6) DEFAULT NULL,
  `is_mitigated` tinyint(1) NOT NULL,
  PRIMARY KEY (`fdg_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table tbl_finding_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_finding_type`;

CREATE TABLE `tbl_finding_type` (
  `fdt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `fdt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `fdt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fdt_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `fdt_collor_code` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `fdt_datetime` int NOT NULL DEFAULT '0',
  `fdt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`fdt_id`),
  KEY `query_index` (`fdt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_finding_type` WRITE;
/*!40000 ALTER TABLE `tbl_finding_type` DISABLE KEYS */;

INSERT INTO `tbl_finding_type` (`fdt_id`, `fdt_name`, `fdt_code`, `fdt_desc`, `fdt_collor_code`, `fdt_datetime`, `fdt_status`)
VALUES
	(1,'False Positive',NULL,NULL,NULL,0,1),
	(2,'False Negative',NULL,NULL,NULL,0,1);

/*!40000 ALTER TABLE `tbl_finding_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_interface
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_interface`;

CREATE TABLE `tbl_interface` (
  `itf_id` int unsigned NOT NULL AUTO_INCREMENT,
  `itf_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `itf_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `itf_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `itf_datetime` int NOT NULL DEFAULT '0',
  `itf_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `itf_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`itf_id`),
  KEY `query_index` (`itf_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_level
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_level`;

CREATE TABLE `tbl_level` (
  `lvl_id` int unsigned NOT NULL AUTO_INCREMENT,
  `lvl_name` varchar(255) NOT NULL DEFAULT '',
  `lvl_code` varchar(100) DEFAULT NULL,
  `lvl_datetime` int NOT NULL DEFAULT '0',
  `lvl_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `lvl_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`lvl_id`),
  KEY `query_index` (`lvl_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_level` WRITE;
/*!40000 ALTER TABLE `tbl_level` DISABLE KEYS */;

INSERT INTO `tbl_level` (`lvl_id`, `lvl_name`, `lvl_code`, `lvl_datetime`, `lvl_status`, `lvl_update_adu_id`)
VALUES
	(1,'Administrator','ADM',1419178562,1,NULL),
	(2,'Management','MGM',1419178562,1,NULL),
	(3,'Developer','DEV',1419178562,1,NULL),
	(4,'Operator','OPR',1419178562,1,NULL),
	(5,'Guest','GUT',1445342840,1,8);

/*!40000 ALTER TABLE `tbl_level` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_lifecycle
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_lifecycle`;

CREATE TABLE `tbl_lifecycle` (
  `lfc_id` int unsigned NOT NULL AUTO_INCREMENT,
  `lfc_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `lfc_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `lfc_desc` text,
  `lfc_datetime` int NOT NULL DEFAULT '0',
  `lfc_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `lfc_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`lfc_id`),
  KEY `query_index` (`lfc_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_lifecycle` WRITE;
/*!40000 ALTER TABLE `tbl_lifecycle` DISABLE KEYS */;

INSERT INTO `tbl_lifecycle` (`lfc_id`, `lfc_name`, `lfc_code`, `lfc_desc`, `lfc_datetime`, `lfc_status`, `lfc_update_adu_id`)
VALUES
	(1,'Production',NULL,NULL,0,1,NULL),
	(2,'Contruction',NULL,NULL,0,1,NULL),
	(3,'Retritment',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_lifecycle` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_platform
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_platform`;

CREATE TABLE `tbl_platform` (
  `plf_id` int unsigned NOT NULL AUTO_INCREMENT,
  `plf_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `plf_code` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `plf_desc` text,
  `plf_datetime` int NOT NULL DEFAULT '0',
  `plf_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `plf_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`plf_id`),
  KEY `query_index` (`plf_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_platform` WRITE;
/*!40000 ALTER TABLE `tbl_platform` DISABLE KEYS */;

INSERT INTO `tbl_platform` (`plf_id`, `plf_name`, `plf_code`, `plf_desc`, `plf_datetime`, `plf_status`, `plf_update_adu_id`)
VALUES
	(1,'API',NULL,NULL,0,1,NULL),
	(2,'Web Apps',NULL,NULL,0,1,NULL),
	(3,'Mobile',NULL,NULL,0,1,NULL),
	(4,'Desktop',NULL,NULL,0,1,NULL),
	(5,'Internet Of Thinks',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_platform` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_project
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_project`;

CREATE TABLE `tbl_project` (
  `prj_id` int unsigned NOT NULL AUTO_INCREMENT,
  `prj_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `prj_code` varchar(5) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `prj_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `prj_pm_adu_id` int DEFAULT NULL COMMENT 'Product manager ',
  `prj_tc_adu_id` int DEFAULT NULL COMMENT 'Technical contact',
  `prj_tm_adu_id` int DEFAULT NULL COMMENT 'Team manager ',
  `prj_pjt_id` int DEFAULT NULL COMMENT 'Project Type',
  `prj_svt_id` int DEFAULT NULL COMMENT 'Businness Severity',
  `prj_plf_id` int DEFAULT NULL COMMENT 'Platform',
  `prj_lfc_id` int DEFAULT NULL COMMENT 'Lifecycle',
  `prj_dvp_id` int DEFAULT NULL,
  `prj_datetime` int NOT NULL DEFAULT '0',
  `prj_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `prj_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`prj_id`),
  KEY `query_index` (`prj_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_project` WRITE;
/*!40000 ALTER TABLE `tbl_project` DISABLE KEYS */;

INSERT INTO `tbl_project` (`prj_id`, `prj_name`, `prj_code`, `prj_desc`, `prj_pm_adu_id`, `prj_tc_adu_id`, `prj_tm_adu_id`, `prj_pjt_id`, `prj_svt_id`, `prj_plf_id`, `prj_lfc_id`, `prj_dvp_id`, `prj_datetime`, `prj_status`, `prj_update_adu_id`)
VALUES
	(1,'Altopay','ATY','sadada',1,8,9,1,1,1,1,1,1589374582,1,1);

/*!40000 ALTER TABLE `tbl_project` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_project_assessment
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_project_assessment`;

CREATE TABLE `tbl_project_assessment` (
  `pas_id` int unsigned NOT NULL AUTO_INCREMENT,
  `pas_prj_id` int DEFAULT NULL COMMENT 'tbl_project',
  `pas_lfc_id` int DEFAULT NULL COMMENT 'tbl_lifecycle',
  `pas_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `pas_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pas_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `pas_datetime` int NOT NULL DEFAULT '0',
  `pas_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`pas_id`),
  KEY `query_index` (`pas_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_project_assessment_test
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_project_assessment_test`;

CREATE TABLE `tbl_project_assessment_test` (
  `pat_id` int unsigned NOT NULL AUTO_INCREMENT,
  `pat_pas_id` int DEFAULT NULL COMMENT 'tbl_project_assessment',
  `pat_stt_id` int DEFAULT NULL COMMENT 'tbl_sectest_type',
  `pat_sts_id` int DEFAULT NULL COMMENT 'tbl_sectest_tools',
  `pat_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `pat_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pat_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `pat_datetime` int NOT NULL DEFAULT '0',
  `pat_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`pat_id`),
  KEY `query_index` (`pat_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_project_regulation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_project_regulation`;

CREATE TABLE `tbl_project_regulation` (
  `pjr_id` int unsigned NOT NULL AUTO_INCREMENT,
  `pjr_prj_id` int NOT NULL,
  `pjr_rgt_id` int DEFAULT NULL,
  `pjr_datetime` int NOT NULL DEFAULT '0',
  `pjr_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`pjr_id`),
  KEY `query_index` (`pjr_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_project_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_project_type`;

CREATE TABLE `tbl_project_type` (
  `pjt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `pjt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `pjt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `pjt_desc` text,
  `pjt_datetime` int NOT NULL DEFAULT '0',
  `pjt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `pjt_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`pjt_id`),
  KEY `query_index` (`pjt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_project_type` WRITE;
/*!40000 ALTER TABLE `tbl_project_type` DISABLE KEYS */;

INSERT INTO `tbl_project_type` (`pjt_id`, `pjt_name`, `pjt_code`, `pjt_desc`, `pjt_datetime`, `pjt_status`, `pjt_update_adu_id`)
VALUES
	(1,'Reasearch And Development',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_project_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_regulation
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_regulation`;

CREATE TABLE `tbl_regulation` (
  `rgt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `rgt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `rgt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `rgt_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `rgt_datetime` int NOT NULL DEFAULT '0',
  `rgt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `rgt_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`rgt_id`),
  KEY `query_index` (`rgt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_regulation` WRITE;
/*!40000 ALTER TABLE `tbl_regulation` DISABLE KEYS */;

INSERT INTO `tbl_regulation` (`rgt_id`, `rgt_name`, `rgt_code`, `rgt_desc`, `rgt_datetime`, `rgt_status`, `rgt_update_adu_id`)
VALUES
	(1,'BI',NULL,NULL,0,1,NULL),
	(2,'PCI-DSS',NULL,NULL,0,1,NULL),
	(3,'ISO 27001',NULL,NULL,0,1,NULL);

/*!40000 ALTER TABLE `tbl_regulation` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_repository
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_repository`;

CREATE TABLE `tbl_repository` (
  `rpy_id` int unsigned NOT NULL AUTO_INCREMENT,
  `rpy_prj_id` int DEFAULT NULL,
  `rpy_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `rpy_code` varchar(5) DEFAULT NULL,
  `rpy_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `rpy_url` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `rpy_datetime` int NOT NULL DEFAULT '0',
  `rpy_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `rpy_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`rpy_id`),
  KEY `query_index` (`rpy_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_repository` WRITE;
/*!40000 ALTER TABLE `tbl_repository` DISABLE KEYS */;

INSERT INTO `tbl_repository` (`rpy_id`, `rpy_prj_id`, `rpy_name`, `rpy_code`, `rpy_desc`, `rpy_url`, `rpy_datetime`, `rpy_status`, `rpy_update_adu_id`)
VALUES
	(1,1,'Disbursment','DBV','hjhjkhk','https://local.bitbucket.com/dis.git',1588830071,1,1);

/*!40000 ALTER TABLE `tbl_repository` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_sectest_tools
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sectest_tools`;

CREATE TABLE `tbl_sectest_tools` (
  `sts_id` int unsigned NOT NULL AUTO_INCREMENT,
  `sts_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `sts_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `sts_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `sts_collor_code` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `sts_datetime` int NOT NULL DEFAULT '0',
  `sts_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`sts_id`),
  KEY `query_index` (`sts_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



# Dump of table tbl_sectest_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_sectest_type`;

CREATE TABLE `tbl_sectest_type` (
  `stt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `stt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `stt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `stt_desc` text CHARACTER SET latin1 COLLATE latin1_swedish_ci,
  `stt_collor_code` varchar(8) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `stt_sla` int DEFAULT NULL,
  `stt_datetime` int NOT NULL DEFAULT '0',
  `stt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  PRIMARY KEY (`stt_id`),
  KEY `query_index` (`stt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_sectest_type` WRITE;
/*!40000 ALTER TABLE `tbl_sectest_type` DISABLE KEYS */;

INSERT INTO `tbl_sectest_type` (`stt_id`, `stt_name`, `stt_code`, `stt_desc`, `stt_collor_code`, `stt_sla`, `stt_datetime`, `stt_status`)
VALUES
	(1,'Static Code Analisys','SCA',NULL,NULL,NULL,0,1),
	(2,'Static Application Security Testing','SAST',NULL,NULL,NULL,0,1),
	(3,'Static Application Security Testing','DAST',NULL,NULL,NULL,0,1),
	(4,'Runtime Application Protection Security Testing','RAPST',NULL,NULL,NULL,0,1),
	(5,'Penetration Testing','PT',NULL,NULL,NULL,0,1),
	(6,'Vulnerebility Assessment','VA',NULL,NULL,NULL,0,1);

/*!40000 ALTER TABLE `tbl_sectest_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_session
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_session`;

CREATE TABLE `tbl_session` (
  `ses_id` int NOT NULL AUTO_INCREMENT,
  `ses_sest_id` int DEFAULT '1' COMMENT 'tbl_session_type',
  `ses_usr_id` int DEFAULT NULL,
  `ses_anr_id` int DEFAULT NULL,
  `ses_key` varchar(255) DEFAULT NULL,
  `ses_activity` int DEFAULT NULL,
  `ses_valid` int DEFAULT NULL,
  `ses_last_ip` varchar(100) DEFAULT NULL,
  `ses_lang_id` int DEFAULT '1',
  `ses_create_datetime` int DEFAULT NULL,
  `ses_app_type` int DEFAULT '0' COMMENT '0 = web, 1 = mobile',
  `ses_dvc_id` int DEFAULT '0' COMMENT '0= not set, 1=Ios, 2=Android',
  `ses_device_id` varchar(255) DEFAULT '',
  `ses_time_zone_name` varchar(100) DEFAULT NULL,
  `ses_time_zone_gmt` varchar(100) DEFAULT NULL,
  `ses_time_zone_offset` varchar(100) DEFAULT NULL,
  `ses_device_name` text,
  `ses_socket_key` varchar(255) DEFAULT NULL,
  `ses_datetime_socket_connect` int DEFAULT NULL,
  `ses_datetime_socket_disconect` int DEFAULT NULL,
  `ses_socket_status` int DEFAULT '3' COMMENT '1=connect, 2= online 3= ofline, 4= disconect',
  PRIMARY KEY (`ses_id`),
  UNIQUE KEY `unique_index` (`ses_id`,`ses_sest_id`,`ses_usr_id`,`ses_anr_id`,`ses_activity`,`ses_dvc_id`,`ses_socket_key`,`ses_socket_status`),
  KEY `ses_usr_id` (`ses_usr_id`,`ses_key`,`ses_activity`,`ses_valid`),
  KEY `index_table` (`ses_socket_key`),
  KEY `index_ses_key` (`ses_key`),
  KEY `index_device_register_id` (`ses_device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_session` WRITE;
/*!40000 ALTER TABLE `tbl_session` DISABLE KEYS */;

INSERT INTO `tbl_session` (`ses_id`, `ses_sest_id`, `ses_usr_id`, `ses_anr_id`, `ses_key`, `ses_activity`, `ses_valid`, `ses_last_ip`, `ses_lang_id`, `ses_create_datetime`, `ses_app_type`, `ses_dvc_id`, `ses_device_id`, `ses_time_zone_name`, `ses_time_zone_gmt`, `ses_time_zone_offset`, `ses_device_name`, `ses_socket_key`, `ses_datetime_socket_connect`, `ses_datetime_socket_disconect`, `ses_socket_status`)
VALUES
	(1,2,1,NULL,'$2y$10$Vm3fvr9VkrEYVj5aYKzfseiSqeSQCDBKgnMhopc5ESUWGjjWsirSW',1496981800,1528517800,'127.0.0.1',1,1496981800,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(3,2,1,NULL,'$2y$10$v5aFQMiK1NDbnwgWKSxH6OAwHZ03vaEDDY71MYvjR1PK9hjEZhYIi',1496981965,1528517965,'127.0.0.1',1,1496981965,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(4,2,1,NULL,'$2y$10$sBoTGUs/P/hoycC6t6vaN.y2LbIyWbTciI47GODTwn6vOsG7yRAZu',1588352973,1619888973,'127.0.0.1',1,1588352973,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(5,2,1,NULL,'$2y$10$n6qZ1pQj3ctoaUysbCQJced0brcYkV0bXEjEROxR5A3/brA6p2MC6',1588410649,1619946649,'127.0.0.1',1,1588410649,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(6,2,1,NULL,'$2y$10$IKrK9UCMc1lsNld5DlEt3.Xz3kSGFjaLkVK2qTgKyNkkuXeHC62mi',1588410786,1619946786,'127.0.0.1',1,1588410786,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(7,2,1,NULL,'$2y$10$0AAQrnMSdBuqZ7HL8lfZQOzuodZ6b0hZ8OYr5q56Iex.Yx69ac0Om',1588438027,1619974027,'127.0.0.1',1,1588438027,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(8,2,1,NULL,'$2y$10$gGDt6MB17BMY0WlKQ0OebeB8TtZKX9PVPfT.KsCeDcinWYchzXOcO',1589050886,1620586886,'127.0.0.1',1,1589050886,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(9,2,1,NULL,'$2y$10$xlRWRDO94ZG1SYtOldQPwu3TD5yRqKZ8UszoCzuogBi4zFHyKUj0K',1589052642,1620588642,'127.0.0.1',1,1589052642,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(10,2,1,NULL,'$2y$10$iFA5ygWx/nzIfjQ4YS.wzOpRSxdfX2EIFEPypcfkLyIZdAI/0r/N6',1589052900,1620588900,'127.0.0.1',1,1589052900,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(11,2,1,NULL,'$2y$10$y.T2UygjGeD5Dw2zg9Rj/uzvSF9d2VZQcykbzKdoba6U10pEfOLH6',1589052973,1620588973,'127.0.0.1',1,1589052973,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(12,2,1,NULL,'$2y$10$QiTncI9BRCZKA6wvsIxXd.3Tv6/Zxwd4hzeT9FNhzzbh1GELYBnki',1589725099,1621261099,'127.0.0.1',1,1589725099,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(13,2,1,NULL,'$2y$10$secswMWb.kRhZ/gKrnXwNOOwSb1cXqOZJg1utXfWMSqQyV2g.ZYNe',1590083511,1621619511,'127.0.0.1',1,1590083511,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(14,2,1,NULL,'$2y$10$GAYnYdWxOe/WMAPngRx8X.osK2uiK2gKATl4IKicQUKx/rC7vAMsm',1590084163,1621620163,'127.0.0.1',1,1590084163,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(15,2,1,NULL,'$2y$10$5nec.fDfxPKYBKI7YkT1u../40jwYx9Fp/AHvtQ0yyFtcQHSAn31q',1590087020,1621623020,'127.0.0.1',1,1590087020,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(16,2,1,NULL,'$2y$10$rsmOnjFjpnfpixfp6CvAUusUpxKfgn66G/TkDtkckgn3CbQWt8jyO',1590087021,1621623021,'127.0.0.1',1,1590087021,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(17,2,1,NULL,'$2y$10$1gn7XyOfHn.cQ9Ek4PQbbO28RuIavgRtGIwRbNrfDaBeMlgVNoK4O',1590087023,1621623023,'127.0.0.1',1,1590087023,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(18,2,1,NULL,'$2y$10$RtoAOqUFCch9.8P3CRpPD.cFCS8Y1hqc70wCw1C4ZcDXJDqMToo4O',1590087024,1621623024,'127.0.0.1',1,1590087024,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(19,2,1,NULL,'$2y$10$YEomBJ9uL6gKOnlEoCO8eulOmzyL9rkgCXOHCfN6RTW6JTm5IH4aS',1590087025,1621623025,'127.0.0.1',1,1590087025,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(20,2,1,NULL,'$2y$10$p7oJaKwNpa.WYrCKi7Hbj.koZIk5kbJ8HlPYhCa/lEukTX0Dw8sum',1590087027,1621623027,'127.0.0.1',1,1590087027,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(21,2,1,NULL,'$2y$10$hkIOI1grohbZ47GDBdS6.udYnuqASBmUtbFETttuP3hLdJ./RwkTW',1590087028,1621623028,'127.0.0.1',1,1590087028,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(22,2,1,NULL,'$2y$10$d5AAE2lINVjvXR9XeWPh6Ozi1uzAwcUMvY.9TDOiT9JbqG0l2bHgu',1590087030,1621623030,'127.0.0.1',1,1590087030,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(23,2,1,NULL,'$2y$10$t9S27.Wpi37LvT8izTX/euF5D8nYYl/20xmNbPuNoST54VlQlTvO.',1590087031,1621623031,'127.0.0.1',1,1590087031,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(24,2,1,NULL,'$2y$10$eup7wrtjiJXAy6r5R/kURew0GDv/arlwMCLik//OFFqs3e5YBh7Du',1590087033,1621623033,'127.0.0.1',1,1590087033,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(25,2,1,NULL,'$2y$10$luoBpkPoBaPu0hVY..viZOkZxAtCw.8cONHjW15yGVO9OXbGm4lBS',1590087034,1621623034,'127.0.0.1',1,1590087034,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(26,2,1,NULL,'$2y$10$C7/tCsXYMPOr3ZSRcRMYnejTuTY/sbJpC/Ehe0XoTCylGEAErXaue',1590087035,1621623035,'127.0.0.1',1,1590087035,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(27,2,1,NULL,'$2y$10$3aTZD2jyHj2mQZAZ2LK8N.6vSBqNtz7AWkZz0dwBi4WmxjPiXtIYi',1590087037,1621623037,'127.0.0.1',1,1590087037,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(28,2,1,NULL,'$2y$10$8hI/T0PBBZOsScJLG6mr4.4baTOPvG0nGVScDpVqkC4M4HaUctmnG',1590087039,1621623039,'127.0.0.1',1,1590087039,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(29,2,1,NULL,'$2y$10$24Gz2opV.YzzQFyoJ5nI2e0KqWIWwVcKyD6o0j0QYTrrqztrnTzYi',1590087054,1621623054,'127.0.0.1',1,1590087054,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(30,2,1,NULL,'$2y$10$H2thn./zxCl7gPzBqul3Kes0lHhJyOXGXAIi9nBFmlVbXfifTSn/m',1590087056,1621623056,'127.0.0.1',1,1590087056,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(31,2,1,NULL,'$2y$10$htjPiF3W5/EIGjGzLmPxvehUYtKJCULlRIwO2t.YU.pEa.G1VFo1S',1590087059,1621623059,'127.0.0.1',1,1590087059,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(32,2,1,NULL,'$2y$10$dV95I7kNfPILgr/LyYFUYOCW6p6of4ekWUeKQgZ7iJqbC7RjTPGVK',1590087061,1621623061,'127.0.0.1',1,1590087061,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(33,2,1,NULL,'$2y$10$/c1mU3IAlJSryBvbkrUAruxW3L91BKFsnJOR81y4lM4/HTaMTWdLS',1590087063,1621623063,'127.0.0.1',1,1590087063,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(34,2,1,NULL,'$2y$10$ptTzPXiaXKbEDIlmGaNWkeqQBOckPBmdyPw3e1HfdFevRKywYd/V2',1590087065,1621623065,'127.0.0.1',1,1590087065,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(35,2,1,NULL,'$2y$10$I/RhmEN7vUYXTLgMuJFTMexcXMH5w0anbLMvZZ6RrwEOkK.THTfkG',1590087067,1621623067,'127.0.0.1',1,1590087067,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(36,2,1,NULL,'$2y$10$jimoeowONsGwuoCXun3b1Ozh7szq1VmFUWzWn3gHy6Nug5Af7D6Su',1590087070,1621623070,'127.0.0.1',1,1590087070,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(37,2,1,NULL,'$2y$10$wvd1dPRwA8yFGrC4PE7tZu2TnVoZ2nQ9M5hnvEzsSwRUvr0nQUSxS',1590087072,1621623072,'127.0.0.1',1,1590087072,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(38,2,1,NULL,'$2y$10$etFDgqX1NV30Xj7FLB6rWeeuhTbxvdKNmfmFOj9DnSzpHpNHFx5Kq',1590087074,1621623074,'127.0.0.1',1,1590087074,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(39,2,1,NULL,'$2y$10$CHAW/vWhhZ0SIT/iuoLFTO8s0LExJ32uTB0PmjMJLUSvybwkWH0IK',1590087080,1621623080,'127.0.0.1',1,1590087080,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(40,2,1,NULL,'$2y$10$hOa0.RsobaUjOq5ziRLTt.4PJhJpThTbjsNn.UDdKRxuKl1sjpWBy',1590087082,1621623082,'127.0.0.1',1,1590087082,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(41,2,1,NULL,'$2y$10$55cR1QNawkcgNsMda69yG.S7PWjLXwYaM1EYRYt/Z8YyIGG/Wedii',1590087084,1621623084,'127.0.0.1',1,1590087084,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(42,2,1,NULL,'$2y$10$hok6QySs.adLATbPn5ym..knATTuptE2wi4wXApJ/QUIPJB8qnAUW',1590087085,1621623085,'127.0.0.1',1,1590087085,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(43,2,1,NULL,'$2y$10$0QLp4WmuVACsQUy3CrTkxOCeRLNXzn6NV/6SmIB8waffzFz1jkUmC',1590087086,1621623086,'127.0.0.1',1,1590087086,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(44,2,1,NULL,'$2y$10$MMU1tztQyBSMTfjvrMmu9.m1Gw6tydPZkpQbSSZyXj2WdejNssbP2',1590087087,1621623087,'127.0.0.1',1,1590087087,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(45,2,1,NULL,'$2y$10$1bFhHcVzr0WQqc4L4cKf/.nSO41RBrrDVeM3B4ImYZM4X9/v0aqNW',1590087089,1621623089,'127.0.0.1',1,1590087089,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(46,2,1,NULL,'$2y$10$WXto4DsvEDIwQQmY3Fr9uuadcDerOZtFdM1zMLmFddL72u4oscVTe',1590087090,1621623090,'127.0.0.1',1,1590087090,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(47,2,1,NULL,'$2y$10$6p1nb2v8nyqAHAOS5.AVsu0B7lkvZ5yonBTDhpXvSX0QBKeehKm.O',1590087091,1621623091,'127.0.0.1',1,1590087091,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(48,2,1,NULL,'$2y$10$EjYzW0CUJcpKS9aT8g0JgO2o4Ratu0E1DwGouHj77iGtxk1Rufb1u',1590087092,1621623092,'127.0.0.1',1,1590087092,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(49,2,1,NULL,'$2y$10$gdm8EKi9xyR2cDFF792X/u1ghujZL4HCKCBGxMtZnrR6CNlmMckcG',1590087099,1621623099,'127.0.0.1',1,1590087099,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(50,2,1,NULL,'$2y$10$l9mube6x89QCG6af0DznSOks4jjXR5/ojzv1bNzMB81mXDPb2uhzi',1590087100,1621623100,'127.0.0.1',1,1590087100,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(51,2,1,NULL,'$2y$10$K/1C9x/.6OmWCsbxuP0o8eAakGwiUh/fT8noMEgTTAWdf4lK8o99a',1590087101,1621623101,'127.0.0.1',1,1590087101,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(52,2,1,NULL,'$2y$10$wBDOLQTszLfVHjWBg.IAPuXIp4/VOUn7i0Nh2Bvm.b3Fra/uVhubu',1590087103,1621623103,'127.0.0.1',1,1590087103,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(53,2,1,NULL,'$2y$10$LEMiyiEiDzhnRCPifFFcjuTHjoffAG8Kw/c9vHWjWGnmzBADXynAy',1590087104,1621623104,'127.0.0.1',1,1590087104,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(54,2,1,NULL,'$2y$10$2BRWteTnPzIu8x1ZRdQCru5ul7xOZIve6aiCCDYY9wS0Vk2TSH7/m',1590087105,1621623105,'127.0.0.1',1,1590087105,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(55,2,1,NULL,'$2y$10$1brjU8w2o1jMbHXezTySwe1cs0wSglFX1nWg7wnwudbOGxPdWMwpm',1590087107,1621623107,'127.0.0.1',1,1590087107,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(56,2,1,NULL,'$2y$10$H86svv7ROjD/HU62U9AnEO/2FuYTtKcQE598uln0QyFaa9WI3L/nu',1590087108,1621623108,'127.0.0.1',1,1590087108,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(57,2,1,NULL,'$2y$10$EJ/2zS5gx.UfDNAGto4SoO/zeAGhw2SOLiSoF7S/yDOBwhw1nV0tu',1590087113,1621623113,'127.0.0.1',1,1590087113,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(58,2,1,NULL,'$2y$10$zZhGhZnTlEQJl7h3G1HJ1.GDw7pzqLMMRaWPYls1U/Bimi/YHphJm',1590087114,1621623114,'127.0.0.1',1,1590087114,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(59,2,1,NULL,'$2y$10$6BtLqtfq640e4vSJkErVQOCpsLxdz3XZvXHGA883PDfPPxstgnlOS',1590087116,1621623116,'127.0.0.1',1,1590087116,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(60,2,1,NULL,'$2y$10$TKdnwAUdX4jKx74Jfg2sUuUfv/2/JZc4LJ2ht68hzGeqENeXekCMG',1590087117,1621623117,'127.0.0.1',1,1590087117,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(61,2,1,NULL,'$2y$10$i3ZMKWknGJGhn4C/qawNnuSwyYe7Mkz4BqY3/TCdjFKeB6ZZpoaK2',1590087118,1621623118,'127.0.0.1',1,1590087118,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(62,2,1,NULL,'$2y$10$3y7XBa9DCSoRo0QnlrGmbequU6EBJR.QH1h8vOFLoiOvD3ejHW/x2',1590087120,1621623120,'127.0.0.1',1,1590087120,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(63,2,1,NULL,'$2y$10$59mEBPHSftE1iQUmIoSm1uZ8kK3vtSX3m5f4uy2OR/U6TpCJdL.p2',1590087122,1621623122,'127.0.0.1',1,1590087122,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(64,2,1,NULL,'$2y$10$1tlKpfDeNpaKkTcvUEsJGehI286maq7GWK5AAbgXZwQdroiS2xz2C',1590087123,1621623123,'127.0.0.1',1,1590087123,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(65,2,1,NULL,'$2y$10$Ec32v5/dNfxF5bDlT3X7RuVXFz2vEPL3VjR09SCc4LjLSj/UkWRGe',1590087124,1621623124,'127.0.0.1',1,1590087124,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(66,2,1,NULL,'$2y$10$PsejWQNuY4bXtYXzWl76yetVpN1ucUNjV24KEr157witvXwTrVZy6',1590087125,1621623125,'127.0.0.1',1,1590087125,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(67,2,1,NULL,'$2y$10$tmr5r8oFL0lHiw1nTWIv.eZic7KWNvH4XJlINAJLodZxIeHJEhAMW',1590087126,1621623126,'127.0.0.1',1,1590087126,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(68,2,1,NULL,'$2y$10$MHbqL6z0uWDitKUPuujyIu7LeU4.bmsRjGtiwXMrn6lhUl1qYrvTC',1590087129,1621623129,'127.0.0.1',1,1590087129,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(69,2,1,NULL,'$2y$10$4SyADO1CERGL.4YxnPNrq.5.yr1eBaLvsRwA3Svese6YJ725BEQD2',1590087130,1621623130,'127.0.0.1',1,1590087130,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(70,2,1,NULL,'$2y$10$hIx3WasYQRR0boNNEakzHuzUkly1MttNsTU.CvZB/kGbbxfQ/KBWC',1590087131,1621623131,'127.0.0.1',1,1590087131,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(71,2,1,NULL,'$2y$10$UxKPxdlTg6WHCgAdE3c61OLDYIBmKmtAk5H23uLnX1x4BlFZKsuyC',1590087133,1621623133,'127.0.0.1',1,1590087133,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(72,2,1,NULL,'$2y$10$6XgU95.FTrKZr0yEttJUX.UtXq77LAq9MkMfV3fuJgB6ub9AD//iq',1590087134,1621623134,'127.0.0.1',1,1590087134,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(73,2,1,NULL,'$2y$10$Qbf65Gd9aZwCRE36VGPy0.jPqQUmXxKykZ5LD9ARWKd2eQPwZq95K',1590087136,1621623136,'127.0.0.1',1,1590087136,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(74,2,1,NULL,'$2y$10$mY4ooOaSq5ZH268rdTEgjOe7LMfzZzMPEZMUINrUMh/6YL6IbUA1e',1590087147,1621623147,'127.0.0.1',1,1590087147,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(75,2,1,NULL,'$2y$10$5F/Jr7ZkgWRIWNCltWvx9uEupxGpz5hyBebUyHzl03MuNm2TB9Zw.',1590087148,1621623148,'127.0.0.1',1,1590087148,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(76,2,1,NULL,'$2y$10$JnWDrDGVrHTQOk9Y93iR9urrpFe1ojCnKrbosO/ZwRa5fCkVoCcQu',1590087150,1621623150,'127.0.0.1',1,1590087150,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(77,2,1,NULL,'$2y$10$KzDUTJUpXxnTVAjI/jEiLOVOwcMWahAMOkt5mw30.xLbH/xmMuSw2',1590087151,1621623151,'127.0.0.1',1,1590087151,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(78,2,1,NULL,'$2y$10$O9uv3H2EpeCH.ma2ZgOZD.Z6c7RAPyaB5pcTypP7aqgCk6CYww2dm',1590087152,1621623152,'127.0.0.1',1,1590087152,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(79,2,1,NULL,'$2y$10$glOehCXDCiWwZYuc0ORn9OvpnN8am3zrHjNLBZS58BWsAV9IDrqie',1590087153,1621623153,'127.0.0.1',1,1590087153,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(80,2,1,NULL,'$2y$10$m5pxdcHAdhbC1Jch.5/JTehUBubMcK5/28HNRF1p1evzDu/wDLokS',1590087154,1621623154,'127.0.0.1',1,1590087154,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(81,2,1,NULL,'$2y$10$U0qJV1wQj972IlEqGVk53ucqK6H5v1MrJrfM0oTTsUaIUkT.k9COW',1590087155,1621623155,'127.0.0.1',1,1590087155,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(82,2,1,NULL,'$2y$10$y09nGus8CrQDW1Ca6pt7uOHSemQFAx5xM9f/EL3yIc0w8xw9fVBXe',1590087156,1621623156,'127.0.0.1',1,1590087156,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(83,2,1,NULL,'$2y$10$JFx8xjBsoSSJ.rWGq3POhegl8L7Bqbtq0Q5GJ6JJhAyngA8QfD/S.',1590087158,1621623158,'127.0.0.1',1,1590087158,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(84,2,1,NULL,'$2y$10$BwPAQmGTSLzkUIurUhyPhOSr/HTV.50GvUqGY6Z/1SL7SYfkkUR7.',1590087159,1621623159,'127.0.0.1',1,1590087159,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(85,2,1,NULL,'$2y$10$aB.AcZqZQmUIc1qB5R513ONq/PSvN5pTigb48Z2MVPr660ccoreY.',1590087160,1621623160,'127.0.0.1',1,1590087160,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(86,2,1,NULL,'$2y$10$ZaaGGPhdH4cok12VQbPz1.laacXYI29Bqw/27aZC1Nlhx3jEBcZLm',1590087161,1621623161,'127.0.0.1',1,1590087161,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(87,2,1,NULL,'$2y$10$c7vze2SGD2.esHgvnlAKpuSwHBc.kcdJSAovFBu9qEfJv3.TvxXpK',1590087162,1621623162,'127.0.0.1',1,1590087162,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(88,2,1,NULL,'$2y$10$Q1kZpOFCpbuJWTH2JtdANeHHtBYQBe9Xl026KEvk3pbOv9Jza/GWK',1590087163,1621623163,'127.0.0.1',1,1590087163,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(89,2,1,NULL,'$2y$10$B9lYMNHk/5kNHGQcpmXS1O5i6o7/cbsmTYB0ioo99osNNWWWAbVAO',1590087164,1621623164,'127.0.0.1',1,1590087164,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(90,2,1,NULL,'$2y$10$JAhU54T09dHX4g.MOcdpFOoWNNpzyTGBq4AT6syPL01VJz1Ouw6NO',1590087165,1621623165,'127.0.0.1',1,1590087165,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(91,2,1,NULL,'$2y$10$1/OJNEO8F5Sl6RKAlzRJVO.EnxAtLTl06kStU3mp/bos.Z7nTF34C',1590087166,1621623166,'127.0.0.1',1,1590087166,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(92,2,1,NULL,'$2y$10$Ro7FgZJ.dUKxZeVrlCzkMO1Q37E51nATDWasXs.nr9OtQk9FeQ9/6',1590087167,1621623167,'127.0.0.1',1,1590087167,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(93,2,1,NULL,'$2y$10$QQpZZjGhCYsnc1IM0Ntfoeu7sH5hUyMDn02q6UYZNGYse.ymZ3Gku',1590087169,1621623169,'127.0.0.1',1,1590087169,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(94,2,1,NULL,'$2y$10$vQBSxybPKYOw9M0T2yN7d.4eLU1wLt4L8AveNXG.hwjx0acOUHq02',1590087170,1621623170,'127.0.0.1',1,1590087170,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(95,2,1,NULL,'$2y$10$RoKfjkGCi/WXtzig/UTZmeD/.Xpp6ArllxreTkdVg3R/MRrDJm1o.',1590087171,1621623171,'127.0.0.1',1,1590087171,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(96,2,1,NULL,'$2y$10$AIgz2wRSYAfaETgzYTinIOC6Nw.PDjrOmjybcmNiNPgCfXy7N9GA.',1590087172,1621623172,'127.0.0.1',1,1590087172,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(97,2,1,NULL,'$2y$10$CpYXfacXkot6Lvsl5QFqDujsCwqMW0QgFwcLaQPQUGyWLpIKHQjHe',1590087173,1621623173,'127.0.0.1',1,1590087173,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(98,2,1,NULL,'$2y$10$lngVQWRhGnspyjeDOoJg0eRQwqC2T.JIq16SzYKm5tj3lJ39hN.hW',1590087174,1621623174,'127.0.0.1',1,1590087174,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(99,2,1,NULL,'$2y$10$14fGdN3SsptZ7XJ6UeD2d.o7.zvca7K/9sEvpBpgdcsXXPgM4IJ8K',1590087177,1621623177,'127.0.0.1',1,1590087177,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(100,2,1,NULL,'$2y$10$VytJILrSbXf5debShP.TzOMhGUawMksf7hH6hOE3FJWBBBfNiXf4O',1590087178,1621623178,'127.0.0.1',1,1590087178,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(101,2,1,NULL,'$2y$10$.AmGjnGje0V7TJAFQYH60e4Ijb82udQfcpb7PAraSt11dR5usECha',1590087179,1621623179,'127.0.0.1',1,1590087179,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(102,2,1,NULL,'$2y$10$0FQlAg05Vushk6/Hm85p1e2/5i4e2Pw7O8SS.EpdigZSpppDbCjeq',1590087180,1621623180,'127.0.0.1',1,1590087180,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(103,2,1,NULL,'$2y$10$hFpOCsC3I40eGbBcXLNXTeWc88h8h7kFmPSo5diBxGZr9.i3O6yae',1590087181,1621623181,'127.0.0.1',1,1590087181,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(104,2,1,NULL,'$2y$10$QHjpBs0vl5MGcfEQv04UDuV3fxncQFFyhDK7iHrZl5Gg.n0DC0ZTa',1590087182,1621623182,'127.0.0.1',1,1590087182,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(105,2,1,NULL,'$2y$10$aaqkDScUWadyqueyzTgnQukTAtgPkDkCsaXVaySV4oj6bv1cxvXi.',1590087183,1621623183,'127.0.0.1',1,1590087183,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(106,2,1,NULL,'$2y$10$RcH8mhVJPE5EhS/Lyj9.c.Q5ex2cyc9q/tNLBUopAHsp/YooLi8Kq',1590087184,1621623184,'127.0.0.1',1,1590087184,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(107,2,1,NULL,'$2y$10$cLb42kYGPwRkOtkNj9pHJeR9MxNXqqGdF2qJxgf9esHNjwvdSSbU.',1590087186,1621623186,'127.0.0.1',1,1590087186,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(108,2,1,NULL,'$2y$10$.Ayo2QrkdK4k0DfFyWXO5e0xjlU6mOMvCKd0f0VVIzgPKe2jTsVyK',1590087187,1621623187,'127.0.0.1',1,1590087187,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(109,2,1,NULL,'$2y$10$LPnjkIBQtOwl0P0xd5gsbOMoDbidBu6jayL4m4KBpatWhG8mgSiJC',1590087188,1621623188,'127.0.0.1',1,1590087188,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(110,2,1,NULL,'$2y$10$k3Sfuppb2Vh9ssJSD2yup.Zc4jsD2WcZmWIECUqpSMyhNOqHHMsPK',1590087189,1621623189,'127.0.0.1',1,1590087189,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(111,2,1,NULL,'$2y$10$KkXFwZ4JMAPZsdKUrJYXQe6KaYVvJpOk48wzzuR5b94uzDPJCJG4G',1590087190,1621623190,'127.0.0.1',1,1590087190,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(112,2,1,NULL,'$2y$10$y5sKt4hPnevgv0LAo.AFFeLTIUDIo0CQH8bRDQVQ8bD6UKg/GijYu',1590087191,1621623191,'127.0.0.1',1,1590087191,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(113,2,1,NULL,'$2y$10$.eJfOC4O5eOHAU.a/pw.CuJxXqvgEThXyf9O4s6iJ5jTBsWbnOxgy',1590087192,1621623192,'127.0.0.1',1,1590087192,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(114,2,1,NULL,'$2y$10$Q3FCTviUVc0RCkxal7FZeudsmnEEfw7UU0DdERFy5tJUEpfG.DOmC',1590087193,1621623193,'127.0.0.1',1,1590087193,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(115,2,1,NULL,'$2y$10$NH3/NaLKPUcLGIFzR7fXnO/fgnStdEWsV9VzV.z7yCVdrMKB14bre',1590087194,1621623194,'127.0.0.1',1,1590087194,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(116,2,1,NULL,'$2y$10$Mz1Esv2rGUhg2Yvi6kVbC.yLhx0yEXk4NEUoU54vlZBLLgBnSOmhe',1590087195,1621623195,'127.0.0.1',1,1590087195,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(117,2,1,NULL,'$2y$10$/IlFBgcn13NQzZd53AtiDOJhSm4rB0e5mqnSs9w4ZII8hHLfhMlj6',1590087197,1621623197,'127.0.0.1',1,1590087197,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(118,2,1,NULL,'$2y$10$./uMB7DTCoEYsFYjir9vouEyIH5OCRp4rR1QEbu036kCWAQfK3IeG',1590087212,1621623212,'127.0.0.1',1,1590087212,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(119,2,1,NULL,'$2y$10$QHZw4W1A6zNOAdk874r8POAgxwvc8JLJrMTD0rcBo3AoXPEQu6k2O',1590087213,1621623213,'127.0.0.1',1,1590087213,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(120,2,1,NULL,'$2y$10$3OjB6EeGkQrawx5HOFuW6uU5ZjwPSKplBacAby2zxmhk1WGv25V2W',1590087214,1621623214,'127.0.0.1',1,1590087214,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(121,2,1,NULL,'$2y$10$19efxkOkhAhgXaKWqfj1VunrQj2VBm3qgT9S7TQkCMm9g7KWafWGu',1590087215,1621623215,'127.0.0.1',1,1590087215,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(122,2,1,NULL,'$2y$10$C8NqccT1zrUc/o0I65ToL.r8hH4Lc/dkFvvV5hTakWeJvHsPT4TuC',1590087216,1621623216,'127.0.0.1',1,1590087216,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(123,2,1,NULL,'$2y$10$YROFkX/JxuXKjsqgMtjqf.aw756PesYsJakMWpRk30BP22GpeSVpu',1590087217,1621623217,'127.0.0.1',1,1590087217,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(124,2,1,NULL,'$2y$10$O9yIuO9Ohf1V/wNCY.jvA.jc8Q59vVQxxwTPkoqDSXJcfYVZDU342',1590087219,1621623219,'127.0.0.1',1,1590087219,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(125,2,1,NULL,'$2y$10$178f5nNK.RIRt1pOxd3SyeAAXWviY0TUFfQljzgAgvqYP1m.F0B..',1590087220,1621623220,'127.0.0.1',1,1590087220,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(126,2,1,NULL,'$2y$10$WKYkyC3k1xP3ZHQqQGy.tu2mfsoJ0H2dQ08.px29que7BC0l.Kmmu',1590087222,1621623222,'127.0.0.1',1,1590087222,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(127,2,1,NULL,'$2y$10$EkJLw3Qk6D39oBe2C5yxWOjTYfEdl7xbNhty2M7RUii0ZYfBpPGWe',1590087223,1621623223,'127.0.0.1',1,1590087223,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(128,2,1,NULL,'$2y$10$22saxNwG8.4ZzHBZ7atcEOEm828OehIJn/qZoP2ULuNsoZ6FsNlnG',1590087224,1621623224,'127.0.0.1',1,1590087224,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(129,2,1,NULL,'$2y$10$UDgo4J1NIcY1YWvV694zy.OsJezETplKoXhpGq.Mt37HyZF1K9zw6',1590087225,1621623225,'127.0.0.1',1,1590087225,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(130,2,1,NULL,'$2y$10$M4L6JqYNXgRz9WaYSrOsq.81S11fgJH0LEleMZW7.tvgRxmjEbXxi',1590087226,1621623226,'127.0.0.1',1,1590087226,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(131,2,1,NULL,'$2y$10$01Ujk2S0KrEEbSP2aFLNO..wOTPCAq9PyFuoyu1q2xUf4ip7xCTui',1590087227,1621623227,'127.0.0.1',1,1590087227,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(132,2,1,NULL,'$2y$10$NdMcV93Gy8lp6LUY6T33SezOL8RsFUn/hBqh.etmEWa/gPdAN9h3u',1590087228,1621623228,'127.0.0.1',1,1590087228,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(133,2,1,NULL,'$2y$10$Ec18wNIyv7Jo7ojZWDSSBuAHTU.S1a1X5JY5LUQJOlNBN6fEELfbO',1590087229,1621623229,'127.0.0.1',1,1590087229,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(134,2,1,NULL,'$2y$10$d6/ccouB6lEAfhW02QCjkOt1WKvLRfAf8GK28/TqwKZb/CmZ3jVai',1590087268,1621623268,'127.0.0.1',1,1590087268,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(135,2,1,NULL,'$2y$10$R8pcNSpI6rFZv1Fa.vnFYOgU15NIaMiXPXcycMqIUwKg2PJMBC38.',1590087269,1621623269,'127.0.0.1',1,1590087269,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(136,2,1,NULL,'$2y$10$mySxY9rqTQp2cgW9.Kmt.u2MZd0hgw0ysmjciQCTLABjyyzIeCxKO',1590087271,1621623271,'127.0.0.1',1,1590087271,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(137,2,1,NULL,'$2y$10$2Lv52jvz2xztgzuJ59Ava.4eFUcInmpx8UztDcQTQnWtssr39Pr5e',1590087272,1621623272,'127.0.0.1',1,1590087272,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(138,2,1,NULL,'$2y$10$AXDOg/QEOL5X2YH3bQeKouWEEVb8iVoD1A/wrK8Bw5S6LLb0OA/yO',1590087273,1621623273,'127.0.0.1',1,1590087273,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(139,2,1,NULL,'$2y$10$Gj.RZWeZqiW1BJ8DsMvANOpGGKKwLhGxNODFJ1YMrNZ.yso3YLHCS',1590087274,1621623274,'127.0.0.1',1,1590087274,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(140,2,1,NULL,'$2y$10$Ma1ffiFHK1VMJddeVtGKZ.vshEnduEfJUc7w4K5/4O9sqfklgp/c.',1590087275,1621623275,'127.0.0.1',1,1590087275,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(141,2,1,NULL,'$2y$10$DrcARzEvPzi5YGUEa.xrYu.kiL6urp.SEMiifCNqjg/LQJWQ5KyNK',1590087276,1621623276,'127.0.0.1',1,1590087276,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(142,2,1,NULL,'$2y$10$gBPH/fJbGFjYlaY3LTeWPu8sAZVxEYly6oRV/eWomdjSv1Go9HheC',1590087277,1621623277,'127.0.0.1',1,1590087277,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(143,2,1,NULL,'$2y$10$ns8ejlfhNaghIjL9DugU3OsMKAH7Hdg2xhIygIButgz5MK7QoELCS',1590087278,1621623278,'127.0.0.1',1,1590087278,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(144,2,1,NULL,'$2y$10$5.xx/i3uAo7xrECBQMIy9.vJvYbRzWhqZsc5YJQBbhd3fIfAJbVWS',1590087279,1621623279,'127.0.0.1',1,1590087279,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(145,2,1,NULL,'$2y$10$Soc1AzHaPjTEDL66OWI07.h6E7UG.pl3wGw2Q8kYjI9hxnPcYPjkG',1590087280,1621623280,'127.0.0.1',1,1590087280,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(146,2,1,NULL,'$2y$10$FjlLZNkIo5tTr4zXVOJRauInGFAHo4g5fjUdPwtf2fx6AAbkOy3k6',1590087281,1621623281,'127.0.0.1',1,1590087281,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(147,2,1,NULL,'$2y$10$YinDaKPGfJu6DEu3YdKKJeppssKHSU6VrVp0rGXSA2exQ8K4q2Gf6',1590087283,1621623283,'127.0.0.1',1,1590087283,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(148,2,1,NULL,'$2y$10$ivE94LrVHk1F9Ww2gP6SG.GMy5P0WOtZB2.J1PzJP7s35nEBwjF4S',1590087284,1621623284,'127.0.0.1',1,1590087284,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(149,2,1,NULL,'$2y$10$N21NmXvRurBWdnTFWam3bOSTsmaP8Z1N8mH2LkueCC/QRpBS.Xz5W',1590087284,1621623284,'127.0.0.1',1,1590087284,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(150,2,1,NULL,'$2y$10$0hCW60fcklMJJOoawdw9u.FCq2uCxWEeE1/VizdJ08SQFtkUQtWgi',1590087285,1621623285,'127.0.0.1',1,1590087285,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(151,2,1,NULL,'$2y$10$0RaLqt8zbBiGJ5BHNhRJAujKBakiBd0N5Xi17VyvtpmV341Au1Vj.',1590087286,1621623286,'127.0.0.1',1,1590087286,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(152,2,1,NULL,'$2y$10$bbgy6KpoJFwF2PsPuMVew.PatPNsPln0Dpa.D7I9i6sN/lMpuJD/.',1590087288,1621623288,'127.0.0.1',1,1590087288,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(153,2,1,NULL,'$2y$10$n9O7AzJjK7YcfuCnNBZjfuKupHja/LMRONvoerl2zVXtDAvUQt0Ly',1590087289,1621623289,'127.0.0.1',1,1590087289,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(154,2,1,NULL,'$2y$10$yXRHELNPkwlktO6bAFF9bufyo91mUKp/E8iFC039k5mAVRxAeWFty',1590087290,1621623290,'127.0.0.1',1,1590087290,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(155,2,1,NULL,'$2y$10$ZDmiklfVK3p8KCjFRrk4xOlhOPvo61Cm0RsD77Sw36vq4px.SxhF6',1590087291,1621623291,'127.0.0.1',1,1590087291,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(156,2,1,NULL,'$2y$10$UnfoXNNPS9eIerO/61s0HOrCbugvDTvwgBqBqacp1F5jIJ7tH5pYa',1590087292,1621623292,'127.0.0.1',1,1590087292,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(157,2,1,NULL,'$2y$10$W4kNYGru/840cHbljkg.SuOBDxiArQ5rWxhAb11aEWBw6vX.ex8Oq',1590087293,1621623293,'127.0.0.1',1,1590087293,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(158,2,1,NULL,'$2y$10$qfrJ.QhWLw6.oq6cTPpdZO2wlwSJVdvntX27OahHq7GFpGioWSiLi',1590087294,1621623294,'127.0.0.1',1,1590087294,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(159,2,1,NULL,'$2y$10$/mB98xwxsVF2bAACUrKAGOTeUyNT2BsjfORJFm3apnKp5QKiIQu4a',1590087295,1621623295,'127.0.0.1',1,1590087295,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(160,2,1,NULL,'$2y$10$1qrTYFjgjtSF94m3f8YGrehjapO3oeMUjx7Rgb72y6AG3phWqmxv6',1590087296,1621623296,'127.0.0.1',1,1590087296,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(161,2,1,NULL,'$2y$10$Y2ek3CJl.kWp1CNkdrOLfeEMq2zjygi.mbutUT9C5R7CJNgZk7OOi',1590087297,1621623297,'127.0.0.1',1,1590087297,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(162,2,1,NULL,'$2y$10$g6NxDur4VdHghX7CzpnGwOFqmWfmNS2Lkm.mBX4unjIPUU7ie2TiC',1590087298,1621623298,'127.0.0.1',1,1590087298,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(163,2,1,NULL,'$2y$10$ByLxtdD3XPCIVglJh1yU9evqM9jZVvNjZh9PWYq0Qhib1so7xtaaC',1590087299,1621623299,'127.0.0.1',1,1590087299,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(164,2,1,NULL,'$2y$10$aUQxBcTzRhdbECE1c07.bOGVpEplUvxv.KbEBLT80C3bJ1dktbd/S',1590087300,1621623300,'127.0.0.1',1,1590087300,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(165,2,1,NULL,'$2y$10$rVuJNL6FNnyONVivwRkQJuY0e0EbvUwUUFpfQJ.CdgJR4L5y0Hdry',1590087302,1621623302,'127.0.0.1',1,1590087302,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(166,2,1,NULL,'$2y$10$WS4jamKyF.B5dXs3CBSkPuenn3zC.TkeAIydx0PMYVzRQRplQpJq.',1590087306,1621623306,'127.0.0.1',1,1590087306,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(167,2,1,NULL,'$2y$10$ccbuN0f.B/alDLZlBWXfwuo448Q8vDwFMhxOpZpsSI/qvvFp4QrGS',1590087308,1621623308,'127.0.0.1',1,1590087308,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(168,2,1,NULL,'$2y$10$BAl3dhxUopvXNgM9vbemBeZbrotxBFQfTqi9xYVVSR0dJS4d2Geku',1590087309,1621623309,'127.0.0.1',1,1590087309,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(169,2,1,NULL,'$2y$10$p7TNw81fFb./DaKr0gduLuiHGqx.1/YH.82ivMtXSgqhgblpH5fBK',1590087310,1621623310,'127.0.0.1',1,1590087310,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(170,2,1,NULL,'$2y$10$RAwaiJuM8i3k67WgIPxWfuoYrhsv8./xMqEUWTTj.ryc7x47548z6',1590087311,1621623311,'127.0.0.1',1,1590087311,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(171,2,1,NULL,'$2y$10$w37nVAPGC4jlyeQ2rS9nQempyi1jmso95hbm/C/BtgqIPzgifVvq.',1590087312,1621623312,'127.0.0.1',1,1590087312,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(172,2,1,NULL,'$2y$10$lBeyy3ML/Na1LvdKL3kM.Osf82gTyMaKMy8MgHsFP41rS4R9.Hp86',1590087314,1621623314,'127.0.0.1',1,1590087314,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(173,2,1,NULL,'$2y$10$I4cMHszMwJohFpvdAgtGE.a5iLl66Tlqu.LdseIuBfJc7FqKRJTay',1590087315,1621623315,'127.0.0.1',1,1590087315,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(174,2,1,NULL,'$2y$10$PNqCz0Z1seTIfxXTVVV95.TDBfdI3HHvjOKvNckAq.hHevPurgi6C',1590087316,1621623316,'127.0.0.1',1,1590087316,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(175,2,1,NULL,'$2y$10$ulA00AS69ydifOGt9GeGluorvmj3kVSbr9U2Q6RIOW/8TwnxHwZPm',1590087317,1621623317,'127.0.0.1',1,1590087317,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(176,2,1,NULL,'$2y$10$aZ6Z1Prns51QTOoeFslGi.ugnCXxjwEZODHjpG3hoXDLvUDxGXXsa',1590087319,1621623319,'127.0.0.1',1,1590087319,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(177,2,1,NULL,'$2y$10$uR6nDL5pp8FYE1AKXbYTo.cYxounwpTAi/vV.gQK6ZZuS/EwWFfBe',1590087320,1621623320,'127.0.0.1',1,1590087320,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(178,2,1,NULL,'$2y$10$wpMDKPcoPRSOhhNNcjozaubium7RUred9i/uzPabfFjqwMNu1kY.S',1590087321,1621623321,'127.0.0.1',1,1590087321,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(179,2,1,NULL,'$2y$10$6k7hWtNsGFcidaN8XcQKpOjIsBTnkdBaXqXFtOXnvvfX4bUs4InZm',1590087322,1621623322,'127.0.0.1',1,1590087322,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(180,2,1,NULL,'$2y$10$8.wuXHFANtJ1XegwhXD6qOrEmChQuUTWyTLk/AL9K3XFkrl9rBjyO',1590087324,1621623324,'127.0.0.1',1,1590087324,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(181,2,1,NULL,'$2y$10$9Pu7cGOVXLewA89PkD3TqeDmRA6boP2qWivwJkj5jKvOQGGijyM92',1590087326,1621623326,'127.0.0.1',1,1590087326,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(182,2,1,NULL,'$2y$10$A3YFJPO7J8e4ZxE8WCH1V.bSUuBP3sxD1Vtx8qJxNXrj8si4wuVFS',1590087327,1621623327,'127.0.0.1',1,1590087327,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(183,2,1,NULL,'$2y$10$.5NB38t84FrnqxGbJhxgheLbV3lJ5OxK3CqH9O.2qG20gM2TBrJVm',1590087329,1621623329,'127.0.0.1',1,1590087329,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(184,2,1,NULL,'$2y$10$j48.GBMNIlv28kNdyjiJS.5IPBvejr8LdSoIuD3NxGCPnmBXbJpfy',1590087330,1621623330,'127.0.0.1',1,1590087330,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(185,2,1,NULL,'$2y$10$7BFKtrHVkTNlugFPC3IJVely11779UwwhdG8wtbeo7hJm798usnOO',1590087331,1621623331,'127.0.0.1',1,1590087331,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(186,2,1,NULL,'$2y$10$/ojaOEtV9E9pLuqBmMIWXuZf/O1jLWs44cXhbKVWgOQXr6YaZ6G12',1590087332,1621623332,'127.0.0.1',1,1590087332,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(187,2,1,NULL,'$2y$10$6nwmEtT0mUixCHF8qVZKzOPvrFYEPAeQjyGFAn5gNHx8kXVlS2MAy',1590087334,1621623334,'127.0.0.1',1,1590087334,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(188,2,1,NULL,'$2y$10$8HQ.LoIGPGbIkxi8vz/Uruefnu0X5wFmXntfatNGC81nV2thaJ7oO',1590087335,1621623335,'127.0.0.1',1,1590087335,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(189,2,1,NULL,'$2y$10$jsTKs2HI7wY/YEm9Dh6tSeYOH4H.zO88NK0kvsqO52ZN8I50e5ofm',1590087336,1621623336,'127.0.0.1',1,1590087336,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(190,2,1,NULL,'$2y$10$i23JmsFKOklIVkN.WARYTO.6RbP1ngQIW0rrJO/SsyjvwICTtymYi',1590087338,1621623338,'127.0.0.1',1,1590087338,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(191,2,1,NULL,'$2y$10$MGLmO0OhiM3mEpH7K0GQHuhmQxT.6ufCAH33YeYLHUrbIuV6XzBKC',1590087339,1621623339,'127.0.0.1',1,1590087339,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(192,2,1,NULL,'$2y$10$u6D3OSaHanl1.OEcik8iZ.qReIs9OaazEXYsa.3B20U7pnHZP6s1K',1590087340,1621623340,'127.0.0.1',1,1590087340,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(193,2,1,NULL,'$2y$10$kXw7r0LjZoudEhTRsWEeS.3nAvJkSN/yVglm6u3zmsksKFsZZc/9a',1590087342,1621623342,'127.0.0.1',1,1590087342,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(194,2,1,NULL,'$2y$10$evzf9/xPuqw87pK55/RlyOFtwIC4sOU/Oxm6PAdVmojh5F0ddfv.u',1590087345,1621623345,'127.0.0.1',1,1590087345,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(195,2,1,NULL,'$2y$10$OE3raOkvY0iD42DggI8a8u0UulyVLBoVZDBAZzRIwS9AX4Lb8SWKu',1590087348,1621623348,'127.0.0.1',1,1590087348,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(196,2,1,NULL,'$2y$10$PlK2EIEtGKn3yib6xsjqMeefuT9pjD7jvvhi/LsWeEgkgVdZ6iike',1590087351,1621623351,'127.0.0.1',1,1590087351,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(197,2,1,NULL,'$2y$10$5Tu62LYuBnDHzkGlmHuRjOJCIPYhhRaH6OXkk3Z1MLuwU2sx6SCga',1590087352,1621623352,'127.0.0.1',1,1590087352,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(198,2,1,NULL,'$2y$10$mQx9JsqeI31IdCuqfB6rl.L8R1TPvRc9mYUrPlGQAnLKY4HIj4vFu',1590087353,1621623353,'127.0.0.1',1,1590087353,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(199,2,1,NULL,'$2y$10$ngQpeHC1I4Ee2/E6dTpvsOwYYtfhPr1vmMQLxQpX76xV6WpMcsPJa',1590087367,1621623367,'127.0.0.1',1,1590087367,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(200,2,1,NULL,'$2y$10$RSM6HPBo1yBICOl6ZcbwG.lYMfZGPXZXR.Oe6CT3wI7/gUrGCH6rG',1590087368,1621623368,'127.0.0.1',1,1590087368,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(201,2,1,NULL,'$2y$10$8LcVumKXlgbv8HDRld97/ePex.SfVB.4RNJZhruSb9syywr1LjFGC',1590087369,1621623369,'127.0.0.1',1,1590087369,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(202,2,1,NULL,'$2y$10$J3v5KLEQ3RV5xh.X4Lsy..cQhNF9.tNVPv3GVFbwyAZa44KZFMg3K',1590087370,1621623370,'127.0.0.1',1,1590087370,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(203,2,1,NULL,'$2y$10$TDCskhqM4KNVY.PdtPUBFeKVx6gxHesXvCpJNnCivm9H4UlAURpMe',1590087371,1621623371,'127.0.0.1',1,1590087371,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(204,2,1,NULL,'$2y$10$7D2FcfWcKAEPzstdTqDz2et3bJrIipI18KHbRvOCoeQK1oG/ZY/j2',1590087372,1621623372,'127.0.0.1',1,1590087372,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(205,2,1,NULL,'$2y$10$LVbbbsrpEPgqVPFQr4zxOet/TxSjS7.MG1E7t18gK66nZ1IwD4qry',1590087373,1621623373,'127.0.0.1',1,1590087373,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(206,2,1,NULL,'$2y$10$ezu1yPY3UFruhKnfsIiInO4KCjGW4jOlzSnwDrbvV3YxLR8YsVKdO',1590087374,1621623374,'127.0.0.1',1,1590087374,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(207,2,1,NULL,'$2y$10$cxQqRa5AHKYLC7Uxshwc7u4E9haLs8iLtZAUuLMmj5tCEkmh4nHAC',1590087375,1621623375,'127.0.0.1',1,1590087375,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(208,2,1,NULL,'$2y$10$S9qozvDI0DNGC0KatcW8W.6V3rJg.IrkryM.dSKDkoVfp/Y94AR8u',1590087378,1621623378,'127.0.0.1',1,1590087378,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(209,2,1,NULL,'$2y$10$Po/S8K74jUW3KH1TQQfZLuCaQpXzvD83XpfHC8MwCuZX9z8L9w3tq',1590087382,1621623382,'127.0.0.1',1,1590087382,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(210,2,1,NULL,'$2y$10$.DLU5Eu3g2QmlAhF2mlsP.Qgx0xLNJQaVX95.FpmJQyrD2Ugt.TWq',1590087383,1621623383,'127.0.0.1',1,1590087383,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(211,2,1,NULL,'$2y$10$Qd5AOXqXV7GerxFAXV/lluxoP9VuOWDlQuSPd7YPL3wlfWTuGeAc.',1590087385,1621623385,'127.0.0.1',1,1590087385,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(212,2,1,NULL,'$2y$10$RZ9zQzLpOR1w1RWDDUeTp.ND9JZhpOGMF2HlppDQ3moiw1RD89Mni',1590087386,1621623386,'127.0.0.1',1,1590087386,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(213,2,1,NULL,'$2y$10$PkGpzUfiZryDfUYH8fOBYejse9Cn7kshtiVBrK/7pI4aUPyGRA/6e',1590087387,1621623387,'127.0.0.1',1,1590087387,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(214,2,1,NULL,'$2y$10$TYgbvMHon.qapeT6IrH25uM24n/Sh81hwCc6mlHdhw41X30Ssfc4O',1590087388,1621623388,'127.0.0.1',1,1590087388,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(215,2,1,NULL,'$2y$10$.qcdQiTtxEKOIWt1JAAqeuF4OvfkApVn60bC5uKMJZNwFo9mw0wAe',1590087389,1621623389,'127.0.0.1',1,1590087389,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(216,2,1,NULL,'$2y$10$tvcv4HxWh3OvzrfbsPQ9hOK0JJlzN6aJBUWBets1h2kbSVSR.l.Vy',1590087391,1621623391,'127.0.0.1',1,1590087391,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(217,2,1,NULL,'$2y$10$4QQUXW2gLmX6Q1qx43R5ie5DPPyxPb85ZDhoQw7Rk3NMfCxoqn63K',1590087393,1621623393,'127.0.0.1',1,1590087393,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(218,2,1,NULL,'$2y$10$wH.CpfKrixyoOePn4doiFuLCdgjh0.B4C65ht9hB7wBQJoCcDKV6.',1590087395,1621623395,'127.0.0.1',1,1590087395,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(219,2,1,NULL,'$2y$10$YgdDwlM4ZaMMlczk2QqPq.qr9NDoICKv6Laj/7RhYGLT49v66ut.a',1590087400,1621623400,'127.0.0.1',1,1590087400,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(220,2,1,NULL,'$2y$10$BqeQ4gZc6KkRvedmcBakPuLVq6QGAPZImlMqnYMKTBws4c0ZHGCOa',1590087401,1621623401,'127.0.0.1',1,1590087401,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(221,2,1,NULL,'$2y$10$mBdI5g136ke6GuqrsPQYBOrOPvOMp.mjFToL99xqEfPgaP7myLBPm',1590087402,1621623402,'127.0.0.1',1,1590087402,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(222,2,1,NULL,'$2y$10$fbA5j/7H1eCq2QE1twe5beaedWfDRHe.KkrDtC6/fR0J5BDBbeAUS',1590087404,1621623404,'127.0.0.1',1,1590087404,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(223,2,1,NULL,'$2y$10$YzUhSn65L86W2ub0l3ZJReGDZu0fKPJwWU6Bti73rOwOmOMH55cmC',1590087405,1621623405,'127.0.0.1',1,1590087405,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(224,2,1,NULL,'$2y$10$OLq7U9ncoFiGh0MocsDqwe1W9Lqlr1UFOCDzs8g1CGuASJUrKC5jG',1590087407,1621623407,'127.0.0.1',1,1590087407,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(225,2,1,NULL,'$2y$10$4aUeJd1yWWf5cz68MF2CUOFywu54GmvUPCbm/W4aAZedY6LkA5i2y',1590087417,1621623417,'127.0.0.1',1,1590087417,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(226,2,1,NULL,'$2y$10$hQBoOqbuwNwRhYQx79Erj.HZ2K6UFLPN4g1CipCPEpq76VosOfE/q',1590087418,1621623418,'127.0.0.1',1,1590087418,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(227,2,1,NULL,'$2y$10$dsYxtluM8mTkRfnQJo1EIeQKI.Oe1cH/8FnVMpMUO8FMem23jLIzS',1590087420,1621623420,'127.0.0.1',1,1590087420,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(228,2,1,NULL,'$2y$10$1.sTWzUWXWvmBQp0We7Es.IrQXzzfGLAKnOTpPh38wt1npW07UO7q',1590087421,1621623421,'127.0.0.1',1,1590087421,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(229,2,1,NULL,'$2y$10$FjSGc0LwrT/F2M1r8GsCEeuc5Y4/zVf6v4VbrtdiUT25IBKeBHZVu',1590087422,1621623422,'127.0.0.1',1,1590087422,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(230,2,1,NULL,'$2y$10$G3u85Q7vBDzSrwOJibaZyuQGWfNQnpkq.tbTbPUFmUff01LWQDst2',1590087423,1621623423,'127.0.0.1',1,1590087423,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(231,2,1,NULL,'$2y$10$yths3gMou8nv9erjGMkhG.67P4cUhnyFd4/xkVrUnSJGILAKFNPk.',1590087424,1621623424,'127.0.0.1',1,1590087424,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(232,2,1,NULL,'$2y$10$6vN2iRvOSvzo6aT514x0nOZzb8Z./sLUhSseyLnsS/DkZLwl79Ulq',1590087426,1621623426,'127.0.0.1',1,1590087426,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(233,2,1,NULL,'$2y$10$H1ZCw.PxKIfcQnbXlvYJIeMnZtfGZzeu2o6WBAkSGlsQRcZqAIWC6',1590087427,1621623427,'127.0.0.1',1,1590087427,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(234,2,1,NULL,'$2y$10$GMpai7xOFghQUILK8V2CkuTIpqxiYiv1Z8eguAGNStbNl36pFQxHq',1590087429,1621623429,'127.0.0.1',1,1590087429,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(235,2,1,NULL,'$2y$10$Vc6mOJ5haR9F2yfDU4cmdebAfVhF5pcm/N28oDPO4MphZUy.bWhYm',1590087430,1621623430,'127.0.0.1',1,1590087430,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(236,2,1,NULL,'$2y$10$Kr/2CYk7yq2vUrQ0NoA77.PF6fOaCYCw0U5NSDyo8TKWUaGRQryUm',1590087431,1621623431,'127.0.0.1',1,1590087431,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(237,2,1,NULL,'$2y$10$ibcavPobioPDhCFTokgtQOZP9GX7PCF4.NiRFv2DtYaFwvaHH3pwy',1590087432,1621623432,'127.0.0.1',1,1590087432,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(238,2,1,NULL,'$2y$10$jU8/zotcQDgo30SIOCf5Ee44W4l/HzcDsbswglPvg0LiUK/G2PBq2',1590087433,1621623433,'127.0.0.1',1,1590087433,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(239,2,1,NULL,'$2y$10$RwG9U8nCpm5EdLqGDuu8duMVv7Buz0MvlzSfPWBR919T024RYF2e2',1590087434,1621623434,'127.0.0.1',1,1590087434,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(240,2,1,NULL,'$2y$10$Uk.3i8t5mMQOJkpLUSBdXO7f/Mdpj0JAm4EW.EwQtgkkVcnYWUd7G',1590087436,1621623436,'127.0.0.1',1,1590087436,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(241,2,1,NULL,'$2y$10$u7ByHkehTdYpZ2YrfSDf6uLxxsYvyPysASAX3apvVSeka9t1rx/lC',1590087438,1621623438,'127.0.0.1',1,1590087438,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(242,2,1,NULL,'$2y$10$qfV/qGu20tD1RLbJeOcEour13quYtJyUfuRrXI55fnj01P9t.UpiC',1590087443,1621623443,'127.0.0.1',1,1590087443,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(243,2,1,NULL,'$2y$10$OURen42jFRHh6f2oWUWHjOnpr8whGR80d/uR0WXYblCpXEY6A5cV6',1590087444,1621623444,'127.0.0.1',1,1590087444,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(244,2,1,NULL,'$2y$10$ks5AH6ABMFXHIn9AscmSWeBEB8PM7Oc0exUiZtqmeo7Y0iJZ2FIRm',1590087446,1621623446,'127.0.0.1',1,1590087446,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(245,2,1,NULL,'$2y$10$ycNnP/05W9Z6cBqDbkKK.eSqLjLAVED02Sie/hoDbDbjeyC6f66bu',1590087447,1621623447,'127.0.0.1',1,1590087447,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(246,2,1,NULL,'$2y$10$cRk78BsWHwCx9XQQYL5Cd.LafSl8Ozl/sdANMHDvkiLhKv.b8ev4C',1590087448,1621623448,'127.0.0.1',1,1590087448,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(247,2,1,NULL,'$2y$10$TlZ.gJT0MlBavNEoMw8yD.fo58Yb9Qt7Xm9TAt8PAaYOZrz4j5tF6',1590087449,1621623449,'127.0.0.1',1,1590087449,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(248,2,1,NULL,'$2y$10$BmtWBLQA33DvwpXac7.JBO1wdUF8cK9o3CMVODLlBCMQfzjkWoJU2',1590087451,1621623451,'127.0.0.1',1,1590087451,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(249,2,1,NULL,'$2y$10$DTiTGO.CVElm1grHxS6G4epY9LXVgZgpR9ZuuQxiOL1ifQFMPZnCS',1590087452,1621623452,'127.0.0.1',1,1590087452,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(250,2,1,NULL,'$2y$10$5f/MFyWB7lROtqhYFpKnkuWLyIvvuowofdYtBNZwRbi.rVOm1FvMu',1590087454,1621623454,'127.0.0.1',1,1590087454,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(251,2,1,NULL,'$2y$10$cSjQGf4lY/8ufhQKRMSfQubhUHGYJGj3GCfy1Pnc8o/Uc9t1krsum',1590087455,1621623455,'127.0.0.1',1,1590087455,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(252,2,1,NULL,'$2y$10$/yiCdehuTxWGEiIAHck.AOee8d5SVPNNwYOsPCX4ukKVFssWO5NSC',1590087457,1621623457,'127.0.0.1',1,1590087457,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(253,2,1,NULL,'$2y$10$g3Icw68LbWN7kyVthveXDunOWPnGCgTMJ4/O74s2cAMYmp9ndJc32',1590087459,1621623459,'127.0.0.1',1,1590087459,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(254,2,1,NULL,'$2y$10$UlgujL.1JYQ4NsQoCzZD/O1LgdkN1Y.8lFdHyGie9JA9TTh61ZQ2S',1590087460,1621623460,'127.0.0.1',1,1590087460,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(255,2,1,NULL,'$2y$10$FsQoHh4yopCkYsR05LqV4OBRKr.Wi2m9MY30BIEbSyzFbz9QqGUpG',1590087461,1621623461,'127.0.0.1',1,1590087461,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(256,2,1,NULL,'$2y$10$BMu.hM7Rfca92o4Qow5zBeqgWcTGFyDkmqmV61XKLbG6LgC3R1fWG',1590087463,1621623463,'127.0.0.1',1,1590087463,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(257,2,1,NULL,'$2y$10$OSoenZluOL8zr9HO8ZHKIOA2Yz3Emw.WojCOAhlpyyLFombNuJcBe',1590087464,1621623464,'127.0.0.1',1,1590087464,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3),
	(258,2,1,NULL,'$2y$10$5dQ8zLuQIEK3uS8mrWibrePWsc6EIYhR6CJsdsFDfRTTCww4mshNm',1590087466,1621623466,'127.0.0.1',1,1590087466,0,3,'','UTC','+00:00','0',NULL,NULL,NULL,NULL,3);

/*!40000 ALTER TABLE `tbl_session` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_session_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_session_type`;

CREATE TABLE `tbl_session_type` (
  `sest_id` int unsigned NOT NULL AUTO_INCREMENT,
  `sest_name` varchar(255) DEFAULT NULL,
  `sest_desck` text,
  `sest_datetime` int DEFAULT NULL,
  `sest_datetime_insert` int DEFAULT NULL,
  `sest_datetime_update` int DEFAULT NULL,
  `sest_datetime_delete` int DEFAULT NULL,
  `sest_adu_id_insert` int DEFAULT NULL,
  `sest_adu_id_update` int DEFAULT NULL,
  `sest_adu_id_delete` int DEFAULT NULL,
  `sest_status` int DEFAULT '1' COMMENT '0=Inactive, 1=Active, 2=Deleted',
  PRIMARY KEY (`sest_id`),
  KEY `query_index` (`sest_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_session_type` WRITE;
/*!40000 ALTER TABLE `tbl_session_type` DISABLE KEYS */;

INSERT INTO `tbl_session_type` (`sest_id`, `sest_name`, `sest_desck`, `sest_datetime`, `sest_datetime_insert`, `sest_datetime_update`, `sest_datetime_delete`, `sest_adu_id_insert`, `sest_adu_id_update`, `sest_adu_id_delete`, `sest_status`)
VALUES
	(1,'Api',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1),
	(2,'Admin',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1);

/*!40000 ALTER TABLE `tbl_session_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table tbl_severity
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tbl_severity`;

CREATE TABLE `tbl_severity` (
  `svt_id` int unsigned NOT NULL AUTO_INCREMENT,
  `svt_name` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL DEFAULT '',
  `svt_code` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci DEFAULT NULL,
  `sct_desc` text,
  `svt_collor_code` varchar(8) DEFAULT NULL,
  `svt_sla` int DEFAULT NULL,
  `svt_datetime` int NOT NULL DEFAULT '0',
  `svt_status` int NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `svt_update_adu_id` int DEFAULT NULL,
  PRIMARY KEY (`svt_id`),
  KEY `query_index` (`svt_status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tbl_severity` WRITE;
/*!40000 ALTER TABLE `tbl_severity` DISABLE KEYS */;

INSERT INTO `tbl_severity` (`svt_id`, `svt_name`, `svt_code`, `sct_desc`, `svt_collor_code`, `svt_sla`, `svt_datetime`, `svt_status`, `svt_update_adu_id`)
VALUES
	(1,'Critical','',NULL,'C70039',7,0,1,NULL),
	(2,'Hight','',NULL,'FF5732',30,0,1,NULL),
	(3,'Medium','','','FFC300',90,0,1,NULL),
	(4,'Low','',NULL,'8cdf02',120,0,1,NULL),
	(5,'Info','',NULL,'',400,0,0,NULL);

/*!40000 ALTER TABLE `tbl_severity` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
