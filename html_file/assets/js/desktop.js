var $j = jQuery.noConflict();
var bp = {
    xsmall: 479,
    small: 599,
    medium: 770,
    large: 979,
    xlarge: 1199
}
$j(document).ready(function () {
    var windowHeight = $j(window).height();
    var windowWidth = $j(window).width();


    if (windowWidth < 1200) {
        $j("html").removeClass("noIE");
    }

    $j(window).scroll(function () {
        if ($j(this).scrollTop() > 50) {
            $j('#header_sticky').show();
        }
        else {
            $j('#header_sticky').hide();
        }
    });



    $j(".sidebar h5.collap span").click(function (e) {
        e.preventDefault();
        $j(this).parent().parent().find(".tgl_c").slideToggle(300);
        if ($j(this).hasClass("active")) {
            $j(this).removeClass('active');
        } else {
            $j(this).addClass('active');
        }

    });


    $j('.carousel').carousel({
        interval: 100000
    });

    $j(".bb-dialog").bind('click', function () {
        var modal = $j(this).attr('href');
        $j(modal).modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true
        });
        setTimeout(function () {
            modal.modal('hide');
        }, 3000);
        return false;
    });
    $j(".close-modal").on("click", function (e) {
        bootbox.hideAll();
    });

    $j('.bxslider').bxSlider({
         auto: true,
         speed:1200,
         adaptiveHeight: true,
    });
});


/* product */

var ProductMediaManager = {
    IMAGE_ZOOM_THRESHOLD: 100,
    zoomEnabled: Modernizr.mq("screen and (min-width:768px)"),
    imageWrapper: null,
    destroyZoom: function () {
        $j('.zoomContainer').remove();
        $j('.product-image-gallery .gallery-image').removeData('elevateZoom');
    },
    createZoom: function (image) {
        ProductMediaManager.destroyZoom();
        var heightdd = $j(".zoom_01").css("height");
        var widthdd = $j(".zoom_01").css("width");
        var zoomConfig = {
            zoomType: 'window',
            responsive: true,
            borderSize: '1',
            imageCrossfade: false,
            zoomWindowWidth: 530,
            zoomWindowHeight: 508,
            zoomWindowPosition: 1,
            zoomWindowOffetx: 125,
            position: '.product-shop'
        };
        if (!ProductMediaManager.zoomEnabled) {
            return;
        }

        if (image.length <= 0) { //no image found
            return;
        }

        if (image[0].naturalWidth && image[0].naturalHeight) {
            var widthDiff = image[0].naturalWidth - image.width() - ProductMediaManager.IMAGE_ZOOM_THRESHOLD;
            var heightDiff = image[0].naturalHeight - image.height() - ProductMediaManager.IMAGE_ZOOM_THRESHOLD;

            if (widthDiff < 0 && heightDiff < 0) {


                image.parents('.product-image').removeClass('zoom-available');

                return;
            } else {
                image.parents('.product-image').addClass('zoom-available');
            }
        }
        image.elevateZoom(zoomConfig);
    },
    swapImage: function (targetImage) {
        targetImage = $j(targetImage);
        targetImage.addClass('gallery-image');

        ProductMediaManager.destroyZoom();

        var imageGallery = $j('.product-image-gallery');

        if (targetImage[0].complete) { //image already loaded -- swap immediately
            imageGallery.find('.gallery-image').removeClass('visible');
            //move target image to correct place, in case it's necessary
            imageGallery.append(targetImage);
            //reveal new image
            targetImage.addClass('visible');
            //wire zoom on new image
            ProductMediaManager.createZoom(targetImage);

        } else { //need to wait for image to load

            //add spinner
            imageGallery.addClass('loading');

            //move target image to correct place, in case it's necessary
            imageGallery.append(targetImage);

            //wait until image is loaded
            imagesLoaded(targetImage, function () {
                //remove spinner
                imageGallery.removeClass('loading');

                //hide old image
                imageGallery.find('.gallery-image').removeClass('visible');

                //reveal new image
                targetImage.addClass('visible');

                //wire zoom on new image
                ProductMediaManager.createZoom(targetImage);
            });

        }
    },
    wireThumbnails: function () {
        //trigger image change event on thumbnail click
        $j('.product-image-thumbs .thumb-link').click(function (e) {
            e.preventDefault();
            var jlink = $j(this);
            var target = $j('#image-' + jlink.data('image-index'));
            ProductMediaManager.swapImage(target);
        });
    },
    initZoom: function () {
        ProductMediaManager.createZoom($j(".no-touch .gallery-image.visible")); //set zoom on first image
    },
    init: function () {
        ProductMediaManager.imageWrapper = $j('.product-img-box');
          enquire.register("screen and (min-width:768px)", {
            match : function() {
                ProductMediaManager.zoomEnabled = true;
                ProductMediaManager.initZoom();
            },
            unmatch : function() {
                ProductMediaManager.destroyZoom();
                ProductMediaManager.zoomEnabled = false;
            }
        });

        //resizing the window causes problems with zoom -- reinitialize
        $j(window).on('delayed-resize', function (e, resizeEvent) {
            ProductMediaManager.destroyZoom();
            ProductMediaManager.initZoom();
        });

        ProductMediaManager.wireThumbnails();
        $j(document).trigger('product-media-loaded', ProductMediaManager);
    }
};

$j(document).ready(function () {
    ProductMediaManager.init();
    $j(".minus_btn").click(function () {
        var inputEl = $j(this).parent().children().next();
        var qty = inputEl.val();
        if ($j(this).parent().hasClass("minus_btn"))
            qty++;
        else
            qty--;
        if (qty < 0)
            qty = 0;
        inputEl.val(qty);
    });

    $j(".plus_btn").click(function () {
        var inputEl = $j(this).parent().children().next();
        var qty = inputEl.val();
        if ($j(this).hasClass("plus_btn"))
            qty++;
        else
            qty--;
        if (qty < 0)
            qty = 0;
        inputEl.val(qty);
    });
    $j(".no-stock").click(function () {

        $j('input[name="sku"]').val($j(this).attr('sku'));
        var modal = ('#notify-popup');
        $j(modal).modal({
            "backdrop": "static",
            "keyboard": true,
            "show": true
        });
        setTimeout(function () {
            modal.modal('hide');
        }, 3000);
        return false;


    })

    $j('#productSubscribe').submit(function (e) {

        e.preventDefault();

        $j.ajax({
            type: "POST",
            url: this.action,
            data: {subscribeEmail: $j('input[name="subscribeEmail"]').val(), sku: $j('input[name="sku"]').val()},
            success: function (data) {
                $j('#productSubscribe').fadeOut('slow');
                $j('#messageSubcriber').html(data.message);
            }
        });
        return false;
    });
});
jQuery.noConflict();
jQuery(function ($) {
    jQuery('.currency-menu .currency-switcher').hoverIntent({
        over: function () {
            jQuery(this).children('.currency-content').slideDown(300);
            jQuery('.currency-content').show();
        },
        out: function () {
            jQuery(this).children('.currency-content').slideUp(300);
            jQuery('.currency-content').hide();
        }
    });
   jQuery('.header-cart .relative').hoverIntent({
        over: function () {
            jQuery(this).children('.cart-items-box').slideDown(300);
            jQuery('.cart-items-box').show();
        },
        out: function () {
            jQuery(this).children('.cart-items-box').slideUp(300);
            jQuery('.cart-items-box').hide();
        }
    });
 


});