<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_developed".
 *
 * @property int $dvp_id
 * @property string $dvp_name
 * @property string|null $dvp_code
 * @property int|null $dvp_desc
 * @property int $dvp_datetime
 * @property int $dvp_status 0=Inactive, 1=Active
 * @property int|null $dvp_update_adu_id
 */
class Developed extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_developed';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dvp_desc', 'dvp_datetime', 'dvp_status', 'dvp_update_adu_id'], 'integer'],
            [['dvp_name'], 'string', 'max' => 255],
            [['dvp_code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dvp_id' => 'Dvp ID',
            'dvp_name' => 'Dvp Name',
            'dvp_code' => 'Dvp Code',
            'dvp_desc' => 'Dvp Desc',
            'dvp_datetime' => 'Dvp Datetime',
            'dvp_status' => 'Dvp Status',
            'dvp_update_adu_id' => 'Dvp Update Adu ID',
        ];
    }
}
