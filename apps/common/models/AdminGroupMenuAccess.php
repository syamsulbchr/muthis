<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_admin_group_menu_access".
 *
 * @property integer $aga_id
 * @property integer $aga_aug_id
 * @property integer $aga_asm_id
 * @property integer $aga_access
 */
class AdminGroupMenuAccess extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_admin_group_menu_access';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aga_aug_id', 'aga_asm_id', 'aga_access'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'aga_id' => 'Aga ID',
            'aga_aug_id' => 'Aga Aug ID',
            'aga_asm_id' => 'Aga Asm ID',
            'aga_access' => 'Aga Access',
        ];
    }
}
