<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_lifecycle".
 *
 * @property int $lfc_id
 * @property string $lfc_name
 * @property string|null $lfc_code
 * @property string|null $lfc_desc
 * @property int $lfc_datetime
 * @property int $lfc_status 0=Inactive, 1=Active
 * @property int|null $lfc_update_adu_id
 */
class Lifecycle extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_lifecycle';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['lfc_desc'], 'string'],
            [['lfc_datetime', 'lfc_status', 'lfc_update_adu_id'], 'integer'],
            [['lfc_name'], 'string', 'max' => 255],
            [['lfc_code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'lfc_id' => 'Lfc ID',
            'lfc_name' => 'Lfc Name',
            'lfc_code' => 'Lfc Code',
            'lfc_desc' => 'Lfc Desc',
            'lfc_datetime' => 'Lfc Datetime',
            'lfc_status' => 'Lfc Status',
            'lfc_update_adu_id' => 'Lfc Update Adu ID',
        ];
    }
}
