<?php

namespace common\models;

use Yii;
use yii\data\ActiveDataProvider;
/**
 * This is the model class for table "tbl_level".
 *
 * @property integer $lvl_id
 * @property string $lvl_name
 * @property integer $lvl_datetime
 * @property integer $lvl_status
 */
class Level extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_level';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lvl_datetime', 'lvl_status'], 'integer'],
            [['lvl_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'lvl_id' => 'ID',
            'lvl_name' => 'Level Name',
            'lvl_datetime' => 'Datetime',
            'lvl_status' => 'Status',
        ];
    }

    public function beforeSave($insert) {
        if (parent::beforeSave($insert)) {
            $this->lvl_datetime = Zone::getGmtZero();
            if(!empty(Yii::$app->user->identity->adu_id))
            {
                $this->lvl_update_adu_id = Yii::$app->user->identity->adu_id;
            }
            
            return true;
        }

        return false;
    }

    public function getStatus() {
        if ($this->lvl_status==0){
            return 'Disable';
        }else if ($this->lvl_status==1){
            return 'Enable';
        }else if ($this->lvl_status==2){
            return 'Deleted';
        }else{    
            return 'Unknow';
        }
        
        
    }

    public function getSearch()
    {
        $model = self::find();
        $model->andFilterWhere(['=', 'lvl_id', $this->lvl_id])
        ->andFilterWhere(['like', 'lvl_name', $this->lvl_name])
        ->andFilterWhere(['=', 'lvl_status', $this->lvl_status])
        ->orderBy(['lvl_id' => SORT_DESC]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            //'filter' => yii\helpers\ArrayHelper::map(CustomerGroup::find()->orderBy('cug_name')->asArray()->all(), 'cug_id', 'cug_name'), 
            'pagination' => [
                'pageSize' => 100,
            ],
        ]);

        return $dataProvider;
    }
}
