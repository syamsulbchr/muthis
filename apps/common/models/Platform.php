<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_platform".
 *
 * @property int $plf_id
 * @property string $plf_name
 * @property string|null $plf_code
 * @property string|null $plf_desc
 * @property int $plf_datetime
 * @property int $plf_status 0=Inactive, 1=Active
 * @property int|null $plf_update_adu_id
 */
class Platform extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_platform';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['plf_desc'], 'string'],
            [['plf_datetime', 'plf_status', 'plf_update_adu_id'], 'integer'],
            [['plf_name'], 'string', 'max' => 255],
            [['plf_code'], 'string', 'max' => 10],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'plf_id' => 'Plf ID',
            'plf_name' => 'Plf Name',
            'plf_code' => 'Plf Code',
            'plf_desc' => 'Plf Desc',
            'plf_datetime' => 'Plf Datetime',
            'plf_status' => 'Plf Status',
            'plf_update_adu_id' => 'Plf Update Adu ID',
        ];
    }
}
