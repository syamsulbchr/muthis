<?php

namespace common\models;
use Yii;
use yii\helpers\Json;
use common\models\MessageError;
use common\modelsMongo\TblSession;
use Firebase\JWT\JWT;
use yii\imagine\Image;
use common\models\S3;

class Helper {
    
    public static function isEmail($email){
        if(eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$", $email)) { 
          return true;
        } 
        return false;
    }
    /**
     * Method used to generate success
     * @return Array sucess
     */
    public static function success($data = array()) {
        $result = array();
        //Core::createResult($result['d'], $data);
        $result['d'] = $data ? $data : (object) array();
        return $result;
    }

    /**
     * Method to get result
     * @return Object
     */
    public function getResult() {
        return (object) $this->result;
    }

    /**
     * Method used to set result
     * @param Array $result array result
     * @return void
     */
    public function setResult($result) {
        $this->result = $result;
    }

    /**
     * Method used to raise error
     * @param Object $module module
     * @param Integer $code error code
     * @param Array $params array of parameters
     * @return Object error
     */
    public static function error($code, $params) {
        $result = Array();
        $error = Core::getError($code);
        $result['e']['c'] = $error['code'];
        $result['e']['m'] = Core::subtitute($error['description'], $params);
        return (object) $result;
    }

    /**
     * Method used to get error
     * @param Integer $code error code
     * @return Array error
     */
    public static function getError($code) {
        global $CORE_ERROR;
        for ($i = 0; $i < count($CORE_ERROR); $i++) {
            if ($CORE_ERROR[$i]['code'] == $code) {
                return $CORE_ERROR[$i];
            }
        }
        return Array();
    }

    public function encrypt($str, $iv = null, $key = null) {
        if ($iv == null) {
            $iv = Core::SECURITY_IV;
        }
        if ($key == null) {
            $key = Core::SECURITY_KEY;
        }
        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
        mcrypt_generic_init($td, $key, $iv);
        $encrypted = mcrypt_generic($td, $str);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return bin2hex($encrypted);
    }

    public function decrypt($code, $iv = null, $key = null) {
        if ($iv == null) {
            $iv = Core::SECURITY_IV;
        }
        if ($key == null) {
            $key = Core::SECURITY_KEY;
        }
        $code = $this->hex2bin($code);
        $td = mcrypt_module_open('rijndael-128', '', 'cbc', $iv);
        mcrypt_generic_init($td, $key, $iv);
        $decrypted = mdecrypt_generic($td, $code);
        mcrypt_generic_deinit($td);
        mcrypt_module_close($td);
        return utf8_encode(trim($decrypted));
    }

    protected function hex2bin($hexdata) {
        $bindata = '';
        for ($i = 0; $i < strlen($hexdata); $i += 2) {
            $bindata .= chr(hexdec(substr($hexdata, $i, 2)));
        }
        return $bindata;
    }

    public static function smsSending($msisdn,$message){
        $url = Yii::$app->params['smsApiUrl']."username=".Yii::$app->params['smsUserName']."&password=" .Yii::$app->params['smsPassword'].
        "&to=" .$msisdn. "&from=" .urlencode(Yii::$app->params['smsMaskingName'])."&message=" .urlencode($message)." ";
        //Curl Start
        $ch = curl_init();
        $timeout = 30;
        curl_setopt ($ch,CURLOPT_URL, $url) ;
        curl_setopt ($ch,CURLOPT_RETURNTRANSFER, 1);
        curl_setopt ($ch,CURLOPT_CONNECTTIMEOUT, $timeout) ;
        $response = curl_exec($ch) ;
        curl_close($ch);
        //Write out the response
        return $response ;
    }

    public static function randomNumber(){
        return intval( "0" . rand(1,9) . rand(0,9) . rand(0,9) . rand(0,9)); // random(ish) 4 digit int
    }

    public static function isValidmobileNumber($msisdn){
        $return                 = false;
        $clearUsername          = str_replace(' ', '',$msisdn);
        $phoneNumber            = trim($clearUsername);
        $param                  = 'msisdn';
        $msg                    = 'Invalid phone number main';
        $helper                 = New Helper;
        $mobilePrefixFirst      = trim(substr($phoneNumber,0,1));
        if($mobilePrefixFirst == '+'){
            $validate = self::validatePrefixWithPlus($phoneNumber);
            //die('1');
        }else{
            //die('2');
            $validate = self::validatePrefixWithoutPlus($phoneNumber);
        }
        
        $return = $validate->return;
        $data   = $validate->data;
        $msg    = $validate->msg;
        $error  = General::errorFormat($param,$msg);
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }
    public static function validatePrefixWithPlus($msisdn){
        $return                 = false;
        $data                   = (object)['msisdn'=>$msisdn];
        $param                  = 'msisdn';
        $msg                    = 'Invalid phone number +';
        $error                  = [];
        $msisdnPrefixLast       = substr($msisdn,1,20);
        $mobilePrefixFirst      = substr($msisdn,0,4);
        $messageError           = New MessageError;
        $languageId             = Yii::$app->params['defaultLanguageId'];
        //echo 'mobilePrefixFirst = '.$mobilePrefixFirst;
        if($mobilePrefixFirst == '+628'){
            //die('sapi');
            $return = true;
            $data   = (object)['msisdn'=>$msisdn]; 
            $msg    = 'Success';
        }else{
            //die('bangke');
            $param       = 'msisdn';
            $objectError = (object)['languageId'=>$languageId,'code'=>505,'replace'=>'Msisdn'];
            $msg         = $messageError->getErrorMessageWithCode($objectError); 
        }
        $error  = General::errorFormat($param,$msg);
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result ;
    }

    public static function validatePrefixWithoutPlus($msisdn){
        $return                 = false;
        $param                  = 'msisdn';
        $msg                    = 'Invalid phone number';
        $phoneNumber            = trim($msisdn);
        $msisdnPrefixLast       = substr($phoneNumber,2,25);
        $mobilePrefixFirst      = substr($phoneNumber,0,2);
        $messageError           = New MessageError;
        $languageId             = Yii::$app->params['defaultLanguageId'];
        if(strlen($phoneNumber) > 8){
            
            if($mobilePrefixFirst == '08'){
                $msisdn = '+62'.substr($phoneNumber,1,25);
                $return = true;
                $msg    = 'Success';
                //die('1');
            }else if($mobilePrefixFirst == '0+'){
                $msisdn = '+'.$msisdnPrefixLast;
                $return = true;
                $msg    = 'Success';
                //die('2');
            }else{
                $msisdn = $phoneNumber;
                $return = false;
                $msg    = 'Invalid MSISDN number';
                //die('3');
            } 
        }else{
            $param       = 'msisdn';
            $objectError = (object)['languageId'=>$languageId,'code'=>505,'replace'=>'Msisdn'];
            $msg         = $messageError->getErrorMessageWithCode($objectError);
            $error       = General::errorFormat($param,$msg); 
        }
        
        $data   = (object)['msisdn'=>$msisdn]; 
        $error  = General::errorFormat($param,$msg);
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result ;
    }

    public static function isValidEmail($email){
        $return = false;
        $data   = (object)['email'=>$email];
        $msg    = 'Invalid email address format';
        $param  = 'email';
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $return = true;
            $msg    = 'Success';
        }
        
        $error  = General::errorFormat($param,$msg);
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result ;
    }

    public static function getMessageCodeRegistration(){
        return 'SELTER : Your SignUp Activation Code is ';
    }

    public static function getMessageForgotPasswordMobile(){
        return 'SELTER : your code for Forgot Password is ';
    }

    public static function getMessageEmailChange(){
        return 'SELTER : Your Verification Code is ';
    }
    

    public static function getactivetab($tabNumber=1, $tabActive=1) {
        if ($tabNumber == $tabActive){
            return 'active';
        }else{    
            return '';
        }
    }

    public function validateUploadedImage($imageName){
        $data           = (object)[];
        $return         = false;
        $msg            = 'Invalid file format';
        $error          = (object)[];
        if(!empty($_FILES)){
            if(isset($_FILES[$imageName])){
                $file_info = getimagesize($_FILES[$imageName]['tmp_name']);
                if(!empty($file_info)){
                    $return  = true;
                    $msg     = 'success';
                }
            }else{
                $msg = 'invalid parameter'.$imageName;
            }
        }else{
            $msg = 'Invalid file upload';
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

    public function validateFileUpload($object){
        $data           = (object)[];
        $return         = false;
        $msg            = 'Invalid file format';
        $error          = (object)[];
        echo '<pre>';
        print_r($object);
        echo '</pre>';
        //die;
        if(!empty($_FILES)){
            echo '<pre>';
            print_r($_FILES);
            echo '</pre>';
            die;
            if(isset($_FILES[$object->image])){
                if(isset($object->typeId)){

                }
                $file_info = getimagesize($_FILES[$imageName]['tmp_name']);
                if(!empty($file_info)){
                    $return  = true;
                    $msg     = 'success';
                }
            }else{
                $msg = 'invalid parameter'.$imageName;
            }
        }else{
            $msg = 'Invalid file upload';
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

    public function moveUploadedFile($file,$movePath,$fileName){
        $uploadfile = $movePath.$fileName;
        if (move_uploaded_file($file['tmp_name'], $uploadfile)) {
            return true;
        }
        return false;
    }

    public static function convertToK($object) {
        $angka = '0';
        if(!empty($object)){
            $angka = 0;
            if(strlen($object)>=4){
                $temp = round($object);
                $temp_number_format = number_format($temp);
                $temp_array = explode(',', $temp_number_format);
                $temp_parts = array('K', 'M', 'B', 'T');
                $temp_count_parts = count($temp_array) - 1;
                $angka = $temp;
                $angka = $temp_array[0] . ((int) $temp_array[1][0] !== 0 ? '.' . $temp_array[1][0] : '');
                $angka .= $temp_parts[$temp_count_parts - 1];
            }else{
                $angka = $object;
            }
        }       
        return $angka;
    }


    public function generateNameFile($prefix,$id,$extention){
        return $prefix.'_'.$id.'_'.time().'.'.$extention;
    } 

    public function getFileExtention($fileName){
        $array = explode('.', $fileName);
        $extension = end($array);
        if($extension == '.file'){
            $extension = '.jpg';
        }
        return strtolower($extension);
    } 

    public static function getCurrentPage($post){
        $page = 0;
        if(isset($post->page) && !empty($post->page) && $post->page > 0){
            $page = $post->page -1;
        }
        return $page;
    }

    public static function getPagingResult($pagination){
        $currentPage    = $pagination->getPage()+1;
        $isload         = false;
        if($pagination->getPageCount()>$currentPage){
            $isload         = true;
        }
        $paging = (object)['count'=>$pagination->getPageCount(),
                   'currentPage'=>$currentPage,
                   'isLoad' => $isload,
                   ];
        return ['paging'=>$paging];
    }

    public static function getPagingResultEmpty(){
        $isload         = false;
        $paging = (object)['count'=>0,
                   'currentPage'=>1,
                   'isLoad' => $isload,
                   ];
        return ['paging'=>$paging];
    }

    public static function getUniCode(){
        $uniqueCode     = intval(rand(1,5) . rand(0,9) . rand(0,9));
        return $uniqueCode;
    }

    public static function validateUserTokenMongoDb($post){//validate token with mongodb
        $return             = false;
        $param              = 'token';
        $msg                = 'Unknow error';
        $error              = (object)[];
        $data               = [];
        $transactionType    = 1;
        $messageError       = New MessageError;
        $languageId         = Yii::$app->params['defaultLanguageId'];
        if(!empty($post)){
            if(isset($post->token)){
                if(TblSession::check($post->token)){
                    $user = TblSession::getUserByToken($post->token);
                    if(!empty($user)){
                        $userMysql = User::find()->where('usr_id=:userId',[':userId'=>$user->usr_id])->one();
                        if(!empty($userMysql)){
                            $return = true;
                            $param  = 'token';
                            $data   = (object)['user'=>$userMysql];
                            $msg    = 'Success';
                        }else{
                            $msg    = 'Invalid user token data';
                            $error  = General::errorFormat($param,$msg);
                        }
                    }else{
                        $objectError = (object)['languageId'=>1,'code'=>507,'replace'=>'User'];
                        $msg         = $messageError->getErrorMessageWithCode($objectError);
                        $error  = General::errorFormat($param,$msg);   
                    }
                }else{
                    $objectError = (object)['languageId'=>$languageId,'code'=>503,'replace'=>'You'];
                    $msg         = $messageError->getErrorMessageWithCode($objectError);
                    $error       = General::errorFormat($param,$msg);
                }    
            }else{
                $objectError = (object)['languageId'=>1,'code'=>505,'replace'=>'You'];
                $msg         = $messageError->getErrorMessageWithCode($objectError); 
                $error       = General::errorFormat($param,$msg);  
            }
        }else{
            $objectError = (object)['languageId'=>1,'code'=>501];
            $msg         = $messageError->getErrorMessageWithCode($objectError); 
            $error       = General::errorFormat($param,$msg);  
        }

        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

    public static function validateUserToken($post){
        $return             = false;
        $param              = 'token';
        $msg                = 'Unknow error';
        $error              = (object)[];
        $data               = [];
        $transactionType    = 1;
        $messageError       = New MessageError;
        $languageId         = Yii::$app->params['defaultLanguageId'];
        $secret             = Yii::$app->params['secretKeyJwt'];
        $algo               = Yii::$app->params['algoJwt'];

        if(!empty($post)){
            try {
                $decoded = JWT::decode($post->token, $secret, [$algo]);
                if(!empty($decoded->jti)){
                    // echo '<pre>';
                    // print_r($decoded);
                    // echo '</pre>';
                    // die;
                    $user = User::find()->select(['usr_id','usr_phone_register','usr_email','usr_username','usr_type_id','usr_password'])->where('usr_id=:userId',[':userId'=>$decoded->jti])->one();
                    if(!empty($user)){
                        $return = true;
                        $param  = 'token';
                        $data   = (object)['user'=>$user];
                        $msg    = 'Success';
                    }else{
                        $objectError = (object)['languageId'=>1,'code'=>503,'replace'=>'You'];
                        $msg         = $messageError->getErrorMessageWithCode($objectError); 
                        $error       = General::errorFormat($param,$msg);  
                    }
                }else{
                    $objectError = (object)['languageId'=>1,'code'=>503,'replace'=>'You'];
                    $msg         = $messageError->getErrorMessageWithCode($objectError); 
                    $error       = General::errorFormat($param,$msg);  
                }
            } catch (\Exception $e) {
                $objectError = (object)['languageId'=>1,'code'=>503,'replace'=>'You'];
                $msg         = $messageError->getErrorMessageWithCode($objectError); 
                $error       = General::errorFormat($param,$msg);  
            }
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

    public static function validateUserTokenx($post){//validate token with mysql
        $return             = false;
        $param              = 'token';
        $msg                = 'Unknow error';
        $error              = (object)[];
        $data               = [];
        $transactionType    = 1;
        $messageError       = New MessageError;
        $languageId         = Yii::$app->params['defaultLanguageId'];
        if(!empty($post)){
            if(isset($post->token)){
                $session        = Session::find()->select('ses_usr_id')->where('ses_key=:token',[':token'=>trim($post->token)])->one();
                if(!empty($session)){
                    $user = User::find()->where('usr_id=:userId',[':userId'=>$session->ses_usr_id])->one();
                    if(!empty($user)){
                        // $connection = Yii::$app->db;
                        // $sql        = 'SHOW processlist';
                        // $query      = $connection->createCommand($sql)->queryAll();
                        // foreach ($query as $row) {
                        //     if($row['Command'] == 'Sleep'){
                        //         $connection->createCommand('KILL '.$row['Id'])->execute();;
                        //         // echo '<pre>';
                        //         // print_r($row);
                        //         // echo '</pre>';
                        //     }
                        // }
                        $return = true;
                        $param  = 'token';
                        $data   = (object)['user'=>$user];
                        $msg    = 'Success';
                    }else{
                        $objectError = (object)['languageId'=>1,'code'=>507,'replace'=>'User'];
                        $msg         = $messageError->getErrorMessageWithCode($objectError);
                        $error  = General::errorFormat($param,$msg);   
                    }
                }else{
                    $objectError = (object)['languageId'=>$languageId,'code'=>503,'replace'=>'You'];
                    $msg         = $messageError->getErrorMessageWithCode($objectError);
                    $error       = General::errorFormat($param,$msg);
                }    
            }else{
                $objectError = (object)['languageId'=>1,'code'=>505,'replace'=>'You'];
                $msg         = $messageError->getErrorMessageWithCode($objectError); 
                $error       = General::errorFormat($param,$msg);  
            }
        }else{
            $objectError = (object)['languageId'=>1,'code'=>501];
            $msg         = $messageError->getErrorMessageWithCode($objectError); 
            $error       = General::errorFormat($param,$msg);  
        }

        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }  

    public static function validateChangePassword($object){
        $return             = false;
        $param              = 'password';
        $msg                = 'Unknow error';
        $error              = (object)[];
        $data               = [];
        $transactionType    = 1;
        $messageError       = New MessageError;
        $languageId         = Yii::$app->params['defaultLanguageId'];
        if(!empty($object)){
            if(isset($object->password)){
                if(!empty($object->password)){
                    if(strlen($object->password) > 7){
                        if(isset($object->oldPassword)){
                            if(!empty($object->oldPassword)){
                                if(strlen($object->oldPassword) > 7){
                                    $return = true;
                                    $msg    = 'Success';
                                }else{
                                    $param       = 'oldPassword';
                                    $objectError = (object)['languageId'=>$languageId,'code'=>555];
                                    $msg         = $messageError->getErrorMessageWithCode($objectError); 
                                    $error       = General::errorFormat($param,$msg);  
                                }
                            }else{
                                $param       = 'oldPassword';
                                $objectError = (object)['languageId'=>$languageId,'code'=>554];
                                $msg         = $messageError->getErrorMessageWithCode($objectError); 
                                $error       = General::errorFormat($param,$msg);  
                            }
                        }else{
                            $param       = 'oldPassword';
                            $objectError = (object)['languageId'=>$languageId,'code'=>554];
                            $msg         = $messageError->getErrorMessageWithCode($objectError); 
                            $error       = General::errorFormat($param,$msg);  
                        }
                    }else{
                        $param       = 'password';
                        $objectError = (object)['languageId'=>$languageId,'code'=>553];
                        $msg         = $messageError->getErrorMessageWithCode($objectError); 
                        $error       = General::errorFormat($param,$msg);  
                    }
                }else{
                    $param       = 'password';
                    $objectError = (object)['languageId'=>$languageId,'code'=>505];
                    $msg         = $messageError->getErrorMessageWithCode($objectError); 
                    $error       = General::errorFormat($param,$msg);  
                }
            }else{
                $param       = 'password';
                $objectError = (object)['languageId'=>$languageId,'code'=>505];
                $msg         = $messageError->getErrorMessageWithCode($objectError); 
                $error       = General::errorFormat($param,$msg);  
            }
        }else{
            $param       = 'password';
            $objectError = (object)['languageId'=>$languageId,'code'=>505];
            $msg         = $messageError->getErrorMessageWithCode($objectError); 
            $error       = General::errorFormat($param,$msg);  
        }

        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

    public static function getFrontEndUrl($url){
        $frontEndUrl  = 'https://www.sellter.co';
        if($url=='local.apisellter.co'){
            $frontEndUrl  = 'https://local.sellter.co';
        }elseif ($url=='apidev.sellter.co') {
            $frontEndUrl  = 'https://dev.sellter.co';
        }elseif ($url=='apistaging.sellter.co') {
            $frontEndUrl  = 'https://staging.sellter.co';
        }elseif ($url=='api.sellter.co') {
            $frontEndUrl  = 'https://www.sellter.co';
        }
        return $frontEndUrl;
    }

    public static function getBackendEndUrl($url){
        $frontEndUrl  = 'https://admin.sellter.co';
        if($url=='local.apisellter.co'){
            $frontEndUrl  = 'https://local.sellter.co';
        }elseif ($url=='apidev.sellter.co') {
            $frontEndUrl  = 'https://dev.sellter.co';
        }elseif ($url=='apistaging.sellter.co') {
            $frontEndUrl  = 'https://staging.sellter.co';
        }elseif ($url=='api.sellter.co') {
            $frontEndUrl  = 'https://www.sellter.co';
        }
        return $frontEndUrl;
    }

    public static function checkUnUseMsisdn($msisdn,$userLogin){
        $return = false;
        $data   = (object)['msisdn'=>$msisdn];
        $param  = 'msisdn';
        $msg    = 'Invalid phone number';
        $error  = (object)[];
        if(!empty($userLogin)){
            $user = User::find()->select('usr_id')->where('usr_phone_register=:msisdn',[':msisdn'=>$msisdn])->one();
            if(empty($user) || $userLogin->usr_id == $user->usr_id){
                $mobileRegister = MobileRegister::find()->where('mbr_msisdn=:msisdn',[':msisdn'=>$msisdn])->one();
                if(empty($mobileRegister)){
                    $return = true;
                    $msg    = 'Success';
                }else{
                    $msg    = 'The number you entered is already registered.';
                }
            }else{
                $msg    = 'The number you entered is already registered.';
            }
        }else{
            $user = User::find()->select('usr_id')->where('usr_phone_register=:msisdn',[':msisdn'=>$msisdn])->one();
            if(empty($user)){
                $mobileRegister = MobileRegister::find()->where('mbr_msisdn=:msisdn',[':msisdn'=>$msisdn])->one();
                if(empty($mobileRegister)){
                    $return = true;
                    $msg    = 'Success';
                }else{
                    $msg    = 'The number you entered is already registered.';
                }
            }else{
                $msg    = 'The number you entered is already registered.';
            }
        }
        $error  = General::errorFormat($param,$msg);
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

    public static function msisdnViewPlus($msisdn){

    }

    public static function generateOngkirCity($object){
        $price    = 0;
        $service  = "";
        $etd      = "";
        $arrPrice = [];

        $postField = "origin=".$object->originId."&originType=".$object->originType."&destination=".$object->toId."&destinationType=".$object->toType."&weight=1000&courier=jne";



        $header = ["content-type" => "application/x-www-form-urlencoded",
                   "key"          => Yii::$app->params['apiKeyRajaOngkir'],
                   ];
        $curl = curl_init(); //inisialisasi curl
           curl_setopt_array($curl, array(
           CURLOPT_URL => Yii::$app->params['urlCekOngkirRJo'],
           CURLOPT_RETURNTRANSFER => true,
           CURLOPT_ENCODING => "",
           CURLOPT_MAXREDIRS => 10,
           CURLOPT_TIMEOUT => 30,
           CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
           CURLOPT_CUSTOMREQUEST => "POST",
           CURLOPT_POSTFIELDS => $postField,
           CURLOPT_HTTPHEADER => array(
            "content-type: application/x-www-form-urlencoded",
            "key:".Yii::$app->params['apiKeyRajaOngkir'],
          ),
        ));

       $curlResult   = curl_exec($curl); 
       $objectResult = (object)Json::decode($curlResult); 
       $results      = $objectResult->rajaongkir['results'][0]['costs'];
       foreach ($results as $value) {         
            if($value['service'] == "REG"){
                $price = $value['cost'][0]['value']; 
                $etd   = $value['cost'][0]['etd']; 
                $service = "REG";
                $objReg = (Object)["price"   => $price,
                         "etd"     => $etd,
                         "service" => $service,
                        ];
                array_push($arrPrice, $objReg);    
            }

            if($value['service'] == "YES"){
                $price = $value['cost'][0]['value']; 
                $service = "YES";
                $etd = "1";
                $obj = (Object)["price"   => $price,
                         "etd"     => $etd,
                         "service" => $service,
                        ];
                array_push($arrPrice, $obj);  
            } 
               
            
       }
        curl_close($curl);
        $objPrice = (object)["list"=> $arrPrice];
        return $objPrice;
    }  

    public static function passwordHash($password){
        return password_hash($password, PASSWORD_DEFAULT);
    } 

    public static function getDataFromCurl($object){
        $ch = curl_init(); //inisialisasi curl
        curl_setopt($ch, CURLOPT_URL, $object->requestUrlApi);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, $object->userAgent);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $object->method);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$object->requestData);
        $curlResult  = curl_exec($ch); 
        // echo 'sapi<pre>';
        // print_r($curlResult);
        // echo '</pre>';
        // die;
        $objectResult   = (object)Json::decode($curlResult); 
        curl_close($ch);
        return $objectResult;
    } 

    public static function getDataFromCurl2($object){
        $ch = curl_init(); //inisialisasi curl
        curl_setopt($ch, CURLOPT_URL, $object->requestUrlApi);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, $object->userAgent);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $object->method);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$object->requestData);
        $curlResult  = curl_exec($ch); 
        echo 'curl2 : <pre>';
        print_r($curlResult);
        echo '</pre>';
        die;
        // $objectResult   = (object)Json::decode($curlResult); 
        // curl_close($ch);
        // return $objectResult;
    } 

    public static function clean($text){
        // allow only letters
          $res = preg_replace("/[^A-Za-z0-9]/", "", $text); 

          // trim what's left to 8 chars
          $res = substr($res, 0, 8);

          // make lowercase
          $res = strtolower($res);

          // return
          return $res;
    }

    public static function textConvertHtml($string){
        $convertText = '';
        if(!empty($string)){
            $stringAschi = iconv('ASCII', 'UTF-8//IGNORE', $string);
            $convertText = str_replace('&amp;','&',htmlspecialchars($stringAschi));
    
        }
        return $convertText;
    }

    /*
    * this function for geting 6 digit number
    * 
    */

    public static function getVirtualAccountLastNumber($numberId){
        $lastNumber = $numberId;
        if(strlen($numberId)==1){
            $lastNumber = '00000'.$numberId;
        }else if(strlen($numberId)==2){
            $lastNumber = '0000'.$numberId;
        }else if(strlen($numberId)==3){
            $lastNumber = '000'.$numberId;
        }else if(strlen($numberId)==4){
            $lastNumber = '00'.$numberId;
        }else if(strlen($numberId)==5){
            $lastNumber = '0'.$numberId;
        }else if(strlen($numberId)==6){
            $lastNumber = $numberId;
        }
        return $lastNumber;
    } 

    public static function generateTrxMsaku($object){
        $order = "SELLTER-ODR-#orderid";
        $topup = "SELLTER-TP-#orderid";
        $trxId = "";
        // type 1 order
        if(!empty($object)){
            if($object->type == 1){
                $trxId = str_replace('#orderid',$object->trxId,$order);
            }else{
                $trxId =  str_replace('#orderid',$object->trxId,$topup);
            }
        }
        return $trxId;
    }


    public static function callBackUrlMsaku($object){
        $callbackUrl = '';
        if(!empty($object)){
            $callbackUrl = Yii::$app->params['backendUrl']."msaku/callbackurl/?id=".$object->trxId."&token=".$object->token."";
        }
        return $callbackUrl;
    }

     public static function getPhonumberVa($phoneNumber){
        $number = 0;
        if(!empty($phoneNumber)){
            $number = substr($phoneNumber,-4);
        }
        return $number;
    } 


    public static function convertHtml($string){
        $convertText = '';
        if(!empty($string)){
            $stringAschi = iconv('ASCII', 'UTF-8//IGNORE', $string);
            $convertText = str_replace('&amp;','&',htmlspecialchars($stringAschi));
    
        }
        return $convertText;
    }

    // public static function seperateVaNumber($number){
    //     $vaNumber = $number;
    //     if(!empty($number) && strlen($number) == 16){
    //         $b =  substr($a, 0,5);
    //         $c =  substr($a, 5,3);
    //         $d =  substr($a, 8,4);
    //         $e =  substr($a, 12,4);
    //         $vaNumber = $b.'-'.$c.'-'.$d.'-'.$e;
    //     }
    //     return $vaNumber;
    // }

    // public static function seperateVaNumberFix($number){
    //     $vaNumber = $number;
    //     $arraySeperate = [5,3,4,4];
    //     $arrayNumber = str_split($number);
    //     echo '<pre>';
    //     print_r($arrayNumber);
    //     echo '</pre>';
    //     $tempNumber = '';
    //     $newNumber  = '';
    //     $separate   = false;
    //     $countLoop  = 0;
    //     foreach ($arraySeperate as $arraySeperateDigit) {
    //         $lastArray  = 0;
    //         $separate   = false;
    //         for ($i=0; $i < $arraySeperateDigit; $i++) {
    //             //$countLoop  = $lastArray;
    //             $countLoop = $countLoop+1;
    //             $lastArray = $i;
    //             if(!$separate){
    //                 $tempNumber = $tempNumber.$arrayNumber[$countLoop-1];
    //                 //echo 'i ke = '.$i .'=='. $arraySeperateDigit.' => '.$tempNumber.' => '.$countLoop.'<br/>';
    //                 if($i == ($arraySeperateDigit-1)){
    //                     $newNumber = $newNumber.$tempNumber.'-';
    //                     //$lastArray = $countLoop;
    //                     $tempNumber = '';
    //                     $separate   = true;
    //                 }
    //             }
                
    //         }
    //     }
    //     echo $newNumber;
    // }

    public static function seperateVaNumberFix($object){
        $number         = trim($object->number);
        $arraySeperate  = $object->format;//[1,2,3,4,5];
        $arrayNumber    = str_split($number);
        $tempNumber     = '';
        $newNumber      = '';
        $countLoop      = 0;
        $startString    = 0;
        $stringNumber   = '';
        $temNewNumber   = '';
        $countLoop =0;
        foreach ($arraySeperate as $countSeperate) {
            $countLoop=$countLoop+1;
            $tempNumber      = substr($number, $startString,$countSeperate);
            $stringNumber    = $stringNumber.$tempNumber.' ';
            $temNumberLenk   = strlen($stringNumber)-$countLoop;
            $startString      = $temNumberLenk;
        } 
        $temNewNumber = $stringNumber.substr($number, $startString,count($arrayNumber));
        if(substr($temNewNumber,-1) == ' '){
            $newNumber = substr($temNewNumber, 0, -1); 
            if(substr($newNumber,-1) == ' '){
                $newNumber = substr($newNumber, 0, -1); 
            }
        }else{
            $newNumber = $temNewNumber;  
        }
        return $newNumber;
    }

    public static function checkRedisServerConnection(){
        $redis  = Yii::$app->redis;
        $waitTimeoutInSeconds = 1; 
        try {
            if(fsockopen($redis->hostname,45,$errCode,$errStr,$waitTimeoutInSeconds)){   
              return true;
            }
        } catch (Exception $e) {
           return false; 
        }
        
        return false;
    }


    public static function maskingCC($number){
        $maskingCard    = "XXXX-XXXX-XXXX-X";
        $listCardNumber = '';
        $cardNumber     = '';
        if(!empty($number)){
            $listCardNumber = substr($number, -3,3);
            $cardNumber     = $maskingCard.$listCardNumber;
        }
        return $cardNumber;
    }

    public static function orderActionUser($object){
        $buyerAction    = 0;
        $sellerAction   = 0;
        $jne            = 0;
        if(!empty($object)){
            if(isset($object->jne)){
                $jne = $object->jne;
            }
            if(isset($object->process)){
                if($object->process == 1){
                    $buyerAction    = 1;
                    $sellerAction   = 0;
                }else if($object->process == 2){
                    $buyerAction    = 0;
                    $sellerAction   = 0;
                }else if($object->process == 3){
                    $buyerAction    = 0;
                    $sellerAction   = 1;
                }else if($object->process == 4){
                    $buyerAction    = 1;
                    $sellerAction   = 0;
                }else if($object->process == 6){
                    $buyerAction    = 0;
                    $sellerAction   = 0;
                }else if($object->process == 5 && $jne == 2){
                    $buyerAction    = 1;
                    $sellerAction   = 0;
                }
                else if($object->process == 5 && $jne == 1){
                    $buyerAction    = 1;
                    $sellerAction   = 1;
                }else if($object->process == 5){
                    $buyerAction    = 0;
                    $sellerAction   = 1;
                }
            }
        }
        $template = (object)['buyerAction'  => $buyerAction,
                             'sellerAction' => $sellerAction,
                            ];
        return $template;
    }

    public static function buildPagingFromAjax($object){
        $template           = '';
        $buildPaging        = '';
        $startLoop          = 0;
        
        $maxShowPagingLimit = Yii::$app->params['maxShowPagingLimit'];
        $limitShowPaging    = $object->paging['count'];
        $maxShowPagingNext  = Yii::$app->params['maxShowPagingNext'];
        $arrowDotLast       = $object->paging['count'];

        if(isset($object->maxShowPagingLimit)){
            $maxShowPagingLimit = $object->maxShowPagingLimit;
        }
        $arrowDotFirst      = '';
        $arrowDotLast       = '';
        $arrowLastNumber    = ''; 

        $arrowFirst         = '<li class="first disabled"><span><i class="fa fa-angle-double-left"></i></span></li>';
        $arrowLast          = '<li class="last"><a href="#" onclick="return false;"><i class="fa fa-angle-double-right"></i></a></li>'; 

        if($object->paging['count'] > 0){
            if($object->paging['count'] > $maxShowPagingLimit){
                $limitShowPaging    = $maxShowPagingLimit;
                $arrowDotLast       .= '<li><a class="" href="#" onclick="return false;">&bull;&bull;&bull;</a></li>';
                $arrowLastNumber    .= '<li><a class="'.$object->classClick.'" href="#" data-id="'.$object->paging['count'].'" data-url="'.$object->url.'" data-page="'.$object->paging['count'].'" onclick="return false;">'.$object->paging['count'].'</a></li>';
            }

            if($object->paging['currentPage'] <= $maxShowPagingLimit){
                $startLoop = $object->paging['currentPage'];
            }
            if(($limitShowPaging+$object->paging['currentPage']) <= $object->paging['count']){
                $limitShowPaging = $limitShowPaging+$object->paging['currentPage'];
            }
            for ($i=$startLoop; $i < $limitShowPaging; $i++) {
                $numberPaging = $i+1;
                if($object->paging['currentPage'] == $numberPaging){
                    $buildPaging .= '<li class="active"><a href="#" data-id="'.$object->id.'" data-url="'.$object->url.'" data-page="'.$numberPaging.'" onclick="return false;">'.$numberPaging.'</a></li>';
                }else{
                    $buildPaging .= '<li><a class="'.$object->classClick.'" href="#" data-id="'.$object->id.'" data-url="'.$object->url.'" data-page="'.$numberPaging.'" onclick="return false;">'.$numberPaging.'</a></li>';
                }
                
            }
            $numberPagingBefore = 1;
            if((int)$object->paging['currentPage'] > 1){
                $numberPagingBefore = (int)$object->paging['currentPage'] - 1;
                $arrowBefore        = '<li class="last"><a href="#" class="'.$object->classClick.'" data-id="'.$object->id.'" data-url="'.$object->url.'" data-page="'.$numberPagingBefore.'" onclick="return false;"><i class="fa fa-angle-left"></i></a></li>';
            }else{
                $arrowBefore        = '<li class="last"><a href="#" data-id="'.$object->id.'" data-url="'.$object->url.'" data-page="'.$numberPagingBefore.'" onclick="return false;"><i class="fa fa-angle-left"></i></a></li>';
            }
            $numberPagingBefore = $object->paging['count'];
            if((int)$object->paging['currentPage'] < $object->paging['count']){
                $numberPagingNext = (int)$object->paging['currentPage'] +1;
                $arrowNext        = '<li class="last"><a href="#" class="'.$object->classClick.'" data-id="'.$object->id.'" data-url="'.$object->url.'" data-page="'.$numberPagingNext.'" onclick="return false;"><i class="fa fa-angle-right"></i></a></li>';
            }else{
                $arrowNext        = '<li class="last"><a href="#" onclick="return false;"><i class="fa fa-angle-right"></i></a></li>';
            }
            
            //$arrowNext          = '<li class="last"><a href="#" onclick="return false;"><i class="fa fa-angle-right"></i></a></li>';
            
            $template = '<ul class="pagination">'
                        .$arrowFirst
                        .$arrowBefore
                        .$arrowDotFirst
                        .$buildPaging
                        .$arrowDotLast
                        .$arrowLastNumber
                        .$arrowNext
                        .$arrowLast.
                    '</ul>';
        }
        //die('ok');
        return $template;
    }

    public static function parsingFormatAtInText($string){
        $return         = false;
        $data           = (object)[];
        $param          = 'string';
        $msg            = 'Invalid string foemat';
        $error          = (object)[];
        $atCharacter    = [];
        $userNameFromAt = [];
        if(!empty($string)){
            $arrString = explode(' ', $string);
            if(!empty($arrString)){
                foreach ($arrString as $splitString) {
                    $firstCharacter = substr($splitString, 0,1);
                    //echo $firstCharacter.' = ' .$splitString.'<br/>';
                    if($firstCharacter == '@'){
                        $countCaracterLen = strlen($splitString);
                        //echo $countCaracterLen;
                        $atCharacter[]  = $splitString;
                        $userNameFromAt[] = substr($splitString, 1,$countCaracterLen);
                    }
                }
                if(!empty($userNameFromAt)){
                    $clearUsername = array_unique(array_map(function($val) {
                                        return $val;
                                        }, $userNameFromAt));
                    $arrUserName   = implode(',',$clearUsername);
                    $return        = true;
                    $data          = (object)$arrUserName;
                    $msg           = 'Success';
                }
                
            }   
        }
        $error  = General::errorFormat($param,$msg);
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

    public static function spitDateTimeRange($dateString){
        $dateStartInt   = time();
        $dateEndInt     = time();
        $dateStartStr   = date('m/d/Y',$dateStartInt).' 00:00';
        $dateEndStr     = date('m/d/Y',$dateEndInt).' 23:59';
        //echo 'dateString = '.$dateString.'<br/>';
        if(strlen($dateString) > 10){
            //$dateString = str_replace('to', '-', $dateString);
            $splitDate = explode('-', $dateString);
            if(!empty($splitDate)){
                $dateStartStr   = trim($splitDate[0]).' 00:00';
                $dateEndStr     = trim($splitDate[1]).' 23:59';
                $dateStartInt   = strtotime($dateStartStr);
                $dateEndInt     = strtotime($dateEndStr);
                // echo '<pre>';
                // print_r($splitDate);
                // echo '</pre>';
                // echo 'dateStartStr = '. $dateStartStr.'<br/>';
                // echo 'dateEndStr = '. $dateEndStr.'<br/>';
                // echo 'dateStartInt = '. $dateStartInt.'<br/>';
                // echo 'dateEndInt = '. $dateEndInt.'<br/>';

                // echo 'dateStart = '. date('d-m-Y H:i',$dateStartInt).'<br/>';
                // echo 'dateEnd = '. date('d-m-Y H:i',$dateEndInt).'<br/>';

            }
        }
        $template = (object)['dateStartStr' => $dateStartStr,
                             'dateEndStr'   => $dateEndStr,
                             'dateStartInt' => Zone::setGmtByZero($dateStartInt),
                             'dateEndInt'   => Zone::setGmtByZero($dateEndInt),
                            ];
        return $template;
    }

    public static function curlCampainBatch($object){
        $return = false;
        $data   = (object)[];

        $ch      = curl_init( $object->url );
        $payload = json_encode( $object->data );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, $payload );
        curl_setopt( $ch, CURLOPT_HTTPHEADER,$object->header);
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        $result  = curl_exec($ch);
        curl_close($ch);
        if($result){ 
            $parsingResult = json_decode($result);
            if(!empty($parsingResult->campaign_token)){
                $data = (object)["token"=>$parsingResult->campaign_token];
                $return = true;
            }
        }

        $result = (object)["return"=>$return,"data"=>$data];
        return $result;
    }

    public static function socketPushNotification($object){
        $data   = (object)[];
        $return = false;
        // http://localhost:14045/api/v1/push-notification
        //echo Yii::$app->params['apiSocket']."api/v1/push-notification".'<br/>'.$object->postData;die;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Yii::$app->params['apiSocket']."/api/v1/push-notification");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER ['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$object->postData);
        $result  = curl_exec($ch);
        curl_close($ch);
        //print_r($result);die;
        // if($result){
        //     $parsingResult = json_decode($result);
        //     $return        = $parsingResult->return;
        // }
        $results = (object)["return"=>$return];
        return $results;
    }

      public static function checkNotificationSetting($object){
        $return = true;
        //di disable dulu
        // $checkMemberNotificaiton =  MemberNotificationSetting::find()
        //                                      ->select(['mens_id','mens_mem_id','mens_nof_id','mens_status'])
        //                                      ->where('mens_mem_id=:memberId AND mens_nof_id =:notifId',[':memberId'=>$object->memberId,':notifId'=>$object->notifId])
        //                                      ->one();
        // if(empty($checkMemberNotificaiton)){
        //     $return = true;
        // }else{
        //     if($checkMemberNotificaiton->mens_status == 1){
        //         $return = true;
        //     }
        // }

        $result = (object)["return"=>$return];
        return $result;
    }

    public static function sendSMS($object){
        $return    = false;
        $msg       = "";
        $replacePhoneNumber = str_replace("+", "", $object->phoneNumber);
        $tempPhoneNumber = substr($replacePhoneNumber, 0,5);
       // if($tempPhoneNumber == "62856" || $tempPhoneNumber == "62815" || $tempPhoneNumber == "62896" || $tempPhoneNumber == "62895" || $tempPhoneNumber == "62858" || $tempPhoneNumber == "62857"){

            $objectSms  = (object)['msisdn'  => $object->phoneNumber,
                                   'sender'  => Yii::$app->params['twiloSenderNumber'],
                                   'message' => "SELLTER ".$object->message,
                            ];
            $smsGatway  = New TwiloSms;
            $sendSms    = $smsGatway->send($objectSms);
        // }else{
        //     $objectSms  = (object)['msisdn'  => $object->phoneNumber,
        //                            'message' => $object->message,
        //                             ];
        //     $sendSms = self::sendSmsJatis($objectSms);
        // }
        if($sendSms->return){
            $return = true;
            $msg        = 'Success';
        }else{
            $msg = 'invalid send message';
        }
        $result = (object)["return"=> $return,"msg"=> $msg];
        return $result;
    }

    public static function sendSmsJatis($object){
        $return    = false;
        $msg       = "";
        $ch        = curl_init();  
        $postField = "userid=".Yii::$app->params['userNameJatis']."&password=".Yii::$app->params['passwordJatis']."&msisdn=".$object->msisdn."&message=".$object->message."&sender=SELLTER&division=IT&batchname=sellterotpt&uploadby=sellter&channel=1";

        curl_setopt($ch,CURLOPT_URL,Yii::$app->params['urlJatisSms']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postField); 
        $curlResult  = curl_exec($ch); 
         if($curlResult){
             $parsingResult = explode('&',$curlResult);
             $status = $parsingResult[0];
             if($status == "Status=1"){
                $return = true;
                $msg = "Succes";
             }else{ 
                $msg = $curlResult;
             }
        }else{
            curl_close($ch);
        }
        $result = (object)["return"=> $return,"msg"=> $msg];
        return $result;
    }


    public static function der2pem($fileDer) {
      $pem = chunk_split(base64_encode($fileDer), 64,"");
      //$pem = "-----BEGIN PUBLIC KEY-----\n".$pem."-----END PUBLIC KEY-----\n";

      return $pem;
    }

    public function imageCompreshAll($object)
    {
        $data           = [];
        $msg            = 'Unknow Error';
        $return         = false;
        $param          = 'object';
        $error          = (object)[];
        $imageIsFile    = 0;
        if(!empty($object)){
            if(!empty($object->sourceUrl)){
                $pathImagesLocal    = './runtime/images/';
                //imagePath
                $imageOriginal      = 'original/';//original file from apps
                $imageThumb         = 'thumb/';//convert file original - thumbnail
                $imageThumbnail     = 'thumbnail/';//convert thumbnail

                if (!file_exists($pathImagesLocal.$imageOriginal)) {
                    mkdir($pathImagesLocal.$imageOriginal, 0777, true);
                    exit;
                }
                if (!file_exists($pathImagesLocal.$imageThumb)) {
                    mkdir($pathImagesLocal.$imageThumb, 0777, true);
                    exit;
                }
                if (!file_exists($pathImagesLocal.$imageThumbnail)) {
                    mkdir($pathImagesLocal.$imageThumbnail, 0777, true);
                    exit;
                }

                //geting image name
                //echo 'sourceUrl1 = '.$object->sourceUrl.'<br/>';
                $imageData          = explode(".", basename($object->sourceUrl));
                $imageNameExtention = $imageData['0'].'.'.$imageData['1'];
                if($imageData['1'] == 'file'){
                    $newName = $imageData['0'].'.jpg';
                    if(copy($object->sourceUrl, $pathImagesLocal.$newName)){//copy tosouce
                        if(S3::upload($object->imageS3Path, $pathImagesLocal ,$newName)){
                            $object->sourceUrl  = Yii::$app->params['awsS3Url'].$object->imageS3Path.$newName;
                            $imageIsFile        = 1;
                        }
                    }
                }
                

                $imageInfo          = getimagesize($object->sourceUrl);
                
                //size original
                $widthOriginal      = ceil($imageInfo[0]);
                $heightOriginal     = ceil($imageInfo[1]);

                //size Thumbnail
                $widthThumbnail     = ceil($widthOriginal/3);
                $heightThumbnail    = ceil($heightOriginal/3);
                
                //size Thumb
                $widthThumb         = ceil($widthOriginal-$widthThumbnail);
                $heightThumb        = ceil($heightOriginal-$heightThumbnail);
                
                $imageData          = explode(".", basename($object->sourceUrl));
                $imageName          = $imageData['0'].'.'.$imageData['1'];

                //echo 'sourceUrl2 = '.$object->sourceUrl.'<br/>';
                //echo 'imageName = '.$imageName;
                //die;


                if(Image::thumbnail($object->sourceUrl, $widthOriginal, $heightOriginal)->save(Yii::getAlias($pathImagesLocal.$imageOriginal.$imageName), ['quality' => 95])){
                    if(S3::upload($object->imageS3Path.$imageOriginal, $pathImagesLocal.$imageOriginal ,$imageName)){
                       
                       if(Image::thumbnail($object->sourceUrl, $widthThumb, $heightThumb)->save(Yii::getAlias($pathImagesLocal.$imageThumb.$imageName), ['quality' => 90])){
                            if(S3::upload($object->imageS3Path.$imageThumb, $pathImagesLocal.$imageThumb ,$imageName)){
                                
                                if(Image::thumbnail($object->sourceUrl, $widthThumbnail, $heightThumbnail)->save(Yii::getAlias($pathImagesLocal.$imageThumbnail.$imageName), ['quality' => 80])){
                                    if(S3::upload($object->imageS3Path.$imageThumbnail, $pathImagesLocal.$imageThumbnail ,$imageName)){
                                        $return = true;
                                        $msg    = 'Success';
                                        $objectImageS3Url = (object)[
                                                                        'original'  => Yii::$app->params['awsS3Url'].$object->imageS3Path.$imageOriginal.$imageName,
                                                                        'thumb'     => Yii::$app->params['awsS3Url'].$object->imageS3Path.$imageThumb.$imageName,
                                                                        'thumbnail' => Yii::$app->params['awsS3Url'].$object->imageS3Path.$imageThumbnail.$imageName,
                                                                    ];
                                        $objectImageCachedUrl = (object)[
                                                                        'original'  => Yii::$app->params['imageChacedUrl'].$object->imageS3Path.$imageOriginal.$imageName,
                                                                        'thumb'     => Yii::$app->params['imageChacedUrl'].$object->imageS3Path.$imageThumb.$imageName,
                                                                        'thumbnail' => Yii::$app->params['imageChacedUrl'].$object->imageS3Path.$imageThumbnail.$imageName,
                                                                    ];
                                        if($imageIsFile == 1){
                                            $imageName = $newName;
                                        }
                                        $data   = (object)[
                                                            'S3Url'         => $objectImageS3Url,
                                                            'cachedUrl'     => $objectImageCachedUrl,
                                                            'imageIsFile'   => $imageIsFile,
                                                            'imageName'     => $imageName,
                                                          ];
                                    }else{
                                        $msg = 'invalid upload to S3';
                                    }
                                }else{
                                    $msg = 'invalid create Thumbnail';
                                }
                            }else{
                                $msg = 'invalid upload to S3';
                            }
                        }else{
                            $msg = 'invalid create Thumbnail';
                        }
                    }else{
                        $msg = 'invalid upload to S3';
                    }
                }else{
                    $msg = 'invalid create Thumbnail';
                }
                
            }else{
                $msg = 'invalid object sourceUrl';
            }
        }else{
            $msg = 'invalid object data';
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return (object)$result;
    }

    public function ImageCompreshx($id)
    {
        $post               = Yii::$app->request->post();
        $response           = [];
        $product            = Product::findOne($id);
        $productImage       = new ProductImage;

        if (!file_exists('./runtime/images/')) {
            mkdir('./runtime/images/', 0777, true);
        }
        
        $file               = UploadedFile::getInstance($productImage, 'pri_photo');
        $newFileName        = $this->generateName('prd_'.$product->prd_id);
        $file->saveAs('runtime/images/' . $newFileName . '.' . $file->extension);
        
        $productImage->pri_photo    = $newFileName . '.' . $file->extension;
        $productImage->pri_pit_id   = 1;
        $productImage->pri_prd_id   = $product->prd_id;
        //echo $productImage->pri_prd_id.' - '.$stockOrderDetail->sod_prd_id;die;
        $pathImages         = './runtime/images/';
        $path               = $pathImages.$productImage->pri_photo;

        $pathSource         = 'source/';
        $pathOriginal       = 'original/';
        $pathThumb          = 'thumb/';
        $pathThumbnail      = 'thumbnail/';

        $s3Path             = 'images/products/';

        if (!file_exists($pathImages.$pathSource)) {
            mkdir($pathImages.$pathSource, 0777, true);
        }

        if (!file_exists($pathImages.$pathOriginal)) {
            mkdir($pathImages.$pathOriginal, 0777, true);
        }

        if (!file_exists($pathImages.$pathThumb)) {
            mkdir($pathImages.$pathThumb, 0777, true);
        }

        if (!file_exists($pathImages.$pathThumbnail)) {
            mkdir($pathImages.$pathThumbnail, 0777, true);
        }

        $imageInfo          = getimagesize($path);

        $width              = ceil($imageInfo[0]);
        $height             = ceil($imageInfo[1]);

        $widthThumbnail     = ceil($width/3);
        $heightThumbnail    = ceil($height/3);

        $widthThumb         = ceil($width-$widthThumbnail);
        $heightThumb        = ceil($height-$heightThumbnail);
        if(copy($path, $pathImages.$pathSource.$productImage->pri_photo)){//copy tosouce
            if(S3::upload($s3Path.$pathSource, $pathImages.$pathSource ,$productImage->pri_photo)){
                //original
                if(Image::thumbnail($path, $width, $height)->save(Yii::getAlias($pathImages.$pathOriginal.$productImage->pri_photo), ['quality' => 95])){
                    if(S3::upload($s3Path.$pathOriginal, $pathImages.$pathOriginal ,$productImage->pri_photo)){
                        //thumb
                        if(Image::thumbnail($path, $widthThumb, $heightThumb)->save(Yii::getAlias($pathImages.$pathThumb.$productImage->pri_photo), ['quality' => 90])){
                            if(S3::upload($s3Path.$pathThumb, $pathImages.$pathThumb ,$productImage->pri_photo)){
                                //thumb
                                if(Image::thumbnail($path, $widthThumbnail, $heightThumbnail)->save(Yii::getAlias($pathImages.$pathThumbnail.$productImage->pri_photo), ['quality' => 80])){
                                    if(S3::upload($s3Path.$pathThumbnail, $pathImages.$pathThumbnail ,$productImage->pri_photo)){
                                        if ($productImage->pri_photo !== null) {

                                            Yii::$app->response->getHeaders()->set('Vary', 'Accept');
                                            //Yii::$app->response->format = Response::FORMAT_JSON;

                                            $response = [];
                                            //$host       = Yii::$app->urlManager->hostInfo;
                                            if ($productImage->save(false)) {
                                                $response['files'][] = [
                                                    'name' => $productImage->pri_photo,
                                                    'type' => $productImage->pri_photo,
                                                    'size' => $productImage->pri_photo,
                                                    'url' => Yii::$app->params['productUrl'].$productImage->pri_photo,
                                                    'thumbnailUrl' => Yii::$app->params['productUrl'].'thumbnail/'.$productImage->pri_photo,
                                                    'deleteUrl' =>  Url::to(['uploaddelete', 'name' => $productImage->pri_photo]),
                                                    'deleteType' => 'POST',
                                                ];
                                            } else {
                                                $response[] = ['error' => Yii::t('app', 'Unable to save picture')];
                                            }
                                            @unlink($productImage->pri_photo);
                                        } else {
                                            if ($picture->hasErrors(['picture'])) {
                                                $response[] = ['error' => HtmlHelper::errors($picture)];
                                            } else {
                                                throw new HttpException(500, Yii::t('app', 'Could not upload file.'));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }    
        }

        
        return json_encode($response);
    }

    public static function clearNotificationSocket($object){
        $data   = (object)[];
        $return = false;
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Yii::$app->params['apiSocket']."/api/v1/notification-count");
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER ['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$object->postData);
        $result  = curl_exec($ch);
        curl_close($ch);
        $results = (object)["return"=>$return];
        return $results;
    }

    public static function sendMessageOffcerSocket($object){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $object->url);
        curl_setopt($ch, CURLOPT_FAILONERROR,1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION,1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER ['HTTP_USER_AGENT']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST,"POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,$object->postData);
        $result  = curl_exec($ch);
        curl_close($ch);
    }



    public static function sendPushNotificationCampaign($object){
        $return = false;
        $msg    = "";
        $data   = (object)[];
        $error  = (object)[];


        $url = "http://fcm.googleapis.com/fcm/send";
        $objectNotification = (object)["body"=> $object->message,
                                       "sound"=> 1,
                                       "object"=> $object->badge,
                                       ];
        $objectData = (object)["id"=> $object->fkId,
                                "notifId"=> $object->notifId,
                                "image"=> $object->image,
                                "msg"=> $object->message,
                                "type"=> $object->type,
                                "action"=> $object->type,
                                "sound"=> 0,
                                "badge"=> 1,
                               ];

        // $objectNotification = (object)["body"=> "ini messsage fcm",
        //                               "sound"=> 1,
        //                                "object"=> "1",
        //                                ];
        // $objectData = (object)["title"=> "",
        //                        "msg"=> "ini message",
        //                        "type"=> 1,
        //                        "action"=> 1,
        //                        "id"=> 1,
        //                        "priority"=> 1,
        //                        "notifId"=> 1,
        //                        "image"=> "https://upload.wikimedia.org/wikipedia/en/3/39/Wakerlink.jpg",
        //                        ];

        $data         = [
                 "to"=> "/topics/Campaign",
               // "to" => "cx9wnmhM1LI:APA91bGodych7wyAuIWTZfrV0o9n8hU3Z5oWgBqdK3-_Eqa2Kro0JEx2TBT3Ai9H8nWPgxIA5VYAy15KwRPYFQHixE-0dmY6hi7C_TE-YUr0Cad4PmfmUmeQ_MHKRx8QhY7G40-iUWrb",
                "priority" => "high",
                "notification"=> $objectNotification,
                "data"=> $objectData,

        ];
        $pasrsingData = json_encode ( $data );

        $server_key = "AAAAdsgR1Is:APA91bHg8GPsLRpWz3JE0znbqERaA8hjbTh6Guy0WDbJ-UBmJabdr8viUyvjtrhDpoEE5nl4JG0udkIFySoEhUjLIc9nSAowp0j9wkLqSV1NvNXkcQtoHsj49DKmjuNIi0xgx_czs0cI";

         $headers = array
        (
            'Authorization: key=' . $server_key,
            'Content-Type: application/json'
        );
            
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $pasrsingData);
        $result = curl_exec($curl);
        if ($result === FALSE) {
            $msg = curl_error($curl);
            // die(curl_error($curl));
        }else{
            $return = true;
            $data = json_decode($result);
            $msg = "Succes";
        }
        curl_close ( $curl );
        $results = (object)["return"=> $return,"msg"=> $msg,"data"=> $data,"error"=> $error];
        return $results;
    }

}
