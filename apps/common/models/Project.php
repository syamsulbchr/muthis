<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_project".
 *
 * @property int $prj_id
 * @property string $prj_name
 * @property string|null $prj_code
 * @property string|null $prj_desc
 * @property int|null $prj_pm_adu_id Product manager 
 * @property int|null $prj_tc_adu_id Technical contact
 * @property int|null $prj_tm_adu_id Team manager 
 * @property int|null $prj_pjt_id Project Type
 * @property int|null $prj_svt_id Businness Severity
 * @property int|null $prj_plf_id Platform
 * @property int|null $prj_lfc_id Lifecycle
 * @property int|null $prj_dvp_id
 * @property int $prj_datetime
 * @property int $prj_status 0=Inactive, 1=Active
 * @property int|null $prj_update_adu_id
 */
class Project extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['prj_desc'], 'string'],
            [['prj_pm_adu_id', 'prj_tc_adu_id', 'prj_tm_adu_id', 'prj_pjt_id', 'prj_svt_id', 'prj_plf_id', 'prj_lfc_id', 'prj_dvp_id', 'prj_datetime', 'prj_status', 'prj_update_adu_id'], 'integer'],
            [['prj_name'], 'string', 'max' => 255],
            [['prj_code'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'prj_id' => 'ID',
            'prj_name' => 'Project',
            'prj_code' => 'Project Code',
            'prj_desc' => 'Description',
            'prj_pm_adu_id' => 'Project Manager',
            'prj_tc_adu_id' => 'Technical Contact',
            'prj_tm_adu_id' => 'Team Manager',
            'prj_pjt_id' => 'Project Type',
            'prj_svt_id' => 'Business Severity',
            'prj_plf_id' => 'Platform',
            'prj_lfc_id' => 'Lifecycle',
            'prj_dvp_id' => 'Develope By',
            'prj_datetime' => 'Datetime',
            'prj_status' => 'Status',
            'prj_update_adu_id' => 'User Update',
        ];
    }
    public function beforeSave($insert) {
        $this->prj_datetime = Zone::getGmtZero();;
        $this->prj_update_adu_id = Yii::$app->user->identity->adu_id;
        return true;
    }

    public function getSeverity()
    {
        return $this->hasOne(Severity::className(), ['svt_id' => 'prj_svt_id']);
    }

    public function getLifecycle()
    {
        return $this->hasOne(Lifecycle::className(), ['lfc_id' => 'prj_lfc_id']);
    }
    public function getPlatform()
    {
        return $this->hasOne(Platform::className(), ['plf_id' => 'prj_plf_id']);
    }
}
