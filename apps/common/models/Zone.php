<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_zone".
 *
 * @property integer $zon_id
 * @property string $zon_cny_code
 * @property string $zon_name
 */
class Zone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_zone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['zon_cny_code', 'zon_name'], 'required'],
            [['zon_cny_code'], 'string', 'max' => 2],
            [['zon_name'], 'string', 'max' => 35]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'zon_id' => Yii::t('app', 'Zon ID'),
            'zon_cny_code' => Yii::t('app', 'Zon Cny Code'),
            'zon_name' => Yii::t('app', 'Zon Name'),
        ];
    }

    public static function convertDateToMysql($date,$time='00:00'){
        $explodeDate = explode('/', $date);
        $dateTime = $explodeDate[2].'-'.$explodeDate[1].'-'.$explodeDate[0].' '.$time;
        return strtotime($dateTime);
    }

    public static function setTimeZone($time){
        //$timezone = 'WIT (+07:00) Asia/Jakarta';
        //$offset = 7 * 3600;
        //return $time+$offset;
        return $time;
    }

    public static function getGmtZero(){
        return time();
    }

    public static function getGmtZeroServer(){
        return time();
    }

    public static function getZeroTimeZoneName(){
        $defaultTimeZone = date_default_timezone_get();
        if($defaultTimeZone!='UTC'){
            $defaultTimeZone = 'UTC';
        }
        return $defaultTimeZone;
    }

    public static function setGmtByZero($time,$zone=array()){
        $offset     = 7 * 3600; //jakarta indonesia
        $timestamp  = $time;
        $zeroGmtTimestamp = $timestamp-$offset;
        return $zeroGmtTimestamp;
    }

    public static function setGmtZeroToSeven($time,$zone=array()){
        $offset     = 7 * 3600; //jakarta indonesia
        $timestamp  = $time;
        $zeroGmtTimestamp = $timestamp+$offset;
        return $zeroGmtTimestamp;
    }

    public static function setGmtByZone($time,$zone=array()){
        $offset     = 7 * 3600; //jakarta indonesia
        $timestamp  = $time;
        $timeZoneTimestamp = $timestamp+$offset;
        return $timeZoneTimestamp;
    }

    public static function getTimeZoneFromIpAddress($clientsIpAddress=''){
        // if(empty($clientsIpAddress)){
        //     $clientsIpAddress = self::get_client_ip();
        // }

        // $clientInformation = unserialize(file_get_contents('http://www.geoplugin.net/php.gp?ip='.$clientsIpAddress));

        // $clientsLatitude = $clientInformation['geoplugin_latitude'];
        // $clientsLongitude = $clientInformation['geoplugin_longitude'];
        // $clientsCountryCode = $clientInformation['geoplugin_countryCode'];

        // $timeZone = self::get_nearest_timezone($clientsLatitude, $clientsLongitude, $clientsCountryCode) ;
        // echo '<pre>';
        // print_r($timeZone);
        // echo '</pre>';
        $timeZone = 'UTC';
        return $timeZone;

    }

    public static function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if(getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if(getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if(getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if(getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if(getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

    public static function get_nearest_timezone($cur_lat, $cur_long, $country_code = '') {
        $timezone_ids = ($country_code) ? \DateTimeZone::listIdentifiers(\DateTimeZone::PER_COUNTRY, $country_code)
            : \DateTimeZone::listIdentifiers();

        if($timezone_ids && is_array($timezone_ids) && isset($timezone_ids[0])) {

            $time_zone = '';
            $tz_distance = 0;

            //only one identifier?
            if (count($timezone_ids) == 1) {
                $time_zone = $timezone_ids[0];
            } else {

                foreach($timezone_ids as $timezone_id) {
                    $timezone = new \DateTimeZone($timezone_id);
                    $location = $timezone->getLocation();
                    $tz_lat   = $location['latitude'];
                    $tz_long  = $location['longitude'];

                    $theta    = $cur_long - $tz_long;
                    $distance = (sin(deg2rad($cur_lat)) * sin(deg2rad($tz_lat)))
                        + (cos(deg2rad($cur_lat)) * cos(deg2rad($tz_lat)) * cos(deg2rad($theta)));
                    $distance = acos($distance);
                    $distance = abs(rad2deg($distance));
                    // echo '<br />'.$timezone_id.' '.$distance;

                    if (!$time_zone || $tz_distance > $distance) {
                        $time_zone   = $timezone_id;
                        $tz_distance = $distance;
                    }

                }
            }
            return  $time_zone;
        }
        return 'unknown';
    }

     public static function setTimeGmtFromZoneStringNameToZeroGmt($zoneStringName,$time){
        $offset             = Yii::$app->params['defaultOffset'];
        $timeHours          = Yii::$app->params['defaultTimePerHours'];
        $getObjectOffset    = self::getOffsetByStringName($zoneStringName,$time);
        if($getObjectOffset->return){
            $offset = $getObjectOffset->data->offset;
        }
        $hours = $offset / $timeHours;
        if($hours > 0){
            return $time-$offset;
        }else{
            return $time+$offset;
        }   
    }

     public static function setTimeGmtFromZoneStringName($zoneStringName,$time){
        $offset             = Yii::$app->params['defaultOffset'];
        $timeHours          = Yii::$app->params['defaultTimePerHours'];
        $getObjectOffset    = self::getOffsetByStringName($zoneStringName,$time);
        if($getObjectOffset->return){
            $offset = $getObjectOffset->data->offset;
        }
        $hours = $offset / $timeHours;
        if($hours > 0){
            return $time+$offset;
        }else{
            return $time-$offset;
        }   
    }
}
