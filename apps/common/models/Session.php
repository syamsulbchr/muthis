<?php

namespace common\models;

use Yii;
use common\modelsMongo\TblSession;
use common\modelsRedis\RedisSession;
/**
 * This is the model class for table "tbl_session".
 *
 * @property integer $ses_id
 * @property integer $ses_sest_id
 * @property integer $ses_usr_id
 * @property integer $ses_anr_id
 * @property string $ses_key
 * @property integer $ses_activity
 * @property integer $ses_valid
 * @property string $ses_last_ip
 * @property integer $ses_create_datetime
 * @property integer $ses_app_type
 * @property integer $ses_device_name
 */
class Session extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_session';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ses_usr_id', 'ses_sest_id', 'ses_anr_id','ses_activity', 'ses_valid','ses_lang_id', 'ses_create_datetime', 'ses_app_type','ses_dvc_id','ses_datetime_socket_connect','ses_datetime_socket_disconect','ses_socket_status'], 'integer'],
            [['ses_device_id','ses_socket_key','ses_device_name'], 'string', 'max' => 255],
            [['ses_device_name','ses_key'], 'string'],
            [['ses_last_ip','ses_time_zone_name','ses_time_zone_gmt'], 'string', 'max' => 100],
            [['ses_time_zone_offset'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ses_id' => 'Ses ID',
            'ses_sest_id', 'Session Type',
            'ses_usr_id' => 'Ses Usr ID',
            'ses_anr_id' => 'Ses Anr ID',
            'ses_lang_id' => 'Language',
            'ses_key' => 'Ses Key',
            'ses_activity' => 'Ses Activity',
            'ses_valid' => 'Ses Valid',
            'ses_last_ip' => 'Ses Last Ip',
            'ses_create_datetime' => 'Ses Create Datetime',
            'ses_app_type' => 'Ses App Type',
            'ses_device_id' => 'Device Id',
            'ses_dvc_id' => 'Device Type',
            'ses_time_zone_name' => 'Time Zone Name',
            'ses_time_zone_gmt' => 'Time Zone GMT',
            'ses_time_zone_offset' => 'Time Zone Offset',
            'ses_device_name' => 'Device Name',
            'ses_socket_key' => 'ses_socket_key',
            'ses_datetime_socket_connect' => 'ses_datetime_socket_connect',
            'ses_datetime_socket_disconect' => 'ses_datetime_socket_disconect',
            'ses_socket_status' => 'ses_socket_status',
        ];
    }

    public function beforeSave($insert) {
        //geting data from GMT 0
        $zeroTimeZoneName       = Zone::getZeroTimeZoneName();
        $zeroTimeZoneDate       = new \DateTimeZone($zeroTimeZoneName);
        $zeroDateTime           = new \DateTime('now',$zeroTimeZoneDate);

        //Geting data information from client acces
        $clientsIpAddress       = Zone::get_client_ip();
        $clientTimeZoneName     = Zone::getTimeZoneFromIpAddress($clientsIpAddress);
        $clientDateTimeZone     = new \DateTimeZone($clientTimeZoneName);
        $clientDateTime         = new \DateTime('now',$clientDateTimeZone);
        $gmtZone                = $clientDateTime->format('P');
        $offsetTimeZone         = $clientDateTimeZone->getOffset($zeroDateTime);
        $this->ses_activity     = Zone::getGmtZero();
        
        if(parent::beforeSave($insert)) {
            $this->ses_last_ip = $clientsIpAddress;
            //$this->ses_app_type = self::MOBILE;
            $this->ses_valid                = strtotime("+1 year");
            $this->ses_time_zone_name       = $clientTimeZoneName;
            $this->ses_time_zone_gmt        = $gmtZone;
            $this->ses_time_zone_offset    = $offsetTimeZone;
            if ($this->isNewRecord) {
                $this->ses_create_datetime = Zone::getGmtZero();
            }
            return true;
        }
        return false;
    }

    public function getUser() {
        return $this->hasOne(User::className(), ['usr_id' => 'ses_usr_id'])
                    ->select('usr_id,usr_username,usr_phone_register,usr_email,usr_password,usr_type_id,usr_auth_key');
    }

    public static function insertNew($userId,$token,$type=1,$languageId=1) {
        $model = new Session();
        $model->ses_usr_id      = $userId;
        $model->ses_key         = $token;
        $model->ses_app_type    = $type;
        $model->ses_lang_id     = $languageId;
        if($model->save()){
          return $model;  
        }else{
            return false;
        }
           
    }

    public static function insertNewSession($object) {
        $return         = false;
        $param          = 'object';
        $msg            = 'Invalid session object data';
        $data           = [];
        $error          = (object)[];
        $transaction    = Yii::$app->db->beginTransaction();

        if(isset($object)){
            if(isset($object->userId)){
                if(isset($object->token)){
                    if(isset($object->type)){
                        if(isset($object->languageId)){
                            $model = new Session();
                            $model->ses_usr_id      = $object->userId;
                            $model->ses_key         = $object->token;
                            $model->ses_app_type    = $object->type;
                            $model->ses_lang_id     = $object->languageId;
                            $model->ses_dvc_id      = $object->deviceTypeId;
                            if($model->save()){
                                $transaction->commit();
                                $return = true;
                                $msg    = 'Success';
                                $data   = $model;
                            }else{
                                $msg = 'invalid save session data';
                                $transaction->rollBack();
                                echo '<pre>';
                                print_r($this->getErrors());
                                echo '</pre>';
                            }
                        }else{
                            $msg = 'invalid paramater language';
                        }
                    }else{
                        $msg = 'invalid paramater language';
                    }
                }else{
                    $msg = 'invalid paramater language';
                }
            }else{
                $msg = 'invalid paramater language';
            }
        }
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;   
    }

    public static function insertNewSaveMongox($userId,$token,$type=1,$languageId=1) {
        $model = new Session;
        $model->ses_usr_id      = $userId;
        $model->ses_key         = $token;
        $model->ses_app_type    = $type;
        $model->ses_lang_id     = $languageId;
        if($model->saveData($model)->return){
            return $model;  
        }else{
            return false;
        }      
    }

    public static function insertNewSaveMongo($userId,$token,$type=1,$languageId=1) {
        // $return     = false;
        // $data       = [];
        // $msg        = 'invalid session object';
        // $error      = (object)[];
        $model = new Session;
        $model->ses_usr_id      = $userId;
        $model->ses_key         = $token;
        $model->ses_app_type    = $type;
        $model->ses_lang_id     = $languageId;

        $saveModel              = $model->saveData($model);
        $return                 = $saveModel->return;
        $data                   = $saveModel->data;;
        $msg                    = $saveModel->msg;;
        $error                  = $saveModel->error;;
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;   
    }

    public static function add($userId,$sessionTypeId=1) {
        $model = new Session();
        $model->ses_usr_id = $userId;
        $model->ses_sest_id = $sessionTypeId;
        $model->ses_key = sha1($userId . time());

        Yii::$app->session->set('ebz_session_key', $model->ses_key);

        if ($model->save())
            return $model;
        return false;
    }

    public static function addSessionBackend($userId,$sessionTypeId=1) {
        $shaToken           = sha1($userId . time());
        $hash               = Helper::passwordHash($shaToken);
        $deviceTypeId       = 3;
        $model              = new Session();
        $model->ses_usr_id  = $userId;
        $model->ses_sest_id = $sessionTypeId;
        $model->ses_key     = $hash;
        $model->ses_dvc_id  = $deviceTypeId;
        Yii::$app->session->set('sellter_session_key', $model->ses_key);
        //User::deleteAll(['and', 'type = :type_id', ['not in', 'usercategoryid', $categoriesList]], [':type_id' => 2]);
        //Session::deleteAll('ses_usr_id = :adminUserId AND ses_sest_id = :sessionTypeId AND ', [':adminUserId' => $model->ses_usr_id, ':sessionTypeId' => $model->ses_sest_id])
        //if(Session::deleteAll('ses_usr_id = :adminUserId AND ses_sest_id = :sessionTypeId', [':adminUserId' => $model->ses_usr_id, ':sessionTypeId' => $model->ses_sest_id])){
            if($model->save()){
                $adminUser = AdminUser::find()->where('adu_id=:adminUserId',[':adminUserId'=>$model->ses_usr_id])->one();
                if(!empty($adminUser)){
                    //$phoneNumber            = '+62 81234567890';
                    // $redisSession           = New RedisSession;
                    // $objectSaveDataRedis    = (object)[
                    //                                     'id'            => $model->ses_id,
                    //                                     'sessionTypeId' => $sessionTypeId,
                    //                                     'tokenKey'      => $model->ses_key,
                    //                                     'userId'        => $model->ses_usr_id,
                    //                                     'memberId'      => $model->ses_usr_id,
                    //                                     'deviceTypeId'  => $deviceTypeId,
                    //                                     'userName'      => $adminUser->adu_username,
                    //                                     'email'         => $adminUser->adu_email,
                    //                                     'phone'         => $phoneNumber,
                    //                                     'screenName'    => $adminUser->adu_screen_name,

                    //                                   ];
                    // $saveRedisSessionData = $redisSession->saveDataAdmin($objectSaveDataRedis);
                    // $return = $saveRedisSessionData->return;
                    // $data   = $saveRedisSessionData->data;
                    // $msg    = $saveRedisSessionData->msg;
                    // $error  = $saveRedisSessionData->error;
                    return $model;
                }
                
            }else{
                die('1');
                return false;
            }
        // }else{

        //     die('2');
        //     return false;
        // }
    }

    

    public static function check($key) {
        // echo 'key = '.$key;die;
        $model = Session::find()->select('ses_id')->where('ses_key=:key',[':key'=>trim($key)])->one();
        if($model) {
            return true;
        }
        return false;
    }

    public static function getObjectSessionLogin($key){
        $model = Session::find()->select('ses_id')->where('ses_key=:key',[':key'=>trim($key)])->one();
        if($model) {
            return $model;
        }
        return (object)[];
    }

    public static function getUserByToken($key) {
        $model  = Session::find()->select('ses_id,ses_valid,ses_usr_id')->where('ses_key=:token',[':token' => trim($key)])->one();
        $user   = [];
        if ($model && $model->ses_valid > time()) {
            //if ($model->save()) {
                $user = User::findOne($model->ses_usr_id);
            //}
        }
        return $user;
    }

    public static function getCountSesionLogin(){
        return self::find()->select('ses_usr_id')->groupBy('ses_usr_id')->count();
    }

    public static function getCountSesionActive($date=[]){
        
        if(isset($date->dateStartInt)){
            $dateStartInt   = $date->dateStartInt;
            $dateEndInt     = $date->dateEndInt;
            return self::find()->select('ses_usr_id')->where('ses_activity >= :dateStartInt AND ses_activity <= :dateEndInt',[':dateStartInt'=>$dateStartInt,':dateEndInt'=>$dateEndInt])->groupBy(['ses_usr_id']);;
        }else{
            return self::find()->select('ses_usr_id')->groupBy(['ses_usr_id']);;
        }
        
    }

    public function saveData(){
        $return                 = false;
        $data                   = [];
        $msg                    = 'unknow error';
        $param                  = 'token';
        $transaction            = Yii::$app->db->beginTransaction();
        if($this->save()){
            $modelsMongo = TblSession::find()->where(['ses_id' => (int)$this->ses_id])->one();
            if(empty($modelsMongo)){
                $modelsMongo = New TblSession;
            }
            
            if($modelsMongo->saveData($this)->return){
                $transaction->commit();
                $return = true;
                $msg    = 'Success';    
            }
        }else{
            $transaction->rollBack();
            echo '<pre>';
            print_r($this->getErrors());
            echo '</pre>';
        }
        $error  = General::errorFormat($param,$msg);
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

    public static function buildDataRedisMember($object){
        $return                 = false;
        $data                   = [];
        $msg                    = 'unknow error';
        $param                  = 'token';
        if(!empty($object)){
            $redisSession           = New RedisSession;
            $objectSaveDataRedis    = (object)[
                                                'id'            => $object->session->ses_id,
                                                'tokenKey'      => $object->session->ses_key,
                                                'userId'        => $object->session->ses_usr_id,
                                                'memberId'      => $object->user->member->mem_id,
                                                'deviceTypeId'  => $object->session->ses_dvc_id,
                                                'userName'      => $object->user->usr_username,
                                                'email'         => $object->user->usr_email,
                                                'phone'         => $object->user->usr_phone_register,
                                                'screenName'    => $object->user->member->mem_screen_name,

                                              ];
            $saveRedisSessionData = $redisSession->saveData($objectSaveDataRedis);
            $return = $saveRedisSessionData->return;
            $data   = $saveRedisSessionData->data;
            $msg    = $saveRedisSessionData->msg;
            $error  = $saveRedisSessionData->error;
            
        }else{
            $msg = 'invalid object build chaced data';
        }
        $error  = General::errorFormat($param,$msg);
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return $result;
    }

}
