<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_repository".
 *
 * @property int $rpy_id
 * @property int|null $rpy_prj_id
 * @property string $rpy_name
 * @property string|null $rpy_desc
 * @property string|null $rpy_url
 * @property int $rpy_datetime
 * @property int $rpy_status 0=Inactive, 1=Active
 * @property int|null $rpy_update_adu_id
 */
class Repository extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_repository';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rpy_prj_id', 'rpy_datetime', 'rpy_status', 'rpy_update_adu_id'], 'integer'],
            [['rpy_desc', 'rpy_url'], 'string'],
            [['rpy_name'], 'string', 'max' => 255],
            [['rpy_code'], 'string', 'max' => 5],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'rpy_id' => 'ID',
            'rpy_prj_id' => 'Project',
            'rpy_name' => 'Name',
            'rpy_code' => 'Code',
            'rpy_desc' => 'Desc',
            'rpy_url' => 'Url',
            'rpy_datetime' => 'Datetime',
            'rpy_status' => 'Status',
            'rpy_update_adu_id' => 'User Update',
        ];
    }
    public function beforeSave($insert) {
        $this->rpy_datetime = Zone::getGmtZero();;
        $this->rpy_update_adu_id = Yii::$app->user->identity->adu_id;
        return true;
    }

    public function getProject()
    {
        return $this->hasOne(Project::className(), ['prj_id' => 'rpy_prj_id']);
    }
}
