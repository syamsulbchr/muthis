<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_department".
 *
 * @property integer $dpt_id
 * @property string $dpt_name
 * @property integer $dpt_datetime
 * @property integer $dpt_status
 */
class Department extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_department';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['dpt_com_id','dpt_datetime', 'dpt_status','dpt_datetime_insert','dpt_datetime_update','dpt_datetime_delete','dpt_adu_id_insert','dpt_adu_id_update','dpt_adu_id_delete',], 'integer'],
            [['dpt_name','dpt_code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'dpt_id' => 'ID',
            'dpt_com_id' => 'Company',
            'dpt_cot_id' => 'Company Type',
            'dpt_name' => 'Name',
            'dpt_code'  => 'Code',
            'dpt_datetime' => 'Last Update',
            'dpt_datetime_insert' => 'Datetime Insert',
            'dpt_datetime_update' => 'Datetime Update',
            'dpt_datetime_delete' => 'Datetime Delete',
            'dpt_adu_id_insert' => 'User Insert',
            'dpt_adu_id_update' => 'User Update',
            'dpt_adu_id_delete' => 'User Delete',
            'dpt_status' => 'Status',
        ];
    }

    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['com_id' => 'dpt_com_id']);
    }

    public function getCompanytype()
    {
        return $this->hasOne(CompanyType::className(), ['cot_id' => 'dpt_cot_id']);
    }

    public function getStatus() {
        if ($this->dpt_status==0){
            return 'Not Active';
        }else if ($this->dpt_status==1){
            return 'Active';
        }else if ($this->dpt_status==2){
            return 'Deleted';
        }else{    
            return 'Unknow';
        }
        
        
    }
}
