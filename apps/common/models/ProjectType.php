<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_project_type".
 *
 * @property int $pjt_id
 * @property string $pjt_name
 * @property string|null $pjt_code
 * @property string|null $pjt_desc
 * @property int $pjt_datetime
 * @property int $pjt_status 0=Inactive, 1=Active
 * @property int|null $pjt_update_adu_id
 */
class ProjectType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_project_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['pjt_desc'], 'string'],
            [['pjt_datetime', 'pjt_status', 'pjt_update_adu_id'], 'integer'],
            [['pjt_name'], 'string', 'max' => 255],
            [['pjt_code'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'pjt_id' => 'Pjt ID',
            'pjt_name' => 'Pjt Name',
            'pjt_code' => 'Pjt Code',
            'pjt_desc' => 'Pjt Desc',
            'pjt_datetime' => 'Pjt Datetime',
            'pjt_status' => 'Pjt Status',
            'pjt_update_adu_id' => 'Pjt Update Adu ID',
        ];
    }
}
