<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_admin_user_menu".
 *
 * @property integer $asm_id
 * @property integer $asm_parent_id
 * @property string $asm_name
 * @property string $asm_icon
 * @property string $asm_url
 * @property integer $asm_sort
 * @property integer $asm_have_child
 * @property integer $asm_status
 */
class AdminUserMenu extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_admin_user_menu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['asm_parent_id','asm_sort', 'asm_have_child', 'asm_status'], 'integer'],
            [['asm_name', 'asm_icon', 'asm_url'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'asm_id' => 'ID',
            'asm_parent_id' => 'Parent',
            'asm_name' => 'Name',
            'asm_icon' => 'Awsome Icon',
            'asm_sort' => 'Sort',
            'asm_url' => 'Url',
            'asm_have_child' => 'Have Child',
            'asm_status' => 'Status',
        ];
    }

    public function getMenuaccess()
    {
        return $this->hasOne(AdminGroupMenuAccess::className(), ['aga_asm_id' => 'asm_id']);
    }
}
