<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_severity".
 *
 * @property int $svt_id
 * @property string $svt_name
 * @property string|null $svt_code
 * @property string|null $sct_desc
 * @property string|null $svt_collor_code
 * @property int $svt_datetime
 * @property int $svt_status 0=Inactive, 1=Active
 * @property int|null $svt_update_adu_id
 */
class Severity extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_severity';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sct_desc'], 'string'],
            [['svt_datetime', 'svt_status', 'svt_update_adu_id', 'svt_sla'], 'integer'],
            [['svt_name'], 'string', 'max' => 255],
            [['svt_code'], 'string', 'max' => 100],
            [['svt_collor_code'], 'string', 'max' => 8],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'svt_id' => 'Svt ID',
            'svt_name' => 'Svt Name',
            'svt_code' => 'Svt Code',
            'sct_desc' => 'Sct Desc',
            'svt_collor_code' => 'Svt Collor Code',
            'svt_sla' => 'SLA',
            'svt_datetime' => 'Svt Datetime',
            'svt_status' => 'Svt Status',
            'svt_update_adu_id' => 'Svt Update Adu ID',
        ];
    }
}
