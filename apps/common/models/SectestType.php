<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_sectest_type".
 *
 * @property int $stt_id
 * @property string $stt_name
 * @property string|null $stt_code
 * @property string|null $stt_desc
 * @property string|null $stt_collor_code
 * @property int|null $stt_sla
 * @property int $stt_datetime
 * @property int $stt_status 0=Inactive, 1=Active
 */
class SectestType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_sectest_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['stt_desc'], 'string'],
            [['stt_sla', 'stt_datetime', 'stt_status'], 'integer'],
            [['stt_name'], 'string', 'max' => 255],
            [['stt_code'], 'string', 'max' => 100],
            [['stt_collor_code'], 'string', 'max' => 8],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'stt_id' => 'Stt ID',
            'stt_name' => 'Stt Name',
            'stt_code' => 'Stt Code',
            'stt_desc' => 'Stt Desc',
            'stt_collor_code' => 'Stt Collor Code',
            'stt_sla' => 'Stt Sla',
            'stt_datetime' => 'Stt Datetime',
            'stt_status' => 'Stt Status',
        ];
    }
}
