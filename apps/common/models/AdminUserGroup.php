<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_admin_user_group".
 *
 * @property integer $aug_id
 * @property string $aug_name
 * @property string $aug_desck
 * @property integer $aug_datetime
 * @property integer $aug_datetime_insert
 * @property integer $aug_datetime_update
 * @property integer $aug_datetime_delete
 * @property integer $aug_adu_id_insert
 * @property integer $aug_adu_id_update
 * @property integer $aug_adu_id_delete
 * @property integer $aug_status
 */
class AdminUserGroup extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tbl_admin_user_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['aug_desck'], 'string'],
            [['aug_datetime', 'aug_datetime_insert', 'aug_datetime_update', 'aug_datetime_delete', 'aug_adu_id_insert', 'aug_adu_id_update', 'aug_adu_id_delete', 'aug_status'], 'integer'],
            [['aug_name','aug_code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'aug_id' => 'Aug ID',
            'aug_name' => 'Name',
            'aug_code' => 'Code',
            'aug_desck' => 'Desck',
            'aug_datetime' => 'Last Update',
            'aug_datetime_insert' => 'Aug Datetime Insert',
            'aug_datetime_update' => 'Aug Datetime Update',
            'aug_datetime_delete' => 'Aug Datetime Delete',
            'aug_adu_id_insert' => 'Aug Adu Id Insert',
            'aug_adu_id_update' => 'Update By',
            'aug_adu_id_delete' => 'Aug Adu Id Delete',
            'aug_status' => 'Status',
        ];
    }

    public function beforeSave($insert) {
        //if (parent::beforeSave($insert)) {
            $this->aug_datetime = Zone::getGmtZero();;
            if ($this->isNewRecord) {
                $this->aug_datetime_insert = Zone::getGmtZero();
                $this->aug_adu_id_insert = Yii::$app->user->identity->adu_id;
            }else{
                $this->aug_datetime_update = Zone::getGmtZero();
                $this->aug_adu_id_update = Yii::$app->user->identity->adu_id;
            }
            
        //}
        return true;
    }

    public function getStatus() {
        if ($this->aug_status==0){
            return 'Not Active';
        }else if ($this->aug_status==1){
            return 'Active';
        }else if ($this->aug_status==2){
            return 'Deleted';
        }else{    
            return 'Unknow';
        }
        
        
    }
}
