<?php 
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Gentayu Security Dasboard';
$this->params['breadcrumbs'][] = $this->title;
?>
    <div>
      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <div class="wraper-login-header" style="text-align: center;">
                <img src="/themes/gentayu/img/gentayu-logo.png">
                <h1 >Gentayu Security Dasboard</h1> 
            </div>
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->field($model, 'username')->textInput(['autofocus' => true,'placeholder'=>'Username'])->label(false) ?>
            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false) ?>
            <div class="form-group">
                <div id="rememberMe" style="padding-top: 10px; float: left;"><?= $form->field($model, 'rememberMe')->checkbox() ?></div>
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary pull-right', 'name' => 'login-button',"style"=>"border:#999999"]) ?>
                <br clear="all" />
            </div>
              <div class="separator">
                  Copyright &copy; 2020 <a href="mailto:sandyQx@gmail.com">Sandi Ardyansyah
              </div>
            <?php ActiveForm::end(); ?>
          </section>
        </div>
      </div>
    </div>
<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm *

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Gentayu Dashboar';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="wraper-login-header">
        <img src="/themes/gentayu/img/logo.png" style="float: left; margin-right: 20px;">
        <h2 style="float: left;margin-top: 15px;"><?= Html::encode($this->title) ?></h2>   
    </div>
    <br clear="all">
    <p style="margin-top: 10px;">Please fill out the following fields to login:</p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
                <?= $form->field($model, 'password')->passwordInput() ?>
                <?= $form->field($model, 'rememberMe')->checkbox() ?>
                <div class="form-group">
                    <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
*/?>

