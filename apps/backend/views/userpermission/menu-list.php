<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\switchinput\SwitchBox;
use common\models\UserPermission;
use common\models\AdminUserMenu;

$this->title = Yii::t('app', $title);
?>
<style>
.trmenu{ height:47px;}
</style>
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?php echo Yii::$app->homeUrl ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span><?=$this->title;?></span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">
	<i class="fa fa-plus-square"></i> <?=$this->title;?> <small><?php echo $pageDesc;?></small>
    <div class="input-group pull-right">
        <a style="cursor:pointer" class="btn green btn-outline pull-right" href="<?= Yii::$app->urlManager->createUrl('/userpermission/menu-create'); ?>"><i class="fa fa-plus-square"></i> <?=Yii::t('app', 'New Menu');;?></a>
        
    </div>  
</h1>
<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]);
		
	
?>

<div class="row">
    <div class="col-md-12">
        <div class="panel panel-sky">
            <div class="panel-body collapse in toggle-light table-responsive">
                <table class="table table-hover table-striped table-bordered">
                  <tr>
                    <td>No</td>
                    <td>Parent</td>
                    <td>child</td>
                    <td>Url</td>
                    <td>icon</td>
                    <td>Sort</td>
                    <td>Status</td>
                    <td colspan="2">Action</td>
                  </tr>
                  
                  <?php
				  $no1 = 0;
                  foreach($menus as $menu){
					  $no1 = $no1+1;
					  $childs = AdminUserMenu::find()->where(['asm_parent_id'=>$menu->asm_id])->orderBy(['asm_sort' => SORT_ASC])->all();
					  if($menu->asm_id == $id){
				  ?>
                          <tr>
                            <td><?=$no1;?></td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$menu->asm_name;?>"></td>
                            <td>&nbsp;</td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$menu->asm_url;?>"></td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$menu->asm_icon;?>"></td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$menu->asm_sort;?>"></td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$menu->asm_status;?>"></td>
                            <td><?=Html::a('<i class="fa fa-save"></i> Save',['/userpermission/menu-update?id=' . $menu->asm_id],['class' => 'btn green btn-outline btn-sm']);?></td>
                            <td><?=Html::a('<i class="fa fa-times"></i> Cance',['/userpermission/menu-list'],['class' => 'btn btn-default btn-sm']);;?></td>
                          </tr>
                  <?php }else{ ?>
                          <tr>
                            <td><?=$no1;?></td>
                            <td><?=$menu->asm_name;?></td>
                            <td>&nbsp;</td>
                            <td><?=$menu->asm_url;?></td>
                            <td><?=$menu->asm_icon;?></td>
                            <td><?=$menu->asm_sort;?></td>
                            <td><?=$menu->asm_status;?></td>
                            <td><?=Html::a('<i class="fa fa-pencil"></i> Update',['/userpermission/menu-update?id=' . $menu->asm_id],['class' => 'btn green btn-outline btn-sm']);?></td>
                            <td><?=Html::a('<i class="fa fa-trash-o"></i> Delete',['/userpermission/menu-delete?id=' . $menu->asm_id],['class' => 'btn red btn-outline btn-sm']);;?></td>
                          </tr>
                  <?php } ?>
                  <?php 
				  foreach($childs as $child){
				  	if($child->asm_id == $id){
				  ?>
                          <tr>
                            <td><?=$no1;?></td>
                            <td>&nbsp;</td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$child->asm_name;?>"></td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$child->asm_url;?>"></td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$child->asm_icon;?>"></td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$child->asm_sort;?>"></td>
                            <td><input type="text" name="" id="" class="form-control" value="<?=$child->asm_status;?>"></td>
                            <td><?=Html::a('<i class="fa fa-save"></i> Save',['/userpermission/menu-update?id=' . $child->asm_id],['class' => 'btn green btn-outline btn-sm']);?></td>
                            <td><?=Html::a('<i class="fa fa-times"></i> Cance',['/userpermission/menu-list'],['class' => 'btn btn-default btn-sm']);;?></td>
                          </tr>
                  <?php }else{ ?>
                          <tr>
                            <td><?=$no1;?></td>
                            <td>&nbsp;</td>
                            <td><?=$child->asm_name;?></td>
                            <td><?=$child->asm_url;?></td>
                            <td><?=$child->asm_icon;?></td>
                            <td><?=$child->asm_sort;?></td>
                            <td><?=$child->asm_status;?></td>
                            <td><?=Html::a('<i class="fa fa-pencil"></i> Update',['/userpermission/menu-update?id=' . $child->asm_id],['class' => 'btn green btn-outline btn-sm']);?></td>
                            <td><?=Html::a('<i class="fa fa-trash-o"></i> Delete',['/userpermission/menu-delete?id=' . $child->asm_id],['class' => 'btn red btn-outline btn-sm']);;?></td>
                          </tr>
                  <?php }
				  	}
				  }
				  ?>
                  
                </table>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>