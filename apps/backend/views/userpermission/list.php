<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use common\models\CustomFieldGroup;
//use common\models\Application;

//$app = new Application();

$this->title = 'Update Action';
?>
<h2><i class="fa fa-user"></i> <?= $this->title ?></h2>
<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]);
?>
<div class="row">
    <div class="col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-body">
                <?php foreach ($data as $row): ?>
                    <div class="col-lg-4">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <div class="col-lg-1"><?= Html::checkbox('action[]', 1, ['value' => $row]) ?></div>
                                    <label class="col-lg-9 control-label"><?= $row ?></label>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3">
                        <div class="btn-toolbar">
                            <button class="btn-primary btn"><i class="fa fa-check"></i> <?= Yii::t('app', 'Save') ?></button>
                            <a href="<?= Yii::$app->urlManager->createAbsoluteUrl(['userpermission/index']) ?>" class="btn-default btn"><i class="fa fa-times"></i> <?= Yii::t('app', 'Cancel') ?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
