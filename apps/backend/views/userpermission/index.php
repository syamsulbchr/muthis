<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use dosamigos\switchinput\SwitchBox;
use common\models\UserPermission;

$this->title = Yii::t('app', 'Permission');
?>
<h2><i class="fa fa-user"></i> <?= $this->title ?></h2>
<div class="row">
</div>
<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]);
?>
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-sky">
            <div class="panel-body collapse in toggle-light table-responsive">
                <table class="table">
                    <tr>
                        <td></td>
                        <?php foreach ($groups as $group): ?>
                            <td><?= $group->aug_name ?></td>
                        <?php endforeach; ?>
                    </tr>
                    <tr>
                        <td>
                            <table class="table">
                                <?php foreach ($actions as $action): ?>
                                    <tr>
                                        <td class="switch-td"><?= $action ?></td>
                                    </tr>
                                <?php endforeach; ?> 
                            </table> 
                        </td>
                        <?php foreach ($groups as $group): ?>
                            <td>
                                <table>
                                    <?php foreach ($actions as $action): ?>
                                        <tr>
                                            <td>
                                                <?php echo  Html::hiddenInput('action[' . $action . '-' . $group->aug_id . ']',0) ?>
                                                <?php echo
                                                SwitchBox::widget([
                                                    'name' => 'action[' . $action . '-' . $group->aug_id . ']',
                                                    'checked' => UserPermission::checkPermissionAvailable($group->aug_id, $action),
                                                    'clientOptions' => [
                                                        'size' => 'small',
                                                        'onColor' => 'success',
                                                        'offColor' => 'danger'
                                                ]]);
                                                ?>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?> 
                                </table> 
                            </td>
                        <?php endforeach; ?>
                    </tr>
                </table>
            </div>
            <div class="panel-footer">
                <div class="row">
                    <div class="btn-toolbar">
                        <button class="btn-primary btn"><i class="fa fa-check"></i> <?= Yii::t('app', 'Save') ?></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>