<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use dosamigos\switchinput\SwitchBox;
use common\models\UserPermission;
use common\models\AdminUserMenu;
use common\models\AdminGroupMenuAccess;
//use kartik\widgets\SwitchInput;

$this->title = Yii::t('app', $title);
?>
<style>
.trmenu{ height:47px;}
</style>
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?php echo Yii::$app->homeUrl ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span><?=$this->title;?></span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<!-- BEGIN PAGE TITLE-->
<h1 class="page-title">
	<i class="fa fa-plus-square"></i> <?=$this->title;?> <small><?php echo $pageDesc;?></small>
    <div class="input-group pull-right">
        <a style="cursor:pointer" class="btn green btn-outline pull-right" href="<?= Yii::$app->urlManager->createUrl('/userpermission/menu-create'); ?>"><i class="fa fa-plus-square"></i> <?=Yii::t('app', 'New Menu');;?></a>
        
    </div>  
</h1>
<?php
$form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'fieldConfig' => [
                'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
                'labelOptions' => ['class' => 'col-lg-3 control-label'],
            ],
        ]);
		
	
?>
<input type="hidden" name="url_change_access" id="url_change_access" value="<?= Yii::$app->urlManager->createUrl('/userpermission/menu-change-access'); ?>">
<div class="row">
    <div class="col-md-12">
        <div class="panel panel-sky">
            <div class="panel-body collapse in toggle-light table-responsive">
                <table class="table table-hover table-striped table-bordered">
                  <tr>
                    <td>No</td>
                    <td>Parent</td>
                    <td>child</td>
                    <td>Url</td>
                    <?php foreach ($groups as $group): ?>
                        <td><?= $group->aug_name ?></td>
                    <?php endforeach; ?>
                    
                  </tr>
                  
                  <?php
				  
				  $no1 = 0;
                  foreach($menus as $menu){
					  $no1 = $no1+1;
					  $childs = AdminUserMenu::find()->where(['asm_parent_id'=>$menu->asm_id])->orderBy(['asm_sort' => SORT_ASC])->all();
				  ?>
                  <tr>
                    <td><?=$no1;?></td>
                    <td><?=$menu->asm_name;?></td>
                    <td>&nbsp;</td>
                    <td><?=$menu->asm_url;?></td>
                    <?php foreach ($groups as $group){ 
						$parrentAdminGroupAccess = AdminGroupMenuAccess::find()->where(['aga_aug_id'=>$group->aug_id,'aga_asm_id'=>$menu->asm_id])->one();
						$parrentChecked = '';
						$parrentStatus = 0;
						if(!empty($parrentAdminGroupAccess)){
							if($parrentAdminGroupAccess->aga_access ==1){
								$parrentChecked = 'checked';
								$parrentStatus 	= 1;
							}
							
						}
					?>
                        <td><input type="checkbox" <?=$parrentChecked;?> class="change_menu_access" data-aug_id="<?=$group->aug_id;?>" data-asm_id="<?=$menu->asm_id;?>" value="<?=$parrentStatus;?>" ></td>
                    <?php } ?>
                    
                  </tr>
                  <?php 
				  foreach($childs as $child){
				  ?>
                  <tr>
                    <td><?=$no1;?></td>
                    <td>&nbsp;</td>
                    <td><?=$child->asm_name;?></td>
                    <td><?=$child->asm_url;?></td>
                    <?php foreach ($groups as $group){ 
						$childAdminGroupAccess = AdminGroupMenuAccess::find()->where(['aga_aug_id'=>$group->aug_id,'aga_asm_id'=>$child->asm_id])->one();
						$childChecked 	= '';
						$childStatus 	= 0;
						if(!empty($childAdminGroupAccess)){
							if($childAdminGroupAccess->aga_access ==1){
								$childChecked = 'checked';
								$childStatus  = 1;
							}
						}
					?>
                        <td><input type="checkbox" <?=$childChecked;?> class="change_menu_access" data-aug_id="<?=$group->aug_id;?>" data-asm_id="<?=$child->asm_id;?>" value="<?=$childStatus;?>"></td>
                    <?php } ?>
                    
                  </tr>
                  <?php
				  	}
				  }
				  
				  ?>
                  
                </table>
            </div>
        </div>
    </div>
</div>
<?php ActiveForm::end(); ?>
<?php $this->registerJsFile($this->theme->baseUrl . '/js/custom/modules/userpermission/common.js', ['depends' => app\themes\gentayu\assets\AppAsset::className()]); ?>