<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

$this->title = ($model->isNewRecord ? Yii::t('app', 'Create New Menu') : Yii::t('app', 'Update Menu'));
?>
<!-- BEGIN PAGE BAR -->
<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <a href="<?php echo Yii::$app->homeUrl ?>">Home</a>
            <i class="fa fa-circle"></i>
        </li>
        <li>
            <span><?=$this->title;?></span>
        </li>
    </ul>
</div>
<!-- END PAGE BAR -->
<br clear="all" />
<div class="portlet light bordered">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-group"></i>
            <span class="caption-subject font-dark sbold uppercase"><?=Yii::t('app', $this->title)?></span>
        </div>
    </div>
    <div class="portlet-body form">
        <?php
		$form = ActiveForm::begin([
			'id' => 'user-form',
			'options' => ['class' => 'form-horizontal'],
			'fieldConfig' => [
				'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div>",
				'labelOptions' => ['class' => 'col-lg-3 control-label'],
			],
		]);
		?>
        	<?= $form->field($model, 'asm_parent_id')->widget(Select2::classname(), [
									'data' => ArrayHelper::map($parent, 'asm_id', 'asm_name'),
									'options' => ['placeholder' => 'Select a state ...'],
									]);?>
        	<?= $form->field($model, 'asm_name')->textInput() ?>
            <?= $form->field($model, 'asm_icon')->textInput() ?>
            <?= $form->field($model, 'asm_url')->textInput() ?>
            <?= $form->field($model, 'asm_sort')->textInput() ?>
            <div class="form-group">
				<?= Html::activeLabel($model, 'asm_have_child', ['class' => 'col-lg-3 control-label']) ?>
                <div class="col-lg-6">
                    <div class="checkbox block">
                        <?= Html::activeCheckbox($model, 'asm_have_child', ['label'=>false]); ?>
                    </div>
                </div>
            </div>
            <div class="form-group">
				<?= Html::activeLabel($model, 'asm_status', ['class' => 'col-lg-3 control-label']) ?>
                <div class="col-lg-6">
                    <div class="checkbox block">
                        <?= Html::activeCheckbox($model, 'asm_status', ['label'=>false]); ?>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <button type="submit" class="pull-right green btn"><i class="fa fa-save"></i> Save</button>
                <button type="reset" class="pull-left btn" onclick="window.location='<?= Yii::$app->urlManager->createUrl('usergroup') ?>'"><i class="fa fa-times"></i> Cancel</button>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
            
            
        

