<?php
namespace backend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
//use backend\controllers\BaseController;
use common\models\UserPermission;
use common\models\AdminUserGroup;
use common\models\AdminUserMenu;
use common\models\AdminGroupMenuAccess;

class UserpermissionController extends Controller {

    public function actionIndex() {
        $actions = $this->combineController();
        $groups = AdminUserGroup::find()->all();

        if (isset($_POST['action'])) {
            foreach ($_POST['action'] as $key => $val) {
                $data = explode('-', $key);
                $this->updateAvailablity($data[0], $data[1], $val);
                $this->refresh();
            }
        }

        return $this->render('index', [
                    'actions' => $actions,
                    'groups' => $groups
        ]);
    }

    public function actionMenuAccess() {

        $actions = $this->combineController();
        $groups = AdminUserGroup::find()->all();
        $menus = AdminUserMenu::find()->where(['asm_parent_id'=>0])->orderBy(['asm_sort' => SORT_ASC])->all();
        
        if (isset($_POST['action'])) {
            foreach ($_POST['action'] as $key => $val) {
                $data = explode('-', $key);
                $this->updateAvailablity($data[0], $data[1], $val);
                $this->refresh();
            }
        }

        return $this->render('menu-access', [
                    'title' => 'Menu Permision',
                    'pageDesc' => '',
                    'menus' => $menus,
                    'groups' => $groups
        ]);
    }

    public function actionMenuList() {
        $menus = AdminUserMenu::find()->where(['asm_parent_id'=>0])->orderBy(['asm_sort' => SORT_ASC])->all();
        $id = 0;
        if(!empty($_GET['id'])){
            $id = $_GET['id'];
        }
        if (isset($_POST['action'])) {
            foreach ($_POST['action'] as $key => $val) {
                $data = explode('-', $key);
                $this->updateAvailablity($data[0], $data[1], $val);
                $this->refresh();
            }
        }

        return $this->render('menu-list', [
                    'title'     => 'Menu List',
                    'pageDesc' => '',
                    'menus' => $menus,
                    'id'    => $id,
        ]);
    }

    public function actionMenuDelete($id) {
        $adminUserMenu = AdminUserMenu::findOne($id);
        $transaction        = Yii::$app->db->beginTransaction();
        try {
            if(!empty($adminUserMenu)){
                $adminUserMenu = AdminUserMenu::findOne($id);
                AdminGroupMenuAccess::deleteAll('aga_asm_id ='.$adminUserMenu->asm_id);
                if($adminUserMenu->delete()){
                    $transaction->commit();
                }
            }
        }catch (Exception $e) {
                //echo '11';
                $transaction->rollback();
                throw $e;
            }

        return $this->redirect(['menu-list']);
    }

    public function actionMenuCreate() {
        $model  = New AdminUserMenu;
        $parent = AdminUserMenu::find()->where(['asm_parent_id'=>0])->orderBy(['asm_sort' => SORT_ASC])->all();
        if (isset($_POST) && $model->load($_POST)) {
            if(empty($model->asm_parent_id)){
                $model->asm_parent_id = 0;
            }
            $model->save();
            return $this->redirect(['/userpermission/menu-list']);
        }

        return $this->render('form', [
                    'model' => $model,
                    'parent' => $parent
        ]);
    }

    public function actionMenuUpdate() {
        $id = $_GET['id'];
        $model  = AdminUserMenu::findOne($id);
        $parent = AdminUserMenu::find()->where(['asm_parent_id'=>0])->orderBy(['asm_sort' => SORT_ASC])->all();
        if (isset($_POST) && $model->load($_POST)) {
            if(empty($model->asm_parent_id)){
                $model->asm_parent_id = 0;
            }
            $model->save();
            return $this->redirect(['/userpermission/menu-list']);
        }

        return $this->render('form', [
                    'model' => $model,
                    'parent' => $parent
        ]);
    }
    public function actionMenuChangeAccess() {
        $post   = Yii::$app->request->post();
        $model = AdminGroupMenuAccess::find()->where(['aga_aug_id'=>$post['aug_id'],'aga_asm_id'=>$post['asm_id']])->one();
        if(!empty($model)){
            $status = 0;
            if ($post['status']==0) {
                $status = 1;
            }
            $model->aga_access = $status;
        }else{
            $model = New AdminGroupMenuAccess;
            $model->aga_aug_id  = $post['aug_id'];
            $model->aga_asm_id  = $post['asm_id'];
            $model->aga_access  = 1; 
        }
        $model->save();
    }    
    public function actionList() {
        $data = $this->combineController();
        if (isset($_POST['action'])) {

            $groups = AdminUserGroup::find()->all();
            foreach ($groups as $g) {
                foreach ($_POST['action'] as $row) {
                    $userPermission = UserPermission::find()->where(['action' => $row, 'group_id' => $g->aug_id]);
                    if ($userPermission->count() == 0) {
                        $new = new UserPermission;
                        $new->group_id = $g->aug_id;
                        $new->action = $row;
                        $new->available = 1;
                        $new->save();
                    }
                }
            }

            return $this->redirect('index');
        }
        return $this->render('list', ['data' => $data]);
    }

    protected function combineController() {
        $data = [];
        foreach ($this->findController() as $key => $val) {
            foreach ($val as $row) {
                $data[] = $key . '/' . $row;
            }
        }
        return $data;
    }

    protected function findController() {
        $path = Yii::getAlias('@backend-controller');
        $dh = opendir($path);
        $controllers = [];
        while (false !== ($filename = readdir($dh))) {
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if ($ext == 'php') {

                $className = str_replace('.php', '', $filename);
                $actions = $this->findAction($className);


                $filename = strtolower($filename);
                $filename = str_replace('controller.php', '', $filename);
                $controllers[$filename] = $actions;
            }
        }
        return $controllers;
    }

    protected function findModul() {
        $path = Yii::getAlias('@backend-modules');
        $dh = opendir($path);
        $controllers = [];
        while (false !== ($filename = readdir($dh))) {
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            if ($ext == 'php') {

                $className = str_replace('.php', '', $filename);
                $actions = $this->findAction($className);


                $filename = strtolower($filename);
                $filename = str_replace('controller.php', '', $filename);
                $controllers[$filename] = $actions;
            }
        }
        return $controllers;
    }

    protected function findAction($className) {
        $nameSpace = "backend\controllers\\" . $className;
        $class = new $nameSpace(1, 'backend');
        $class_methods = get_class_methods($class);
        $actions = [];
        foreach ($class_methods as $method_name) {
            if (strpos($method_name, 'action') !== false) {
                if ($method_name !== 'actions') {
                    $method_name = strtolower($method_name);
                    $method_name = str_replace('action', '', $method_name);
                    $actions[] = $method_name;
                }
            }
        }
        return $actions;
    }

    protected function updateAvailablity($action, $groupId, $availablelity) {
        UserPermission::updateAll([
            'available' => $availablelity
        ],'action=:action and group_id=:group_id',[':group_id'=>$groupId,':action'=>$action]);
    }

}
