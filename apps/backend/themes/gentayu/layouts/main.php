<?php

use common\components\helpers\Identity;
use yii\helpers\Html;
use app\themes\gentayu\assets\AppAsset;
//use common\models\Orders;

AppAsset::register($this);
$this->beginPage(); 
$this->title = 'Gentayu Security Dasboard';
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?= Html::encode($this->title) ?></title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head(); ?>
    <script src="<?=$this->theme->baseUrl;?>/lib/jquery/dist/jquery.js"></script>
    <link rel="shortcut icon" href="<?=$this->theme->baseUrl;?>/img/favicon/favicon.ico" />
  </head>
  <body class="nav-md">
    <?php $this->beginBody(); ?>
    <div class="container body">
      <div class="main_container">
        <?php echo $this->render('menu',['title'=>$this->title]); ?>

        <!-- top navigation -->
        <?php echo $this->render('header/main'); ?>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <?php echo $content; ?>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php echo $this->render('footer/main'); ?>
        <!-- /footer content -->
      </div>
    </div>
    <?php $this->endBody(); ?>
  </body>
</html>
<?php $this->endPage(); ?>