<?php

use common\components\helpers\Identity;
use yii\helpers\Html;
use app\themes\gentayu\assets\AppAsset;
//use common\models\Orders;

AppAsset::register($this);
$this->beginPage(); 
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Gentayu Security Dasboard</title>
    <?= Html::csrfMetaTags() ?>
    <?php $this->head(); ?>
    <link rel="shortcut icon" href="<?=$this->theme->baseUrl;?>/img/favicon/favicon.ico" />
  </head>
<body class="login">
    <?php $this->beginBody(); ?>
        <?php echo $content; ?>
    <?php $this->endBody(); ?>
</body>
</html>
<?php $this->endPage(); ?>