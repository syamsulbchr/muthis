<?php
use common\models\AdminUserMenu;
use common\models\AdminGroupMenuAccess;
Use yii\helpers\BaseUrl;
Use yii\helpers\Url;
use yii\helpers\Html;

$sql = 'select * from tbl_admin_user_menu asm 
    join tbl_admin_group_menu_access aga ON aga.`aga_asm_id` = asm.`asm_id` 
      AND aga.`aga_aug_id`='.Yii::$app->user->identity->adu_aug_id.' 
      AND aga_access =1
      AND asm_status =1 
    where asm_parent_id=0 Order By asm_sort ';
//echo $sql;
$query = Yii::$app->db->createCommand($sql)->queryAll();
$currentUrl = Yii::$app->request->getUrl();
?>
<div class="col-md-3 left_col">
  <div class="left_col scroll-view">
    <div class="navbar nav_title" style="border: 0;">
      <a href="<?=Url::home();;?>" class="site_title"><img src="<?=$this->theme->baseUrl;?>/img/gentayu-logo.png" alt="<?=$title;?>" class="header-logo"> <span class="header-name"><?=$title;?></span></a>
    </div>

    <div class="clearfix"></div>
    <?php
    /*
    <!-- menu profile quick info -->
    <div class="profile clearfix">
      <div class="profile_pic">
        <img src="<?=$this->theme->baseUrl;?>/img/users/vina.jpg" alt="..." class="img-circle profile_img">
      </div>
      <div class="profile_info">
        <span>Welcome,</span>
        <h2>John Doe</h2>
      </div>
    </div>
    <!-- /menu profile quick info -->

    <br />
    */
    ?>
    <!-- sidebar menu -->
    <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
      <div class="menu_section">
        <ul class="nav side-menu">
          <?php 
            foreach($query as $masterMenu){
            $master = AdminUserMenu::findOne($masterMenu['asm_id']);
          ?>
          <?php 
            if($master->asm_have_child==0){
              $parrentNavItem = '';
              $parrentNavLink = '';
              //echo 'Url = '.Yii::$app->request->getUrl() .' == '.'/'.$master->asm_url;
              if(Yii::$app->request->getUrl() == '/'.$master->asm_url){
                $parrentNavItem = 'active open';
                $parrentNavLink = 'nav-toggle';
              }
            ?>
              <li><a><i class="fa <?=$master->asm_icon;?>"></i> <?=$master->asm_name;?></a>
            <?php
            }else{
              $childMenuOpen = AdminUserMenu::find()->where('asm_status =1 AND asm_parent_id=:parentId AND asm_url=:currentUrl',[':parentId'=>$master->asm_id,':currentUrl'=>substr(Yii::$app->request->getUrl(),1,1000)])->one();
              $childNavItemOpen   = '';
              $arrowOpen      = '';
              //echo 'Url = '.Yii::$app->request->getUrl() .' == '.'/'.$child->asm_url;
              if(!empty($childMenuOpen)){
                $childNavItemOpen = 'active open';
                $arrowOpen = 'open';
              }
            ?>
              <li><a><i class="fa <?=$master->asm_icon;?>"></i> <?=$master->asm_name;?> <span class="fa fa-chevron-down"></span></a>
              <ul class="nav child_menu">
                <?php
                  //echo $master->adu_aug_id;
                  $childMenu = AdminUserMenu::find()->where('asm_status =1 AND asm_parent_id='.$master->asm_id)->orderBy(['asm_sort' => SORT_ASC])->all();
                  ?>
                  <?php 
                  foreach($childMenu as $child){
                    //echo $child->asm_id;
                    $adminGroupMenuAccess = AdminGroupMenuAccess::find()->where('aga_asm_id='.$child->asm_id.' AND aga_access=1 AND aga_aug_id='.Yii::$app->user->identity->adu_aug_id)->one();
                    if(!empty($adminGroupMenuAccess)){
                      $childNavItem = '';
                      $childNavLink = '';
                      //echo 'Url = '.Yii::$app->request->getUrl() .' == '.'/'.$child->asm_url;
                      if(Yii::$app->request->getUrl() == '/'.$child->asm_url){
                        $childNavItem = 'active open';
                        $childNavLink = 'nav-toggle';
                      }
                    ?>
                    <li><a href="<?=Yii::$app->urlManager->createUrl($child->asm_url);?>"><?=$child->asm_name;?></a></li>
                  <?php
                    }
                  }
                  ?>
                  </a>
              </ul>
            <?php 
              }
            ?>
          </li>

        <?php } ?>
        </ul>
      </div>
    </div>
    <!-- /sidebar menu -->
    <?php 
    /*
    <!-- /menu footer buttons -->
    <div class="sidebar-footer hidden-small">
      <a data-toggle="tooltip" data-placement="top" title="Settings">
        <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="FullScreen">
        <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Lock">
        <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
      </a>
      <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
        <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
      </a>
    </div>
    <!-- /menu footer buttons -->
    */
    ?>
  </div>
</div>