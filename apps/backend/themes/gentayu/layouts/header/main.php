<?php
use yii\helpers\Html;
?>
<div class="top_nav">
<div class="nav_menu">
    <div class="nav toggle">
      <a id="menu_toggle"><i class="fa fa-bars"></i></a>
    </div>
    <nav class="nav navbar-nav">
    <ul class=" navbar-right">
      <li class="nav-item dropdown open" style="padding-left: 15px;">
        <a href="javascript:;" class="user-profile dropdown-toggle" aria-haspopup="true" id="navbarDropdown" data-toggle="dropdown" aria-expanded="false">
          <img src="<?=$this->theme->baseUrl;?>/img/users/vina.jpg" alt="">Vina
        </a>
        <div class="dropdown-menu dropdown-usermenu pull-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item"  href="javascript:;"> Profile</a>
            <a class="dropdown-item"  href="javascript:;">
              <span class="badge bg-red pull-right">50%</span>
              <span>Settings</span>
            </a>
          <a class="dropdown-item"  href="/site/index">Help</a>
          <?php
            //Html::beginForm(['/site/logout'], 'post');
            //echo Html::submitButton(
            //    'Log Out <i class="fa fa-sign-out pull-right"></i>',
                //['class' => 'btn btn-default logout','style'=>'width:100%; text-align:left;font-size:12px; padding-left:18px;']
            //);
            //Html::endForm();
          ?>
          <?php
        $menuItems =
            Html::beginForm(['/site/logout'], 'post')
            . Html::submitButton(
                'Logout <i class="fa fa-sign-out pull-right"></i>',
                ['class' => 'btn btn-default logout','style'=>'width:100%; text-align:left;font-size:12px; padding-left:18px;']
            )
            . Html::endForm();
          echo $menuItems;
        ?>
        </div>
      </li>
      
      <?php echo $this->render('../notification/general'); ?>

      <li style="margin-right: 20px;">
        <div class="filter">
            <div id="reportrange" class="pull-right" style="background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
              <i class="glyphicon glyphicon-calendar fa fa-calendar"></i>
              <span>December 30, 2014 - January 28, 2015</span> <b class="caret"></b>
            </div>
          </div>
      </li>
    </ul>
  </nav>
</div>
</div>