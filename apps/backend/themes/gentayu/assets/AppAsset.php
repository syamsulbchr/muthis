<?php
namespace app\themes\gentayu\assets;

use yii\web\AssetBundle;

class AppAsset extends AssetBundle {

    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
      'themes/gentayu/lib/bootstrap/dist/css/bootstrap.min.css',
      'themes/gentayu/lib/font-awesome/css/font-awesome.min.css',
      'themes/gentayu/lib/nprogress/nprogress.css', //main
      'themes/gentayu/lib/bootstrap-daterangepicker/daterangepicker.css',
      'themes/gentayu/lib/animate.css/animate.min.css',
      'themes/gentayu/css/custom.css',
      'themes/gentayu/css/common.css',
    ];
    public $js = [
		  'themes/gentayu/lib/jquery/dist/jquery.min.js',
      'themes/gentayu/lib/bootstrap/dist/js/bootstrap.bundle.min.js',
      'themes/gentayu/lib/fastclick/lib/fastclick.js',
      'themes/gentayu/lib/nprogress/nprogress.js',
      'themes/gentayu/lib/Chart.js/dist/Chart.min.js',
      'themes/gentayu/lib/jquery-sparkline/dist/jquery.sparkline.min.js',
      'themes/gentayu/lib/Flot/jquery.flot.js',
      'themes/gentayu/lib/Flot/jquery.flot.pie.js',
      'themes/gentayu/lib/Flot/jquery.flot.time.js',
      'themes/gentayu/lib/Flot/jquery.flot.stack.js',
      'themes/gentayu/lib/Flot/jquery.flot.resize.js',
      'themes/gentayu/lib/flot.orderbars/js/jquery.flot.orderBars.js',
      'themes/gentayu/lib/flot-spline/js/jquery.flot.spline.min.js',
      'themes/gentayu/lib/flot.curvedlines/curvedLines.js',
      'themes/gentayu/lib/DateJS/build/date.js',
      'themes/gentayu/lib/moment/min/moment.min.js',
      'themes/gentayu/lib/bootstrap-daterangepicker/daterangepicker.js',
      'themes/gentayu/js/custom.min.js',
      'themes/gentayu/js/common.js'
    ];
    public $depends = [
        //'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];

}
