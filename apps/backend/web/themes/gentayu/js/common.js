$('.checked_change').click(function(e) {
    var _this   = $(this);
    var status  = _this.val();
    var id  = _this.data('id');
    var url = _this.data('url'); 
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    console.log(_this);
    $('#message').css("display","block");
    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {id:id, status:status, _csrf_backend : csrfToken},
        success: function(json){
            console.log(json);
            $('#message').css("display","none");
            $('#message').html(json.msg); 
        }
    });
    
});
$('.menu_permision_change').click(function(e) {
    var _this   = $(this);
    var status  = _this.val();
    var id      = _this.data('id');
    var augid   = _this.data('augid');
    var url = _this.data('url'); 
    var csrfToken = $('meta[name="csrf-token"]').attr("content");
    console.log(_this);
    $('#message').css("display","block");
    $.ajax({
        type: 'POST',
        url: url,
        dataType: 'json',
        data: {id:id, augid:augid, status:status, _csrf_backend : csrfToken},
        success: function(json){
            console.log(json);
            $('#message').css("display","none");
            $('#message').html(json.msg); 
        }
    });
    
});

