<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

$this->title = ($model->isNewRecord ? Yii::t('app', 'Create '.$title) : Yii::t('app', 'Update '.$title));
?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?=$this->title;?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Form <small><?=$this->title;?></small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a class="dropdown-item" href="#">Settings 1</a>
                  </li>
                  <li><a class="dropdown-item" href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>
            <?php
            $form = ActiveForm::begin([
                'id' => 'user-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div><br clear=\"all\"/>",
                    'labelOptions' => ['class' => 'col-form-label col-md-3 col-sm-3 label-align'],
                ],
            ]);
            ?>
            <?= $form->field($model, 'prj_name')->textInput() ?>
            <?= $form->field($model, 'prj_code')->textInput() ?>
            <?= $form->field($model, 'prj_desc')->textArea() ?>
            <?=$form->field($model, 'prj_pm_adu_id')->dropDownList(
                ArrayHelper::map($users, 'adu_id', 'adu_screen_name'), 
                ['prompt'=>'Please Select Project Manager']);
            ?>
            <?=$form->field($model, 'prj_tc_adu_id')->dropDownList(
                ArrayHelper::map($users, 'adu_id', 'adu_screen_name'), 
                ['prompt'=>'Please Select Technical Contact']);
            ?>
            <?=$form->field($model, 'prj_tm_adu_id')->dropDownList(
                ArrayHelper::map($users, 'adu_id', 'adu_screen_name'), 
                ['prompt'=>'Please Select Team Manager']);
            ?>
            <?=$form->field($model, 'prj_pjt_id')->dropDownList(
                ArrayHelper::map($projectType, 'pjt_id', 'pjt_name'), 
                ['prompt'=>'Please Select Project Type']);
            ?>
            <?=$form->field($model, 'prj_svt_id')->dropDownList(
                ArrayHelper::map($severity, 'svt_id', 'svt_name'), 
                ['prompt'=>'Please Select Business Severity']);
            ?>
            <?=$form->field($model, 'prj_plf_id')->dropDownList(
                ArrayHelper::map($platform, 'plf_id', 'plf_name'), 
                ['prompt'=>'Please Select Platform']);
            ?>
            <?=$form->field($model, 'prj_lfc_id')->dropDownList(
                ArrayHelper::map($lifecycle, 'lfc_id', 'lfc_name'), 
                ['prompt'=>'Please Select Lifecycle']);
            ?>
            <?=$form->field($model, 'prj_dvp_id')->dropDownList(
                ArrayHelper::map($developed, 'dvp_id', 'dvp_name'), 
                ['prompt'=>'Please Select Vendor']);
            ?>
            <div class="form-actions">
                <button type="submit" class="btn btn-round btn-success pull-right"><i class="fa fa-save"></i> Save</button>
                <button type="reset" class="btn btn-round btn-secondary pull-left" onclick="window.location='<?= Yii::$app->urlManager->createUrl('/projects/project') ?>'"><i class="fa fa-times"></i> Cancel</button>
            </div>
            <br clear="all"/>
        <?php ActiveForm::end(); ?>
          </div>
        </div>
      </div>
    </div>
</div>