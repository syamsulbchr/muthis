<?php
use yii\helpers\Html;
use common\models\AdminUserMenu;
?>
<div class="page-title">
  <div class="title_left">
    <h3>PROJECT <small><?=$title;?></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5   form-group pull-right top_search">
      <div class="input-group">
        <a style="cursor:pointer" class="btn btn-round btn-success" href="<?= Yii::$app->urlManager->createUrl('/projects/repository/create'); ?>"><i class="fa fa-plus-square"></i> <?=Yii::t('app', 'New Menu');;?></a>
      </div>
    </div>
  </div>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
        <td>No</td>
        <td>Name</td>
        <td>Code</td>
        <td>Description</td>
        <td>Url</td>
        <td>Last Update</td>
        <td>Status</td>
        <td colspan="2">Action</td>
    </tr>
  </thead>
  <tbody>
    <?php
    $no1 = 0;
    foreach($model as $data){
      $no1 = $no1+1;
      $parentChecked = '';
      $parentStatus  = 0;
      if($data->rpy_status == 1){
        $parentChecked = 'checked';
        $parentStatus  = 1;
      }
      ?>
      <tr>
        <td><?=$no1;?></td>
        <td><?=$data->project->prj_name;?></td>
        <td><?=$data->rpy_name;?></td>
        <td><?=$data->rpy_code;?></td>
        <td><?=$data->rpy_desc;?></td>
        <td><?=$data->rpy_url;?></td>
        <td><?=$data->rpy_datetime;?></td>
        <td><input type="checkbox" <?=$parentChecked;?> class="checked_change" data-url="<?= Yii::$app->urlManager->createUrl('/projects/repository/change-status'); ?>" data-id="<?=$data->rpy_id;?>" value="<?=$parentStatus;?>"></td>
        <td><?=Html::a('<i class="fa fa-pencil"></i>',['/projects/repository/update?id=' . $data->rpy_id],['class' => 'btn green btn-outline btn-sm']);?></td>
        <td><?=Html::a('<i class="fa fa-trash-o"></i>',['/projects/repository/delete?id=' . $data->rpy_id],['class' => 'btn red btn-outline btn-sm']);;?></td>
      </tr>
    <?php
    }
    ?>
  </tbody>
</table>

