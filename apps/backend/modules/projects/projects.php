<?php

namespace backend\modules\projects;

class Projects extends \yii\base\Module
{
    public $controllerNamespace = 'backend\modules\projects\controllers';

    public function init()
    {
        parent::init();

        // custom initialization code goes here
    }
}
