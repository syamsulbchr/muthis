<?php
use yii\helpers\Html;
use common\models\AdminUserMenu;
?>
<div class="page-title">
  <div class="title_left">
    <h3>RBAC <small>Menu Access List</small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5   form-group pull-right top_search">
      <div class="input-group">
        <a style="cursor:pointer" class="btn btn-round btn-success" href="<?= Yii::$app->urlManager->createUrl('/rbac/default/create'); ?>"><i class="fa fa-plus-square"></i> <?=Yii::t('app', 'New Menu');;?></a>
      </div>
    </div>
  </div>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
        <td>No</td>
        <td>Parent</td>
        <td>child</td>
        <td>Url</td>
        <td>icon</td>
        <td>Sort</td>
        <td>Status</td>
        <td colspan="2">Action</td>
    </tr>
  </thead>
  <tbody>
    <?php
    $no1 = 0;
    foreach($menus as $menu){
      $no1 = $no1+1;
      $parentChecked = '';
      $parentStatus  = 0;
      if($menu->asm_status == 1){
        $parentChecked = 'checked';
        $parentStatus  = 1;
      }
      $childs = AdminUserMenu::find()->where(['asm_parent_id'=>$menu->asm_id])->orderBy(['asm_sort' => SORT_ASC])->all();
      
      ?>
      <tr>
        <td><?=$no1;?></td>
        <td><?=$menu->asm_name;?></td>
        <td>&nbsp;</td>
        <td><?=$menu->asm_url;?></td>
        <td><?=$menu->asm_icon;?></td>
        <td><?=$menu->asm_sort;?></td>
        <td><input type="checkbox" <?=$parentChecked;?> class="checked_change" data-url="<?= Yii::$app->urlManager->createUrl('/rbac/default/change-status'); ?>" data-id="<?=$menu->asm_id;?>" value="<?=$parentStatus;?>"></td>
        <td><?=Html::a('<i class="fa fa-pencil"></i> Update',['/rbac/default/update?id=' . $menu->asm_id],['class' => 'btn green btn-outline btn-sm']);?></td>
        <td><?=Html::a('<i class="fa fa-trash-o"></i> Delete',['/rbac/default/delete?id=' . $menu->asm_id],['class' => 'btn red btn-outline btn-sm']);;?></td>
      </tr>
    <?php 
    foreach($childs as $child){
      $childChecked = '';
      $childStatus  = 0;
      if($child->asm_status == 1){
        $childChecked = 'checked';
        $childStatus  = 1;
      }
      ?>
      <tr>
        <td><?=$no1;?></td>
        <td>&nbsp;</td>
        <td><?=$child->asm_name;?></td>
        <td><?=$child->asm_url;?></td>
        <td><?=$child->asm_icon;?></td>
        <td><?=$child->asm_sort;?></td>
        <td><input type="checkbox" <?=$childChecked;?> class="checked_change" data-url="<?= Yii::$app->urlManager->createUrl('/rbac/default/change-status'); ?>" data-id="<?=$child->asm_id;?>" value="<?=$childStatus;?>"></td>
        <td><?=Html::a('<i class="fa fa-pencil"></i> Update',['/rbac/default/update?id=' . $child->asm_id],['class' => 'btn green btn-outline btn-sm']);?></td>
        <td><?=Html::a('<i class="fa fa-trash-o"></i> Delete',['/rbac/default/delete?id=' . $child->asm_id],['class' => 'btn red btn-outline btn-sm']);;?></td>
      </tr>
      <?php
      }
    }
    ?>
  </tbody>
</table>

