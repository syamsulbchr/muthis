<?php
use yii\helpers\Html;
use common\models\AdminUserMenu;
?>
<div class="page-title">
  <div class="title_left">
    <h3>RBAC <small><?=$title;?></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5   form-group pull-right top_search">
      <div class="input-group">
        <a style="cursor:pointer" class="btn btn-round btn-success" href="<?= Yii::$app->urlManager->createUrl('/rbac/default/create'); ?>"><i class="fa fa-plus-square"></i> <?=Yii::t('app', 'New Menu');;?></a>
      </div>
    </div>
  </div>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
        <td>No</td>
        <td>Name</td>
        <td>Email</td>
        <td>Group</td>
        <td>Department</td>
        <td>Level</td>
        <td>Status</td>
        <td colspan="2" style="text-align: center;">Action</td>
    </tr>
  </thead>
  <tbody>
    <?php
    $no1 = 0;
    foreach($datas as $data){
      $no1 = $no1+1;
      $parentChecked = '';
      $parentStatus  = 0;
      if($data->adu_status == 1){
        $parentChecked = 'checked';
        $parentStatus  = 1;
      }
      ?>
      <tr>
        <td><?=$no1;?></td>
        <td><?=$data->adu_screen_name;?></td>
        <td><?=$data->adu_email;?></td>
        <td><?=$data->userGroup->aug_name;?></td>
        <td><?=$data->department->dpt_name;?></td>
        <td><?=$data->level->lvl_name;?></td>
        <td><input type="checkbox" <?=$parentChecked;?> class="checked_change" data-url="<?= Yii::$app->urlManager->createUrl('/rbac/users/change-status'); ?>" data-id="<?=$data->adu_id;?>" value="<?=$parentStatus;?>"></td>
        <td><?=Html::a('<i class="fa fa-pencil"></i>',['/rbac/users/update?id=' . $data->adu_id],['class' => 'btn green btn-outline btn-sm', 'alt'=>'Update']);?></td>
        <td><?=Html::a('<i class="fa fa-trash-o"></i>',['/rbac/users/delete?id=' . $data->adu_id],['class' => 'btn red btn-outline btn-sm', 'alt'=>'Delete']);;?></td>
      </tr>
      <?php
    }
    ?>
  </tbody>
</table>

