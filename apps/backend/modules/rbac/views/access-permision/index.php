<?php
use yii\helpers\Html;
use common\models\AdminUserMenu;
use common\models\AdminGroupMenuAccess;
?>
<div class="page-title">
  <div class="title_left">
    <h3>RBAC <small><?=$title;?></small></h3>
  </div>

  <div class="title_right">
    <div class="col-md-5 col-sm-5   form-group pull-right top_search">
      <div class="input-group">
        <a style="cursor:pointer" class="btn btn-round btn-success" href="<?= Yii::$app->urlManager->createUrl('/rbac/group/create'); ?>"><i class="fa fa-plus-square"></i> <?=Yii::t('app', 'New Menu');;?></a>
      </div>
    </div>
  </div>
</div>
<table class="table table-bordered">
  <thead>
    <tr>
        <td>No</td>
        <td>Parent</td>
        <td>Child</td>
        <td>Url</td>
        <?php foreach ($groups as $group): ?>
            <td><?= $group->aug_name ?></td>
        <?php endforeach; ?>
    </tr>
  </thead>
  <?php
          
  $no1 = 0;
          foreach($menus as $menu){
    $no1 = $no1+1;
    $childs = AdminUserMenu::find()->where(['asm_parent_id'=>$menu->asm_id])->orderBy(['asm_sort' => SORT_ASC])->all();
  ?>
          <tr>
            <td><?=$no1;?></td>
            <td><?=$menu->asm_name;?></td>
            <td>&nbsp;</td>
            <td><?=$menu->asm_url;?></td>
            <?php foreach ($groups as $group){ 
              $parrentAdminGroupAccess = AdminGroupMenuAccess::find()->where(['aga_aug_id'=>$group->aug_id,'aga_asm_id'=>$menu->asm_id])->one();
              $parrentChecked = '';
              $parrentStatus = 0;
              if(!empty($parrentAdminGroupAccess)){
                if($parrentAdminGroupAccess->aga_access ==1){
                  $parrentChecked = 'checked';
                  $parrentStatus  = 1;
                }
                
              }
            ?>
            <td><input type="checkbox" <?=$parrentChecked;?> class="menu_permision_change" data-url="<?= Yii::$app->urlManager->createUrl('/rbac/access-permision/change-status'); ?>" data-id="<?=$menu->asm_id;?>" data-augid="<?=$group->aug_id;?>" value="<?=$parrentStatus;?>" ></td>
            <?php } ?>
            
          </tr>
          <?php 
          foreach($childs as $child){
          ?>
          <tr>
            <td><?=$no1;?></td>
            <td>&nbsp;</td>
            <td><?=$child->asm_name;?></td>
            <td><?=$child->asm_url;?></td>
            <?php foreach ($groups as $group){ 
              $childAdminGroupAccess = AdminGroupMenuAccess::find()->where(['aga_aug_id'=>$group->aug_id,'aga_asm_id'=>$child->asm_id])->one();
              $childChecked   = '';
              $childStatus  = 0;
              if(!empty($childAdminGroupAccess)){
                if($childAdminGroupAccess->aga_access ==1){
                  $childChecked = 'checked';
                  $childStatus  = 1;
                }
              }
            ?>
            <td><input type="checkbox" <?=$childChecked;?> class="menu_permision_change" data-url="<?= Yii::$app->urlManager->createUrl('/rbac/access-permision/change-status'); ?>" data-id="<?=$child->asm_id;?>" data-augid="<?=$group->aug_id;?>" value="<?=$childStatus;?>"></td>
            <?php } ?>
            
          </tr>
          <?php
    }
  }
  
  ?>
</table>

