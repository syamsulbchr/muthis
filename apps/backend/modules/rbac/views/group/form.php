<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

$this->title = $title;
?>
<div class="">
    <div class="page-title">
        <div class="title_left">
            <h3><?=$this->title;?></h3>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-sm-12 ">
        <div class="x_panel">
          <div class="x_title">
            <h2>Form<small><?=$title;?></small></h2>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br>
            <?php
            $form = ActiveForm::begin([
                'id' => 'user-form',
                'options' => ['class' => 'form-horizontal'],
                'fieldConfig' => [
                    'template' => "{label}\n<div class=\"col-lg-6\">{input}\n<div>{error}</div></div><br clear=\"all\"/>",
                    'labelOptions' => ['class' => 'col-form-label col-md-3 col-sm-3 label-align'],
                ],
            ]);
            ?>
            <?php 
            /*
            echo $form->field($model, 'asm_parent_id')->widget(Select2::classname(), [
                                    'data' => ArrayHelper::map($parent, 'asm_id', 'asm_name'),
                                    'options' => ['placeholder' => 'Select a state ...'],
                                    ]);
            
            echo $form->field($model, 'asm_parent_id')->widget(Select2::classname(), [
                'data' => [],
                'language' => 'de',
                'options' => ['placeholder' => 'Select a state ...'],
                'pluginOptions' => [
                    'allowClear' => true
                ],
            ]);
            
            echo $form->field($model, 'asm_parent_id')->dropDownList(
                ArrayHelper::map($parent, 'asm_id', 'asm_name'), 
                ['prompt'=>'Please Select Parent']);
            */
            ?>
            <?= $form->field($model, 'aug_name')->textInput() ?>
            <?= $form->field($model, 'aug_code')->textInput() ?>
            <?= $form->field($model, 'aug_desck')->textInput() ?>
            <div class="form-group">
                <?= Html::activeLabel($model, 'aug_status', ['class' => 'col-form-label col-md-3 col-sm-3 label-align']) ?>
                <div class="col-lg-6">
                    <div class="checkbox block">
                        <?= Html::activeCheckbox($model, 'aug_status', ['label'=>false]); ?>
                    </div>
                </div>
            </div>
            <br clear="all"/>
            <div class="form-actions">
                <button type="submit" class="btn btn-round btn-success pull-right"><i class="fa fa-save"></i> Save</button>
                <button type="reset" class="btn btn-round btn-secondary pull-left" onclick="window.location='<?= Yii::$app->urlManager->createUrl('/rbac/group') ?>'"><i class="fa fa-times"></i> Cancel</button>
            </div>
            <br clear="all"/>
        <?php ActiveForm::end(); ?>
          </div>
        </div>
      </div>
    </div>
</div>