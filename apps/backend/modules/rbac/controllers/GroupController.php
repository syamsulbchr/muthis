<?php

namespace backend\modules\rbac\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use common\models\AdminUserGroup;

class GroupController extends Controller
{
    public function actionIndex()
    {
        $model = AdminUserGroup::find()->all();
        return $this->render('index', 
                            [
                                'title'     => 'List User Group',
                                'pageDesc' => '',
                                'model' => $model,
                             ]
                            );

    }

 	public function actionCreate()
    {
        $post   = Yii::$app->request->post();
        $model  = New AdminUserGroup;
        $transaction    = Yii::$app->db->beginTransaction();
        if($model->load($post)) {
            if($model->save()){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollBack();
                echo "<pre>";
                print_r($model);
                print_r($model->getErrors());
                echo "</pre>";
                die;
            }
        }

        return $this->render('form', [
                    'title'        => 'Create User Group',
                    'model' => $model,
                    'parent' => $parent
        ]);
    }

    public function actionUpdate($id) {

        $post   = Yii::$app->request->post();
        $model  = AdminUserGroup::findOne($id);
        $transaction    = Yii::$app->db->beginTransaction();
        if($model->load($post)) {
            if($model->save()){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollBack();
            }
        }

        return $this->render('form', [
                    'title'     => 'Update User Group',
                    'model'     => $model,
                    'parent'    => $parent
        ]);
    }

    public function actionDelete($id)
    {
        $model = AdminUserGroup::findOne($id);
        $transaction   = Yii::$app->db->beginTransaction();
        try {
            if(!empty($model)){
                if($model->delete()){
                    $transaction->commit();
                }
            }
        }catch (Exception $e) {
            //echo '11';
            $transaction->rollback();
        }
        return $this->redirect(['index']);
    }

    public function actionChangeStatus()
    {
        //die('sa');
        $post           = (object)Yii::$app->request->post();
        //die('sa');
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '';
        $transaction    = Yii::$app->db->beginTransaction();
        if(isset($post->id) && isset($post->status)){
            $model = AdminUserGroup::findOne($post->id);
            if(!empty($model)){
                $status = 0;
                if($post->status == 0){
                    $status = 1;
                }
                $model->aug_status = $status;
                if($model->save()){
                    $return = true;
                    $msg    = 'Success';
                    $transaction->commit();
                }else{
                    echo '<pre>';
                    print_r($banner->getErrors());
                    echo '</pre>';
                    $msg    = 'Error, AJX4: Sintac Error';
                    $transaction->rollBack();
                }   
            }else{
                $msg = 'template not found';
            }
            $data = (object)['html'=>$html];
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }
}
