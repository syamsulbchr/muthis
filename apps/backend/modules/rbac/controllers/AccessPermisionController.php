<?php

namespace backend\modules\rbac\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use common\models\AdminUserGroup;
use common\models\AdminUserMenu;
use common\models\AdminGroupMenuAccess;

class AccessPermisionController extends Controller
{
    public function actionIndex()
    {
        //$actions = $this->combineController();
        $groups = AdminUserGroup::find()->all();
        $menus = AdminUserMenu::find()->where(['asm_parent_id'=>0])->orderBy(['asm_sort' => SORT_ASC])->all();
        
        if (isset($_POST['action'])) {
            foreach ($_POST['action'] as $key => $val) {
                $data = explode('-', $key);
                $this->updateAvailablity($data[0], $data[1], $val);
                $this->refresh();
            }
        }
        return $this->render('index', 
                            [
                                'title'     => 'List User Group',
                                'pageDesc' => '',
                                'menus' => $menus,
                                'groups' => $groups
                             ]
                            );

    }

 	public function actionCreate()
    {
        $post   = Yii::$app->request->post();
        $model  = New AdminUserGroup;
        $transaction    = Yii::$app->db->beginTransaction();
        if($model->load($post)) {
            if($model->save()){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollBack();
                echo "<pre>";
                print_r($model);
                print_r($model->getErrors());
                echo "</pre>";
                die;
            }
        }

        return $this->render('form', [
                    'title'        => 'Create User Group',
                    'model' => $model,
                    'parent' => $parent
        ]);
    }

    public function actionUpdate($id) {

        $post   = Yii::$app->request->post();
        $model  = AdminUserGroup::findOne($id);
        $transaction    = Yii::$app->db->beginTransaction();
        if($model->load($post)) {
            if($model->save()){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollBack();
            }
        }

        return $this->render('form', [
                    'title'     => 'Update User Group',
                    'model'     => $model,
                    'parent'    => $parent
        ]);
    }

    public function actionDelete($id)
    {
        $model = AdminUserGroup::findOne($id);
        $transaction   = Yii::$app->db->beginTransaction();
        try {
            if(!empty($model)){
                if($model->delete()){
                    $transaction->commit();
                }
            }
        }catch (Exception $e) {
            //echo '11';
            $transaction->rollback();
        }
        return $this->redirect(['index']);
    }

    public function actionChangeStatus()
    {
        //die('sa');
        $post           = (object)Yii::$app->request->post();
        //die('sa');
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '';
        $transaction    = Yii::$app->db->beginTransaction();
        if(isset($post->id) && isset($post->status)){
            $model = AdminGroupMenuAccess::find()->where('aga_aug_id=:groupId AND aga_asm_id=:menuId',[':groupId'=>$post->augid,':menuId'=>$post->id])->one();
            $status = 0;
            if($post->status == 0){
                $status = 1;
            }
            if(empty($model)){
                $model = New AdminGroupMenuAccess; 
            }
            $model->aga_aug_id = $post->augid;
            $model->aga_asm_id = $post->id;
            $model->aga_access = $status;
            if($model->save()){
                $return = true;
                $msg    = 'Success';
                $transaction->commit();
            }else{
                echo '<pre>';
                print_r($model->getErrors());
                echo '</pre>';
                $msg    = 'Error, AJX4: Sintac Error';
                $transaction->rollBack();
            }
            $data = (object)['html'=>$html];
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }

    protected function updateAvailablity($action, $groupId, $availablelity) {
        UserPermission::updateAll([
            'available' => $availablelity
        ],'action=:action and group_id=:group_id',[':group_id'=>$groupId,':action'=>$action]);
    }
}
