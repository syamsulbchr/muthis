<?php

namespace backend\modules\rbac\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use common\models\AdminUserMenu;
use common\models\AdminGroupMenuAccess;
use common\models\AdminUser;
use common\models\AdminUserGroup;
use common\models\Department;
use common\models\Level;

class UsersController extends Controller
{
    public function actionIndex()
    {
        $datas = AdminUser::find()->orderBy(['adu_id' => SORT_ASC])->all();
        return $this->render('index', 
                            [
                                'title'     => 'Users Access',
                                'pageDesc'  => '',
                                'datas'     => $datas,
                             ]
                            );

    }

 	public function actionCreate()
    {
        $post           = Yii::$app->request->post();
        $model          = New AdminUser;
        $group          = AdminUserGroup::find()->where(['aug_status'=>1])->all();
        $department     = Department::find()->where(['dpt_status'=>1])->all();
        $level          = Level::find()->where(['lvl_status'=>1])->all();
        $transaction    = Yii::$app->db->beginTransaction();
        if($model->load($post)) {
            $model->adu_screen_name = $model->adu_first_name.' '.$model->adu_last_name;
            if($model->save()){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollBack();
            }
        }

        return $this->render('form', [
                    'title'      => 'Users Access',
                    'model'      => $model,
                    'group'      => $group,
                    'department' => $department,
                    'level'      => $level,
        ]);
    }

    public function actionUpdate($id) {

        $post           = Yii::$app->request->post();
        $model          = AdminUser::findOne($id);
        $group          = AdminUserGroup::find()->where(['aug_status'=>1])->all();
        $department     = Department::find()->where(['dpt_status'=>1])->all();
        $level          = Level::find()->where(['lvl_status'=>1])->all();
        $transaction    = Yii::$app->db->beginTransaction();
        if($model->load($post)) {
            $model->adu_screen_name = $model->adu_first_name.' '.$model->adu_last_name;
            if($model->save()){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollBack();
            }
        }

        return $this->render('form', [
                    'title'      => 'Users Access',
                    'model'      => $model,
                    'group'      => $group,
                    'department' => $department,
                    'level'      => $level,
        ]);
    }

    public function actionDelete($id)
    {
        $model          = AdminUser::findOne($id);
        $transaction    = Yii::$app->db->beginTransaction();
        try {
            if(!empty($model)){
                if($model->delete()){
                    $transaction->commit();
                }
            }
        }catch (Exception $e) {
            //echo '11';
            $transaction->rollback();
        }
        return $this->redirect(['index']);
    }

    public function actionChangeStatus()
    {
        //die('sa');
        $post           = (object)Yii::$app->request->post();
        //die('sa');
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '';
        $transaction    = Yii::$app->db->beginTransaction();
        if(isset($post->id) && isset($post->status)){
            $model = AdminUser::findOne($post->id);
            if(!empty($model)){
                $status = 0;
                if($post->status == 0){
                    $status = 1;
                }
                $model->adu_status = $status;
                if($model->save()){
                    $return = true;
                    $msg    = 'Success';
                    $transaction->commit();
                }else{
                    echo '<pre>';
                    print_r($model->getErrors());
                    echo '</pre>';
                    $msg    = 'Error, AJX4: Sintac Error';
                    $transaction->rollBack();
                }   
            }else{
                $msg = 'template not found';
            }
            $data = (object)['html'=>$html];
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }
}
