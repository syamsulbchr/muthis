<?php

namespace backend\modules\rbac\controllers;

use Yii;
use yii\web\Controller;
use yii\data\ActiveDataProvider;
use yii\helpers\Json;
use common\models\AdminUserMenu;
use common\models\AdminGroupMenuAccess;

class DefaultController extends Controller
{
    public function actionIndex()
    {
        $menus = AdminUserMenu::find()->where(['asm_parent_id'=>0])->orderBy(['asm_sort' => SORT_ASC])->all();
        $id = 0;
        if(!empty($_GET['id'])){
            $id = $_GET['id'];
        }
        if (isset($_POST['action'])) {
            foreach ($_POST['action'] as $key => $val) {
                $data = explode('-', $key);
                $this->updateAvailablity($data[0], $data[1], $val);
                $this->refresh();
            }
        }
        return $this->render('index', 
                            [
                                'title'     => 'Menu List',
                                'pageDesc' => '',
                                'menus' => $menus,
                                'id'    => $id,
                             ]
                            );

    }

 	public function actionCreate()
    {
        $post   = Yii::$app->request->post();
        $model  = New AdminUserMenu;
        $parent = AdminUserMenu::find()->where(['asm_parent_id'=>0])->orderBy(['asm_sort' => SORT_ASC])->all();
        $transaction    = Yii::$app->db->beginTransaction();
        if($model->load($post)) {
            if(empty($model->asm_parent_id)){
                $model->asm_parent_id = 0;
            }
            if($model->save()){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollBack();
                echo "<pre>";
                print_r($model);
                print_r($model->getErrors());
                echo "</pre>";
                die;
            }
        }

        return $this->render('form', [
                    'title'        => 'Create',
                    'model' => $model,
                    'parent' => $parent
        ]);
    }

    public function actionUpdate($id) {

        $post   = Yii::$app->request->post();
        $model  = AdminUserMenu::findOne($id);
        $parent = AdminUserMenu::find()->where(['asm_parent_id'=>0])->orderBy(['asm_sort' => SORT_ASC])->all();
        $transaction    = Yii::$app->db->beginTransaction();
        if($model->load($post)) {
            if(empty($model->asm_parent_id)){
                $model->asm_parent_id = 0;
            }
            if($model->save()){
                $transaction->commit();
                return $this->redirect(['index']);
            }else{
                $transaction->rollBack();
            }
        }

        return $this->render('form', [
                    'title'     => 'Update',
                    'model'     => $model,
                    'parent'    => $parent
        ]);
    }

    public function actionDelete($id)
    {
        $adminUserMenu = AdminUserMenu::findOne($id);
        $transaction   = Yii::$app->db->beginTransaction();
        try {
            if(!empty($adminUserMenu)){
                $adminUserMenu = AdminUserMenu::findOne($id);
                AdminGroupMenuAccess::deleteAll('aga_asm_id ='.$adminUserMenu->asm_id);
                if($adminUserMenu->delete()){
                    $transaction->commit();
                }
            }
        }catch (Exception $e) {
            //echo '11';
            $transaction->rollback();
        }
        return $this->redirect(['index']);
    }

    public function actionChangeStatus()
    {
        //die('sa');
        $post           = (object)Yii::$app->request->post();
        //die('sa');
        $msg            = 'Invalid parameter';
        $param          = 'token';
        $return         = false;
        $error          = (object)[];
        $data           = (object)[];
        $html           = '';
        $transaction    = Yii::$app->db->beginTransaction();
        if(isset($post->id) && isset($post->status)){
            $model = AdminUserMenu::findOne($post->id);
            if(!empty($model)){
                $status = 0;
                if($post->status == 0){
                    $status = 1;
                }
                $model->asm_status = $status;
                if($model->save()){
                    $return = true;
                    $msg    = 'Success';
                    $transaction->commit();
                }else{
                    echo '<pre>';
                    print_r($model->getErrors());
                    echo '</pre>';
                    $msg    = 'Error, AJX4: Sintac Error';
                    $transaction->rollBack();
                }   
            }else{
                $msg = 'template not found';
            }
            $data = (object)['html'=>$html];
        }
        
        $result = (object)['return'=>$return,'data'=>$data,'msg'=>$msg,'error'=>$error];
        return Json::encode($result);
        
    }
}
